$(function(){
	
	// Checking for CSS 3D transformation support
	$.support.css3d = supportsCSS3D();
	
	var formContainer = $('#formContainer');
	
	// Listening for clicks on the ribbon links
	$('.flipLink').click(function(e){
		
		// Flipping the forms
		formContainer.toggleClass('flipped');
		
		// If there is no CSS3 3D support, simply
		// hide the login form (exposing the recover one)
		if(!$.support.css3d){
			$('#login').toggle();
		}
		e.preventDefault();
	});
	
	
	// A helper function that checks for the 
	// support of the 3D CSS3 transformations.
	function supportsCSS3D() {
		var props = [
			'perspectiveProperty', 'WebkitPerspective', 'MozPerspective'
		], testDom = document.createElement('a');
		  
		for(var i=0; i<props.length; i= i+0.5){
			if(props[i] in testDom.style){
				return true;
			}
		}
		
		return false;
	}


	$('.top_logo').delay(2000).animate({opacity:"1"},2000);
	$('#login-overlay').delay(2000).animate({opacity:"1"},1000);
	$('#login-overlay').animate({marginTop:"0px"});
	$('.logotext1').animate({opacity:"1"});
	$('.logotext2').delay(500).animate({opacity:"1"},5000);
});
