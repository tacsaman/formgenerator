//****************************************************************
//               Base64 Encode/decode Function
//               *****************************
//****************************************************************

//var Base64 = {

   // private property
_keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

// public method for encoding
          function encode  (input) {
             var output = "";
             var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
             var i = 0;
             input = _utf8_encode(input);
             while (i < input.length) {

                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                   enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                   enc4 = 64;
                }

                output = output +
                   _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                   _keyStr.charAt(enc3) + _keyStr.charAt(enc4);

             }

             return output;
          }

          // public method for decoding
function decode (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {

               enc1 = _keyStr.indexOf(input.charAt(i++));
               enc2 = _keyStr.indexOf(input.charAt(i++));
               enc3 = _keyStr.indexOf(input.charAt(i++));
               enc4 = _keyStr.indexOf(input.charAt(i++));

               chr1 = (enc1 << 2) | (enc2 >> 4);
               chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
               chr3 = ((enc3 & 3) << 6) | enc4;
               output = output + String.fromCharCode(chr1);
               if (enc3 != 64) {
                  output = output + String.fromCharCode(chr2);
               }
               if (enc4 != 64) {
                  output = output + String.fromCharCode(chr3);
               }

            }
            output = _utf8_decode(output);
            return output;

         }

         // private method for UTF-8 encoding
function _utf8_encode (string) {
                  string = string.replace(/\r\n/g,"\n");
                  var utftext = "";

                  for (var n = 0; n < string.length; n++) {

                     var c = string.charCodeAt(n);

                     if (c < 128) {
                        utftext += String.fromCharCode(c);
                     }
                     else if((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                     }
                     else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                     }

                  }

                  return utftext;
               }

               // private method for UTF-8 decoding
function _utf8_decode (utftext) {
                  var string = "";
                  var i = 0;
                  var c = c1 = c2 = 0;

                  while ( i < utftext.length ) {

                     c = utftext.charCodeAt(i);

                     if (c < 128) {
                        string += String.fromCharCode(c);
                        i++;
                     }
                     else if((c > 191) && (c < 224)) {
                        c2 = utftext.charCodeAt(i+1);
                        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                        i += 2;
                     }
                     else {
                        c2 = utftext.charCodeAt(i+1);
                        c3 = utftext.charCodeAt(i+2);
                        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                        i += 3;
                     }

                  }

                  return string;
               }

function farsiToUnicode (strTemp)
{
  //## begin UI_DisplayMgr::farsiToUnicode%404B116801A5.body preserve=yes
	var	dstTemp;
   var chTemp = "";
	dstTemp = "";
   var len = strTemp.length, i;
    i = 0;
    while (i < len ) {
		chTemp = strTemp[i];
		switch (chTemp)
		{
		case ' ': 
        	dstTemp += "+";
		break;
		case '�': 
        	dstTemp += "%D8%A7";
		break;
		case '�': 
        	dstTemp += "%D8%A8";
		break;
		case '�': 
        	dstTemp += "%D9%BE";
		break;
		case '�': 
        	dstTemp += "%D8%AA";
		break;
		case '�': 
        	dstTemp += "%D8%AB";
		break;
		case '�': 
        	dstTemp += "%D8%AC";
		break;
		case '�': 
        	dstTemp += "%DA%86";
		break;
		case '�': 
        	dstTemp += "%D8%AD";
		break;
		case '�': 
        	dstTemp += "%D8%AE";
		break;
		case '�': 
        	dstTemp += "%D8%AF";
		break;
		case '�': 
        	dstTemp += "%D8%B0";
		break;
		case '�': 
        	dstTemp += "%D8%B1";
		break;
		case '�': 
        	dstTemp += "%D8%B2";
		break;
		case '�': 
        	dstTemp += "%DA%98";
		break;
		case '�':
        	dstTemp += "%D8%B3";
		break;
		case '�': 
        	dstTemp += "%D8%B4";
		break;
		case '�': 
        	dstTemp += "%D8%B5";
		break;
		case '�': 
        	dstTemp += "%D8%B6";
		break;
		case '�': 
        	dstTemp += "%D8%B7";
		break;
		case '�': 
        	dstTemp += "%D8%B8";
		break;
		case '�': 
        	dstTemp += "%D8%B9";
		break;
		case '�': 
        	dstTemp += "%D8%BA";
		break;
		case '�': 
        	dstTemp += "%D9%81";
		break;
		case '�': 
        	dstTemp += "%D9%82";
		break;
		case '�': 
        	dstTemp += "%D9%83";
		break;
		case '�': 
        	dstTemp += "%D9%83";
		break;
		case '�': 
        	dstTemp += "%DA%AF";
		break;
		case '�': 
        	dstTemp += "%D9%84";
		break;
		case '�': 
        	dstTemp += "%D9%85";
		break;
		case '�': 
        	dstTemp += "%D9%86";
		break;
		case '�': 
        	dstTemp += "%D9%88";
		break;
		case '�': 
        	dstTemp += "%D9%87";
		break;
//		case 'X': 
//        	dstTemp += "%D9%8A";
//		break;
		case '�': 
        	dstTemp += "%D9%8A";
		break;
		case '�': 
        	dstTemp += "%D8%A2";
		break;
		case '�': 
        	dstTemp += "%D8%A1";
		break;
		//case '�': 
        //	dstTemp += "%D8%A3";                                                                 \
		//break;
		case '�': 
        	dstTemp += "%D8%A5";
		break;
		case '�': 
        	dstTemp += "%D8%A6";
		break;
		case '�': 
        	dstTemp += "%D8%A4";
		break;
		case '�': 
        	dstTemp += "%D8%A9";
		break;
		case '�': 
        	dstTemp += "%D9%8D";
		break;
		case '�': 
        	dstTemp += "%D9%8C";
		break;
		case '�': 
        	dstTemp += "%D9%8B";
		break;
		case '��': 
        	dstTemp += "%D9%91";
		break;
		case '�': 
        	dstTemp += "%D9%90";
		break;
		case '�': 
        	dstTemp += "%D9%8F";
		break;
		case '�': 
        	dstTemp += "%D9%8E";
		break;
		case '�': 
        	dstTemp += "%D8%9F";
		break;
		default: 
        	if(chTemp == '�')
	        	dstTemp += "%D9%8A";
			else          	
	        	dstTemp += chTemp;
		}
	i++;
	} 
    if (dstTemp == "+")
		dstTemp = "";   
    return dstTemp;
  //## end UI_DisplayMgr::farsiToUnicode%404B116801A5.body
}

