$(document).ready(function () {
	$("#uiswitch").iButton({
		labelOn: "Desktop&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
		labelOff: "&nbsp;&nbsp;&nbsp;&nbsp;Classic"
	});
	$("form#recover input").attr("disabled","disabled");
	$("input#username").focus(function () {
		$("label#unLabel").fadeOut("fast");
	}).blur(function () {
		if ($(this).val() == '') $("label#unLabel").fadeIn("fast");
	});
	$("input#password").focus(function () {
		$("label#pwLabel").fadeOut("fast");
	}).blur(function () {
		if ($(this).val() == '') $("label#pwLabel").fadeIn("fast");
	});
	$("input#username").focus();
//	$("form#login").submit(onClick);
	$("input#tryagain").click(tryAgain);
/*	if (getQueryVariable("err"))
	{
		refresh();
		$("form#login input").attr("disabled","disabled");
		$("form#recover input").removeAttr("disabled");
		$("form#recover").css('zIndex', 100);
		stateChanged()

	}*/
	get("username").select()

});
function get(a) {
	return document.getElementById(a)
}

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  } 
}

function keypress(c) {
	var d = get("username");
	var a = c;
	var b = true;
	var e;
	if ("which" in a) {
		e = a.which
	} else {
		if ("keyCode" in a) {
			e = a.keyCode
		} else {
			if ("keyCode" in window.event) {
				e = window.event.keyCode
			} else {
				if ("which" in window.event) {
					e = a.which
				} else {
					alert("the browser don't support")
				}
			}
		}
	}
	if (e == 13) {
		return onClick()
	}
	return true
}

function GetXmlHttpObject() {
	var a = null;
	try {
		a = new XMLHttpRequest()
	} catch (b) {
		try {
			a = new ActiveXObject("Msxml2.XMLHTTP")
		} catch (b) {
			a = new ActiveXObject("Microsoft.XMLHTTP")
		}
	}
	return a
}

function onClick() {
	var d = get("username");
	var c = d.value;
	var a = get("password");
	var b = a.value;
	var e = "";
	//var s = get("uiswitch");


	//s = s.checked;
	if (validate(c) === false) {
		e = "Please enter the username"
	}
	if (validate(b) === false) {
		e += e ? " and password" : "Please enter the password"
	}
	
	if (e) {
		$(".err-label").html(e + ".");
		return false
	}
	/* Flipping */
	$(".err-label").html("");
	$('#loading-spin').show();
	var formContainer = $('#formContainer');
	formContainer.toggleClass('flipped')
	if (!$.support.css3d) {
		$('#login').toggle("slow", function () {});
	}
	/*End Filiping*/
	refresh();
	$("form#login input").attr("disabled","disabled");
	$("form#recover input").removeAttr("disabled");
	$("form#recover").css('zIndex', 100);
	stateChanged()
	return false;
}

function tryAgain() {
	var formContainer = $('#formContainer');
	$('#tryagain').fadeOut();
	$('#loading-sping').fadeOut();
	$('#loading-cmp').fadeOut();
	$("form#login input#submitBtn").show();
	formContainer.toggleClass('flipped')
	if (!$.support.css3d) {
		$('#login').toggle("slow", function () {});
	}
	$("form#login input").removeAttr("disabled");
	$("form#recover input").attr("disabled","disabled");
	$("form#recover").css('zIndex', -100);
	$("input#password").select();
}

function onLoading(a) {
	get("username").disabled = a;
	get("password").disabled = a;
	get("submitBtn").disabled = a;
	if (!a) {
		get("username").select()
	} 
	else {
		$(".err-label").html("");
	}
	//get("loading").style.visibility = !a ? "hidden" : "visible"
}

function refresh() {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Your browser does not support AJAX.\nPlease update your browser.");
		return
	}
	//onLoading(true);
	//var a = "?username=" + c + "&password=" + b;
	url = "cxf/rest/authentication/getLoginError";
	//alert(url)
	xmlHttp.onreadystatechange = stateChanged;
	$.ajax({
		url: 'http://localhost:8080/cxf/rest/authentication/getLoginError',
	});
	$.ajax().done(function (msg) {
		onLoading(false);
		$('#loading-spin').hide();
		$('#loading-cmp').fadeIn();
		$('#tryagain').fadeIn(function () {
			$('#tryagain').focus()
		});
		
		$("#recover .err-label").html(msg);
	});
	//xmlHttp.open("POST", url, true);
	//xmlHttp.send()
}

function stateChanged() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		var resp = eval("(" + xmlHttp.responseText + ")");
		if (resp.success == true) {
			get("token").value = resp.token;
			get("login-form").submit()
		} else {
			onLoading(false);
			if (resp.errors) {
				$("#recover .err-label").html(resp.errors);
			} else {
				$("#recover .err-label").html("An unknown error occurred.");
			}
		}
	}
}

function validate(a) {
	return a !== ""
};

function setCookie(name, value, expires, path, domain, secure) 
{
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );
	/*
	if the expires variable is set, make the correct 
	expires time, the current script below will set 
	it for x number of days, to make it for hours, 
	delete * 24, for minutes, delete * 60 * 24
	*/
	if ( expires )
	{
	expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );
	
	document.cookie = name + "=" +escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
	( ( path ) ? ";path=" + path : "" ) + 
	( ( domain ) ? ";domain=" + domain : "" ) +
	( ( secure ) ? ";secure" : "" );
}


