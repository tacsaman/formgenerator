Ext.define('Ext.ux.homepanel', {
    extend: 'Ext.Panel',
    alias: 'widget.homepanel',

    constructor: function (config) {
        var me = this;
        var cfg = Ext.apply({}, Ext.clone(config));
        cfg.items=cfg.items || [];
	var html = '<uls class="wui-nav-list">';
		for(var i=0; i<cfg.items.items.length; i++)
			if(cfg.items.items[i].data.class)
				html += '<li><img src="'+Ext.BLANK_IMAGE_URL+'" class="'+cfg.items.items[i].data.iconCls+'"/><br />'+cfg.items.items[i].data.text+'</a></li>';
			else if(i)
				html += '</ul><div class="wui-nav-list-header"><h4>'+cfg.items.items[i].data.text+'</h4></div><ul class="wui-nav-list">';
			else
				html = '<div class="wui-nav-list-header"><h4>'+cfg.items.items[i].data.text+'</h4></div><ul class="wui-nav-list">';
		html += '</ul>';

	cfg.html = html;
	cfg.title = 'Home';
	cfg.autoScroll= true;
	cfg.border= false; 
        this.callParent([cfg]);
    }
});
