Ext.Loader.setConfig({
    enabled: true
});

Ext.Loader.setPath('Ext.ux', 'ux');

Ext.require([
    'Ext.tip.QuickTipManager',
    'Ext.container.Viewport',
    'Ext.layout.*',
    'Ext.form.Panel',
    'Ext.form.Label',
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.tree.*',
    'Ext.selection.*',
    'Ext.tab.Panel',
    'Ext.ux.layout.Center'
]);

Ext.define('MyDesktop.customXtypes.module', {
    extend: 'Ext.container.Container',
    alias: 'widget.module',
    cls: '',
    layout: {
        type: 'border'
    },
    homeTabStore: null,
    /*
	    constructor: function (config) {
		alert("in constructor");
	        var me = this;
	        var cfg = Ext.apply({}, Ext.clone(config));
	    	
	       	Ext.define('Home', {
			extend: 'Ext.data.Model',
			autoLoad: true,
			fields: [{name:'module',type:'string'}],
			proxy: {
				type: 'rest',
				url: cfg.homeurl,
				reader: {
					type: 'json',
					root: 'homeTab',
					successProperty: 'success'
				}
			},
			hasMany: ['Submodules']
		});
			
		Ext.define('Submodules', {
			extend: 'Ext.data.Model',
			fields: ['text', 'iconCls', 'class'],
			proxy: {
				type: 'rest',
				url: cfg.homeurl,
				reader: {
					type: 'json',
					root: 'submodules'
				}
			},
			belongsTo: 'Home'
		});

		Home.load('', {
			success: function (homeTab, options) {
				me.homeTabStore = homeTab; 			
			},
			failure:function(){  alert("faaaaaaaaail");}					

		});
	        this.callParent([cfg]);قبض برق
	    },
	*/
    initComponent: function() {

        var me = this;


        me.layoutExamples = [];
        me.store = Ext.create('Ext.data.TreeStore', {
            root: {
                expanded: true,
                text: 'classData'
            },
            proxy: {
                type: 'ajax',
                url: me.url
            }
        });


        me.store.on('load', function(store, records, successful, eOpts) {
            var data = this.tree.root.childNodes[0].childNodes;
            var html = '';


            var firstTime = 1;
            var subEnd = 0;
				var tmp="",tmp2="";
				tmp = "style=\"float:right !important\""; tmp2="-rtl";
            for (var i = 0; i < data.length; i++) {

                if (firstTime) {
                    if (data[i].raw.class) {
                        var html = '<ul class="wui-nav-list">';
                        html += '<li '+tmp+'><a id="' + data[i].data.id + '"><img src="' + Ext.BLANK_IMAGE_URL + '"class="' + data[i].raw.clss + '"/><br />' + data[i].data.text + '</a></li>';
                        firstTime = 0;
                    } else {
                        html += '<div><div class="wui-nav-list-header'+tmp2+'"><h4>' + data[i].data.text + '</h4></div><ul class="wui-nav-list">';

                        for (var j = 0; j < data[i].childNodes.length; j++) {
                            html += '<li'+tmp+'><a id="' + data[i].childNodes[j].data.id + '"><img src="' + Ext.BLANK_IMAGE_URL + '"class="' + data[i].childNodes[j].raw.clss + '"/><br />' + data[i].childNodes[j].data.text + '</a></li>';
                        }
                        subEnd = 1;
                        firstTime = 0;
                    }
                } else {



                    if (data[i].raw.class) {
                        if (subEnd) {
                            html += '</ul><div class="wui-nav-list-header'+tmp2+'"></div><ul class="wui-nav-list"><li '+tmp+'><a id="' + data[i].data.id + '"><img src="' + Ext.BLANK_IMAGE_URL + '"class="' + data[i].raw.clss + '"/><br />' + data[i].data.text + '</a></li>';
                            subEnd = 0;
                        } else
                            html += '<li '+tmp+'><a id="' + data[i].data.id + '"><img src="' + Ext.BLANK_IMAGE_URL + '"class="' + data[i].raw.clss + '"/><br />' + data[i].data.text + '</a></li>';
                    } else {
                        html += '</ul><div class="wui-nav-list-header'+tmp2+'"><h4>' + data[i].data.text + '</h4></div><ul class="wui-nav-list">';

                        for (var j = 0; j < data[i].childNodes.length; j++) {
                            html += '<li '+tmp+'><a id="' + data[i].childNodes[j].data.id + '"><img src="' + Ext.BLANK_IMAGE_URL + '"class="' + data[i].childNodes[j].raw.clss + '"/><br />' + data[i].childNodes[j].data.text + '</a></li>';
                        }
                        subEnd = 1;
                    }
                }
            }
            html += '</ul>';
            Ext.getCmp(me.prefix + '-content-panel').add({
                title: 'خانه',
                html: html,
                closable: false,
                listeners: {
                    render: function(component) {

                        var childLi = Ext.select(' ul > li > a ');

                        Ext.each(childLi.elements, function(item, i) {

                            item = Ext.get(item.id);
                            item.on({
                                click: function(cmp, a) {
                                    var rcd = store.getById(item.id);
                                    var cls = rcd.raw.clss;
                                    me.systemTreePanel.fireEvent('itemclick', null, rcd, null, null, null, null);
                                    me.systemTreePanel.getSelectionModel().select(rcd);
                                }
                            });

                        });
                    }
                }
            });

        });




        me.systemContentPanel = {
            id: me.prefix + '-content-panel',
            region: 'center',
            xtype: 'tabpanel',
            margins: '2 5 5 0',
            activeTab: 0,
            border: false,
            width: 800,
            defaults: {
                closable: true
            }

        };



        me.systemTreePanel = Ext.create('Ext.tree.Panel', {
            xtype: 'treepanel',
            id: me.prefix + 'tree-panel',
            layout: 'fit',
            region: 'center',
            split: true,
            minSize: 150,
            rootVisible: false,
            autoScroll: true,
            store: me.store
        });

        me.systemTreePanel.on('itemclick', function(t, record, item, index, e, eOpts) {

            if (record.get('leaf')) {
					userStore.modules().each(function(module) {
						if (module.get("name") == me.prefix){
							connection = module.get("connection");
						}
					});
                var moduleClass = record.raw.class;
                mainView = moduleClass + '.view.MainView';
                initController = moduleClass + '.controller.init';
                var treeTabId = record.getId() + '-panel';
                var tabpanel = Ext.getCmp(me.prefix + '-content-panel');
                var tabObject = tabpanel.getComponent(treeTabId);
                if (tabObject) {
                    tabpanel.layout.setActiveItem(treeTabId);
                } else {
					 	  var operation= record.raw.operation;
							if (! Ext.ClassManager.isCreated(initController)){
  		                  var controller = Ext.create(initController,{connection:connection});
      	              controller.init();
						  }
                    var newTab = Ext.create(mainView,{connection:connection,operation:operation});
                    tabpanel.add(newTab);
                    newid = newTab.getItemId();
                    tabpanel.layout.setActiveItem(newid);
                }


            }
        });

        Ext.applyIf(me, {

            items: [{
                    xtype: 'box',
                    id: me.prefix + '-header',
                    region: 'north',

                }, {
                    layout: 'border',
                    id: me.prefix + '-module-panel',
                    region: 'west',
                    border: false,
                    split: true,
                    margins: '2 0 5 5',
                    width: 200,
                    minSize: 100,
                    maxSize: 500,
                    items: [
                        me.systemTreePanel
                    ]
                },
                me.systemContentPanel
            ]
        });

        me.callParent(arguments);
    }
});
