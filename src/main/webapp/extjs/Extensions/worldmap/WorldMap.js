/**
 * @class Ext.ux.WorldMap
 * @author Holger Schäfer (h.schaefer@uniorg.de)
 * 
 * Ext 4.1.x port of WorldMap
 * 
 * Canvas World Map function 
 * Documentation: http://joncom.be/code/excanvas-world-map
 * 
 * Copyright (c) 2009 Jon Combe (http://joncom.be)
 * 
 * Usage:
 * {
 * 		xtype: 'worldmap',
 * 		// optional 
 * 		bgcolor: '#99b3cc',
 * 	    fgcolor: '#f2efe9',
 * 	    bordercolor: '#808080',
 * 	    padding: 10,
 * 	    zoom: 'ca,cl,us,ru',
 * 		footerLine: 'Hello World'  		
 * }
 *  
 * // color fill countries after rendering
 *	map.setDetails({
 *		'de': '#0000FF',
 *		'gb': '#00FF00'
 *	});
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */
Ext.Loader.setPath('Ext.ux', 'ux');
Ext.define('Ext.ux.WorldMap', {
    extend : 'Ext.Component',
    alias : 'widget.worldmap',
    
    unsupportedCanvasText: 'Please update your browser! Your browser does not support HTML5 canvas element.',
    Data: '',
	initComponent: function(config) {
        var me = this;
        
        Ext.apply(me, config, {
        	layout: 'fit',
        	bgcolor: '#99b3cc',
    	    fgcolor: '#ECFFD2',
    	    bordercolor: '#808080',
    	    padding: 0,
    	    zoom: 'ca,cl,us,ru'
    	    //zoom: 'de,gb'
        });        
        
        me.hasCanvasSupport = Ext.supports.Canvas;

    	if (me.hasCanvasSupport) {
    		me.autoEl = 'canvas';
    	} else {
    		me.autoEl = 'div';
    		me.padding = 10,
    		me.html = me.unsupportedCanvasText;
    		me.style = { backgroundColor: me.bgcolor };
    	}            	
    	
        // resize the canvas to fill browser window dynamically
        if (me.hasCanvasSupport) {
        	me.mon(this, 'resize', this.onPanelResize, this);
        }        
        
        me.callParent(arguments);
    },
    
    onRender: function() {
    	var me = this;
    	me.callParent(arguments);
    	me.canvasEl = me.el; 
    	me.canvas = me.canvasEl.dom;
    },
    
    onPanelResize: function(panel, width, height, oldWidth, oldHeight, opts) {
    	this.canvas.width = width;
        this.canvas.height = height;
        this.draw(); 
    },
    
    onDestroy: function () {
    	var me = this;
    	Ext.destroy(me.canvasEl);
        me.canvasEl = null;
        me.callParent();        
    },
    
    setDetails: function(details) {
    	var me = this;
    	me.details = details;
    	me.draw();
    },    
    
    draw: function(config) {
    	var me = this;    	
    	if (!me.hasCanvasSupport) return;
    	
	  	//create variables
    	var oWorldMap = me.Data;
    	var oSettings = (config || {});	 
		oSettings.detail = (oSettings.detail || {});
		var sBGColor = me.bgcolor;
		var sFGColor = me.fgcolor;
		me.sFGColor = sFGColor;
		var sBorderColor = me.bordercolor;
		me.sBorderColor = sBorderColor;
		var iPadding = ((me.padding || 10) * 2);
		var sZoom = (me.zoom || "ca,cl,us,ru");
		var iOffsetX = 0;
		var iOffsetY = 0;

		// get canvas dimensions, set bgcolor
		var oCanvas = me.canvas;
		var iCanvasWidth = oCanvas.width;
		var iCanvasHeight = oCanvas.height;
		oCanvas.style.backgroundColor = sBGColor;

		//create drawing area	  
		var oCTX = (oCanvas).getContext('2d');
		me.context = oCTX;
		oCTX.clearRect(0, 0, iCanvasWidth, iCanvasHeight);
		oCTX.lineWidth = (oSettings.borderwidth || 1);
	  
		//oCTX.fillRect(10, 20, 100, 100);	  

		// calculate zoom: create variables
		var aZoom = sZoom.split(",");
		var iMinX = oWorldMap[aZoom[0]][0][0][0];
		var iMaxX = oWorldMap[aZoom[0]][0][0][0];
		var iMinY = oWorldMap[aZoom[0]][0][0][1];
		var iMaxY = oWorldMap[aZoom[0]][0][0][1];

		// calculate zoom: find map range
		for (var iCountry = 0; iCountry < aZoom.length; iCountry++) {
			for (var iPath = 0; iPath < oWorldMap[aZoom[iCountry]].length; iPath++) {
				for (var iCoord = 0; iCoord < oWorldMap[aZoom[iCountry]][iPath].length; iCoord++) {
					iMinX = Math.min( iMinX, oWorldMap[aZoom[iCountry]][iPath][iCoord][0] );
					iMaxX = Math.max( iMaxX, oWorldMap[aZoom[iCountry]][iPath][iCoord][0] );
					iMinY = Math.min( iMinY, oWorldMap[aZoom[iCountry]][iPath][iCoord][1] );
					iMaxY = Math.max( iMaxY, oWorldMap[aZoom[iCountry]][iPath][iCoord][1] );
				}
			}
		}

		// calculate zoom ratio
		var iRatio = Math.min(
			((iCanvasWidth - iPadding) / (iMaxX - iMinX)), 
			((iCanvasHeight - iPadding) / (iMaxY - iMinY)) 
		);
		me.iRatio = iRatio;

		// calculate zoom offsets
		var iMidX = (iMinX + ((iMaxX - iMinX) / 2));
		var iMidY = (iMinY + ((iMaxY - iMinY) / 2));
		iOffsetX = ((iMidX * iRatio) - (iCanvasWidth / 2));
		me.iOffsetX = iOffsetX;
		iOffsetY = ((iMidY * iRatio) - (iCanvasHeight / 2));
		me.iOffsetY = iOffsetY;

		// draw "plain" countries
		for (var sCountry in oWorldMap) {
			me.drawCountry(sCountry, sFGColor);
		}

		// draw "details" countries
		for (var sCountry in me.details) {
			if (oWorldMap[sCountry]) {
				me.drawCountry(sCountry, me.details[sCountry]);
			}
		}    	    
	  
		me.drawFooter();
		//me.settings = oSettings;
    },    
    
    // private draw function
    drawCountry: function(sCountry, sColor) {
    	var me = this;
    	
    	var oWorldMap = me.Data;
    	var oCTX = me.context;
    	var iRatio = me.iRatio;
    	var iOffsetX = me.iOffsetX;
    	var iOffsetY = me.iOffsetY;
    	//var oSettings = me.settings;
    	var sFGColor = me.sFGColor;
    	
    	oCTX.fillStyle = sColor;
    	oCTX.strokeStyle = me.sBorderColor;
    	oCTX.beginPath();

    	//loop through paths
    	var bIE = (navigator.userAgent.indexOf("MSIE") > -1);
    	for (var iPath = 0; iPath < oWorldMap[sCountry].length; iPath++) {
    		oCTX.moveTo(
    			(oWorldMap[sCountry][iPath][0][0] * iRatio) - iOffsetX, 
    			(oWorldMap[sCountry][iPath][0][1] * iRatio) - iOffsetY
    		);
    		for (var iCoord = 1; iCoord < oWorldMap[sCountry][iPath].length; iCoord++) {
    			oCTX.lineTo(
    				(oWorldMap[sCountry][iPath][iCoord][0] * iRatio) - iOffsetX, 
    				(oWorldMap[sCountry][iPath][iCoord][1] * iRatio) - iOffsetY
    			);
    		}
    		oCTX.closePath();
    		oCTX.fill();

    		// IE, again...
    		if (bIE == true) {
    			oCTX.beginPath();
    			oCTX.moveTo(
    				(oWorldMap[sCountry][iPath][0][0] * iRatio) - iOffsetX, 
    				(oWorldMap[sCountry][iPath][0][1] * iRatio) - iOffsetY
    			);
    			for (iCoord = 1; iCoord < oWorldMap[sCountry][iPath].length; iCoord++) {
    				oCTX.lineTo(
    					(oWorldMap[sCountry][iPath][iCoord][0] * iRatio) - iOffsetX, 
    					(oWorldMap[sCountry][iPath][iCoord][1] * iRatio) - iOffsetY
    				);
    			}
    			oCTX.closePath();
    		}
    		oCTX.stroke();
    	}

    	// awful hack for Lesotho / South Africa (draw Lesotho again, kids!)
    	if (sCountry == "za") {
    		// choose colour
    		/*
        	if (oSettings.detail["ls"]) {
          	oCTX.fillStyle = oSettings.detail["ls"];
        	} 	else {
          	oCTX.fillStyle = sFGColor;
        	}
    		 */
    		oCTX.fillStyle = sFGColor;

    		// loop through paths
    		oCTX.beginPath();
    		for (var iPath = 0; iPath < oWorldMap["ls"].length; iPath++) {
    			oCTX.moveTo(
    				(oWorldMap["ls"][iPath][0][0] * iRatio) - iOffsetX, 
    				(oWorldMap["ls"][iPath][0][1] * iRatio) - iOffsetY
    			);
    			for (iCoord = 1; iCoord < oWorldMap["ls"][iPath].length; iCoord++) {
    				oCTX.lineTo(
    					(oWorldMap["ls"][iPath][iCoord][0] * iRatio) - iOffsetX, 
    					(oWorldMap["ls"][iPath][iCoord][1] * iRatio) - iOffsetY
    				);
    			}
    			oCTX.closePath();
    			oCTX.fill();
    			oCTX.stroke();
    		}
    	}
    },    
    
    drawFooter: function() {
    	var me = this;
    	if (!Ext.isEmpty(me.footerLine)) {
	    	var ctx = me.context; 
	    	ctx.font = "22px verdana";
	    	ctx.strokeText(me.footerLine, me.getWidth()/5, me.getHeight() - 20);
    	}
    },
    
   
});
