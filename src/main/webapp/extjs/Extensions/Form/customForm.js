Ext.define('Ext.ux.customForm', {
    extend: 'Ext.form.Panel',
    requires: ['Ext.toolbar.Toolbar',
	       'Ext.button.Button'],
    alias: 'widget.customform',

    /**
     * @cfg {String/Object} layout
     * Designed to support hbox and vbox layouts. Defaults to 'vbox'.
     * If you pass in the configuration 'hbox', all items will automatically get a 'defaultComboPadding'
     * of 10.
     */
   
    hideSaveBtn:false,
    hideCancelBtn:false,
    hideResetBtn:false,
    title: '',
    saveId:'',
    saveType:'',
    extraParams: [],
    loadId:'',
    loadType:'',
    loadType1:'',
    loadGroupId:'',
    loadModuleId:'',
    saveItemId: '',
    cancelItemId: '',
    resetItemId: '',
    bindGrid:'',
    autoLoad:true,
    expandOnGrid:false,
    saveIconCls: 'icon-save',
    cancelIconCls: 'icon-cancel',
    resetIconCls: '',
    defaultSave:true,
    defaultReset:true,
    defaultafterrender:true,
    toolbarArray:[],
    saveCallback:null,
    submitUrl:'',

    initComponent: function() {
        // automatically provide some padding when user sets layout to hbox
        var me = this;
		  me.frame=true;
	me.addEvents(
            'saveForm',
            'cancelForm',
            'resetForm'
        );
	Ext.applyIf(me, {
		baseParams: {
               		id: me.loadId,
			token:Ext.ux.TOKEN,
                	type: me.loadType,
                	type1: me.loadType1
            	},            
		dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'button',
			    itemId: me.resetItemId,
                            text: userStore.strings().findRecord("alias","common_refresh").data.string,
 			    iconCls: me.resetIconCls,
			    hidden: me.hideResetBtn,
			    handler: function(button, e) {
				me.fireEvent('resetForm', me);
                            },
                        },
                        {
                            xtype: 'button',
			    itemId: me.saveItemId + '-submit',
                            text: userStore.strings().findRecord("alias","common_save").data.string,
			    iconCls: me.saveIconCls,
			    hidden: me.hideSaveBtn,
			    handler: function(button, e) {
				me.fireEvent('saveForm', me);
                            },
                        },
                        {
                            xtype: 'button',
			    itemId: me.cancelItemId,
                            text: userStore.strings().findRecord("alias","common_cancel").data.string,
			    iconCls: me.cancelIconCls,
			    hidden: me.hideCancelBtn,
			    handler: function(button, e) {
				me.fireEvent('cancelForm', me);
                            },
                        }
                    ]
                }
            ],
            listeners: {
                afterrender: {
                    fn: me.onFormAfterRender,
                    scope: me
                },
		cancelForm: {
                    fn: me.onCancelForm,
                    scope: me
                },
		saveForm: {
                    fn: me.onSaveForm,
                    scope: me
                },
		resetForm: {
                    fn: me.onResetForm,
                    scope: me
                }
            }
        });
	
	for(var i=0; i <me.toolbarArray.length;i++){
		
		me.dockedItems[0].items.push(
     		{
                  xtype: 'button',
                  text:me.toolbarArray[i].text,
		  tooltip:me.toolbarArray[i].tooltip,
		  iconCls:me.toolbarArray[i].iconCls,
		  itemId:me.toolbarArray[i].itemId,
		  disabled:me.toolbarArray[i].disabled,
		  handler: function(button, e) {
			for(var j =0 ;j<me.toolbarArray.length; j++){
				if(this.text == me.toolbarArray[j].text)
					break;			
			}
			me.fireEvent(me.toolbarArray[j].event, me, e);
		  }     
		});
	}

        me.callParent(arguments);
    },

    onFormAfterRender: function(me, eOpts) {
	 if(this.defaultafterrender){
		if(this.autoLoad){
			me.load({
				method:'GET',
				waitMsg:'Loading data...'
			});
		}
	}
    },

    onCancelForm: function(me, eventOptions) {
	if(me.up().xtype == "container" && me.up().up().xtype =="tabpanel")
	    me.up().up().getActiveTab().destroy();
	else if (me.up().xtype == "window")
	    me.up().close();
	else 
	    me.close();
    },
   
    callBack: function(){
	this.saveCallback.call();
	this.getRefOwner().close();
    },    

    onSaveForm: function(me, eventOptions) {
     if(this.defaultSave){
	//extraParams = {id1: 100, id2: 200, "tag with spaces": 300};	
	
	this.extraParams['id'] = this.saveId;
	if (!this.getValues()['type'])
		this.extraParams['type'] = this.saveType;
	else
		this.extraParams['type'] = this.getValues()['type'] ;

	var util = Ext.create('MyDesktop.Util.util', {callBack: function(){me.saveCallback.call();me.getRefOwner().close();}, url: me.submitUrl, data: this.extraParams, form: this});
	util.sendForm();
     }
    },

    onResetForm: function(me, eventOptions) {
		if(this.defaultReset)	
			me.load({
				method:'GET',
				waitMsg:'Loading'
			});
    }
});
