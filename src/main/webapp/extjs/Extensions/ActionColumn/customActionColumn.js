Ext.define('Ext.ux.customActionCol', {
    extend: 'Ext.grid.column.Action',
    alias: 'widget.customactioncol',

    enableContextMenu: true,
    draggable: false,
    hideable: false,
    menuDisabled: true,
    sortable: false,
    resizable: false,
    fixed: true,
    autoWidth: true,
    iconWidth: 50,
    align: 'center',
    hideIndex: null,
    hiddenCls: Ext.baseCSSPrefix + 'action-col-img-noaction',
    text: 'Actions',
    editiconCls: 'wui-row-edit',
    deleteiconCls: 'wui-row-delete',
    deleteurl: '',
    deleteobject: null,
    deletetip: 'حذف',
    edittip: 'ویرایش',
    deletekeyArray: [],
    deletable: true,
    alwaysShowDelete: false,
    customEdit: false,
    defaultCustomEdit: true,
    //////////////

    constructor: function(config) {
        var me = this;
        this.addEvents(
            'customEditAction'
        );
        var cfg = Ext.apply({}, Ext.clone(config));
        cfg.items = cfg.items || [];

        if (cfg.customEdit == true) {
            cfg.items.push({
                iconCls: this.editiconCls,
                tooltip: this.edittip,
                handler: this.customEditFunction,
                handler: function(view, rowIndex, colIndex, item, e, record, row) {
                    if (cfg.customEdit) {
                        me.fireEvent('customEditAction', view, rowIndex, colIndex, item, e, record, row);
                    }
                }
            });
        }

        if (cfg.deletable) {
            cfg.items.push({
                iconCls: this.deleteiconCls,
                tooltip: this.deletetip,
                handler: function(view, rowIndex, colIndex, item, e, record, row) {
			var use = record.data.use !== undefined ? record.data.use : "NO";
			use = use == "0" ? "NO" : use;
                    if (use == "NO" || this.alwaysShowDelete == true) {
                        for (var i = 0; i < this.deletekeyArray.length; i++) {
                            this.deleteobject[this.deletekeyArray[i][0]] = record.get(this.deletekeyArray[i][1]);
                        }
                        Ext.Msg.show({
                            title: userStore.strings().findRecord("alias", "warning").data.string,
                            msg: userStore.strings().findRecord("alias", "admin_delete_confirm_item").data.string,
                            buttons: Ext.Msg.YESNOCANCEL,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn, text) {
                                if (btn == 'yes') {

                                    Ext.Ajax.request({
                                        waitMsg: 'deleting...',
					headers: { 'Content-Type': 'application/json' },
                                        url: me.deleteurl,
method:'GET',
                                        params:me.deleteobject,
                                        failure: function(response, options) {
                                            Ext.MessageBox.alert(userStore.strings().findRecord("alias", "warning").data.string, 'Lost connection to the server!');
                                        },
                                        success: function(o) {
                                            var decoded = Ext.decode(o.responseText);
                                            if (decoded.success && !decoded.errors) {
                                                view.getStore().reload();
                                            } else if (decoded.errors) {
                                                Ext.MessageBox.alert(userStore.strings().findRecord("alias", "error").data.string, 'Success false!');
                                                view.getStore().reload();
                                            } else
                                                Ext.MessageBox.alert(userStore.strings().findRecord("alias", "error").data.string, 'Some error occured!');
                                        }
                                    });

                                }

                            }

                        });
                    }
                },
                getClass: function(v, metadata, r, rowIndex, colIndex, store) {
		    if(this.alwaysShowDelete == false){
		            // hide this action if row data flag indicates it is not deletable
		            if (r.data.use.toUpperCase() == 'YES' || r.data.use == "1") {
		                return ' <img src="" alt="" height="16" width="16" style="cursor:default !important;"> ';
		            } else if (r.data.use.toUpperCase() == 'NO' || r.data.use == "0") {
		                return this.deleteiconCls;
		            }
		    }
		    else{
     			return this.deleteiconCls;
		    }

                }
            });
        }




        var items = cfg.items || [this];
        /*			
		for (var index = 0; index < items.length; index++) {
			var item = items[index];

			// patch for 4.1 beta bug
			//if (item.iconCls) {
			//	this.iconCls = item.iconCls;
			//}
			if (item.hideIndex || item.clsFn) {
				item.tmpIconCls = item.iconCls || '';
				item.tmpTooltip = item.tooltip || '';
				if (item.iconCls) delete item.iconCls;
				if (item.tooltip) delete item.tooltip;
				Ext.apply(item, {
					getClass: function(v, meta, record) {
						var i = 0;
						if (v) i = v.match(/<img/g).length;
						if (this.items[i].hideIndex) {
							if (record.get(this.items[i].hideIndex)) {
								this.items[i].tooltip = '';
								return this.hiddenCls;
							}
						}
						if (!this.items[i].clsFn) {
							this.items[i].tooltip = this.items[i].tmpTooltip;
							return this.items[i].tmpIconCls;
						}
						else {
							var cls = this.items[i].clsFn.call(this, record, this.items[i]);
							return cls;
						}
					}
				});
			}
		}
*/
        cfg.itemCount = items.length;
        cfg.listeners = {
            customEditAction: {
                fn: this.onCustomEditAction,
                scope: this
            }
        }

        this.callParent([cfg]);
    },

    initComponent: function() {
        if (this.autoWidth) {
            this.width = (this.iconWidth * this.itemCount);
        }
        if (this.width) {
            this.minWidth = this.width;
            this.maxWidth = this.width;
        }

        this.callParent();
        this.addEvents(
            'actionclick'
        );
    },

    onRender: function() {
        this.callParent(arguments);
        if (this.enableContextMenu) {
            this.grid = this.up('tablepanel');
            this.grid.menu = Ext.create('Ext.menu.Menu');
            this.grid.on('itemcontextmenu', this.showContextMenu, this);
        }
    },

    onDestroy: function() {
        if (this.rendered && this.grid.menu) {
            this.grid.menu.destroy();
            this.grid.menu = null;
            delete this.grid.menu;
        }
        this.callParent(arguments);
    },

    showContextMenu: function(view, record, el, index, e, ops) {
        var actionIndex = this.ownerCt.getHeaderIndex(this);
        var actionEl = Ext.get(Ext.get(el).query('td')[actionIndex]).first();
        var imgs = actionEl.query('img');
        this.grid.menu.items.each(function(item) {
            item.destroy();
        });
        this.grid.menu.items.clear();

        Ext.each(imgs, function(item) {
            var img = Ext.get(item);
            if (!img.hasCls(this.hiddenCls)) {
                var clss = Ext.String.trim(img.getAttribute('class')).split(' ');
                var cls = clss[clss.length - 1];
                this.grid.menu.add({
                    text: img.getAttribute('data-qtip'),
                    iconCls: cls,
                    handler: function() {
                        this.fireEvent('actionclick', this.grid, this.grid.getStore(), record, cls);
                    },
                    scope: this
                });
            }
        }, this);
        if (this.grid.menu.items.getCount()) {
            this.grid.getSelectionModel().select(record);
            this.grid.menu.showAt(e.getX(), e.getY(), true);
            e.stopEvent();
        }
    },

    processEvent: function(type, view, cell, recordIndex, cellIndex, e) {
            if (type == 'dblclick') {
                return false;
            }
            if (type == 'click') {
                var match = e.getTarget().className.match(this.actionIdRe);
                if (match) {
                    var tmp = Ext.String.trim(e.getTarget().className).split(' ');
                    var action = tmp[tmp.length - 1];
                    if (action != '' && action != this.hiddenCls) {
                        this.fireEvent('actionclick', view.ownerCt, view.getStore(), view.getStore().getAt(recordIndex), action);
                    }
                }
            }
            return this.callParent(arguments);
        }
        /*,
        	
        	onCustomEditAction:function(view, rowIndex, colIndex, item, e, record, row){
        	    if(this.defaultCustomEdit){
        		console.log(view);
            	    }
        	}*/
});
