Ext.Loader.setPath('Ext.ux', 'ux');
Ext.define('Ext.ux.customGrid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.selection.CellModel',
        'Ext.grid.*',
        'Ext.util.*',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Paging',
        'Ext.ux.grid.FiltersFeature',
        'Ext.ux.ajax.JsonSimlet',
        'Ext.ux.ajax.SimManager',
        'Ext.button.Split',
        'Ext.toolbar.Separator',
        'Ext.grid.column.Action',
        'Ext.toolbar.TextItem',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Text'
    ],

    alias: 'widget.customgrid',
    ////////
    /**
     * @private
     * search value initialization
     */
    searchValue: null,

    /**
     * @private
     * The row indexes where matching strings are found. (used by previous and next buttons)
     */
    indexes: [],

    /**
     * @private
     * The row index of the first search, it could change if next or previous buttons are used.
     */
    currentIndex: null,

    /**
     * @private
     * The generated regular expression used for searching.
     */
    searchRegExp: null,

    /**
     * @private
     * Case sensitive mode.
     */
    caseSensitive: false,

    /**
     * @private
     * Regular expression mode.
     */
    regExpMode: false,

    /**
     * @cfg {String} matchCls
     * The matched string css classe.
     */
    // matchCls: 'x-livesearch-match',


    ///////
    title: '',
    hideNewBtn: false,
    newBtnText: '',
    newBtnTip: '',
    newBtnIconCls: '',
    hidePaging: false,
    hidetbar: false,
    hidesearchbar: false,
    hideRefreshBtn: true,
    newBtnItemId: '',
    showRowonNew: true,
    editURL: '',
    addURL: '',
    editObject: null,
    defaultEdit: true,
    addRecord: null,
    model: '',
    editflg: -1,
    addflg: -1,
    flg: this.editflg, //by default flag equals editflg.when user clicks add btn, flag changes to addflg.
    editable: true,
    editkeyArray: [],
    cancelObject: '',
    toolbarArray: [],
    editType: 0, //0 for edit , 1 for add
    disableEditRow: false, // to disable row editing in response to double click on a row
    addFlag: '',
    defaultStatusText: "خالی",
    scroll: true,
    urlDetect: '',

    initComponent: function() {
        var me = this;
        me.addEvents(
            'newBtnClick'
        );
        for (var i = 0; i < me.toolbarArray.length; i++) {
            me.addEvents(me.toolbarArray[i].event);
        }
        /*
        	me.bbar = Ext.create('Ext.ux.statusbar.StatusBar', {
                    defaultText: me.defaultStatusText,
                    name: 'searchStatusBar'
                });
        */
        var filters = {
            ftype: 'filters',
            encode: false, // json encode the filter query
            local: true, // defaults to false (remote filtering)
            filters: [{
                type: 'boolean',
                dataIndex: 'visible'
            }],
            menuFilterText: 'فیلتر'
        };
        Ext.applyIf(me, {
            features: [filters],
            bbar: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
flex:1,
                    displayInfo: true,
                    store: me.getStore(),
                    hidden: me.hidePaging,
		    items:[{ xtype: 'tbseparator' },{
                    xtype: 'numberfield',
                    itemId: 'pageSize',
						  margin:'0 10 0 10',
                    fieldLabel: "اندازه صفحه:",
                    hidden: me.hidePaging,
                    value: 25,
						  labelWidth:60,
                    width: 120,
                    minValue: 1,
                    maxValue: 100000,
                    listeners: {
                        change: function(field, value) {
                            me.getStore().pageSize = value;
                            me.getStore().reload({
                                params: {
                                    start: 0,
                                    limit: value
                                }
                            });
                        }
                    }
                },{ xtype: 'tbseparator' }]
                }
            ],
            dockedItems: [{
					xtype: 'label',
					hidden: me.hidePaging
				}, {
                    xtype: 'toolbar',
                    dock: 'top',
                    hidden: me.hidetbar,
                    items: [{
                        xtype: 'splitbutton',
                        text: 'دستی',
                        handler: me.setRefreshing,
                        scope: me,
                        hidden: me.hideRefreshBtn,
                        iconCls: 'wui-refresh',
                        itemId: 'refbtnId',
                        menu: [{
                            text: 'دستی',
                            type: 0,
                            handler: me.setRefreshing,
                            scope: me
                        }, {
                            text: '۵ ثانیه',
                            type: 5,
                            handler: me.setRefreshing,
                            scope: me
                        }, {
                            text: '۱۵ ثانیه',
                            type: 15,
                            handler: me.setRefreshing,
                            scope: me
                        }, {
                            text: '۳۰ ثانیه',
                            type: 30,
                            handler: me.setRefreshing,
                            scope: me
                        }, {
                            text: '۶۰ ثانیه',
                            type: 60,
                            handler: me.setRefreshing,
                            scope: me
                        }]
                    }, {
                        xtype: 'tbseparator',
                        hidden: me.hideRefreshBtn
                    }, {
                        xtype: 'button',
                        text: me.newBtnText,
                        itemId: me.newBtnItemId,
                        tooltip: me.newBtnTip,
                        hidden: me.hideNewBtn,
                        iconCls: ((me.newBtnIconCls == '') ? 'icon-user-add' : me.newBtnIconCls),
                        handler: function(button, e) {
                            me.fireEvent('newBtnClick', me, e);
                        }
                    }]
                },
                ///////////////////////////////search bar
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    hidden: me.hidesearchbar,
                    items: [
                        'جستجو', {
                            xtype: 'textfield',
                            name: 'searchField',
                            hideLabel: true,
                            width: 100,
                            listeners: {
                                change: {
                                    fn: me.onTextFieldChange,
                                    scope: this,
                                    buffer: 100
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: 'قبلی',
                            iconCls: 'icon-next',
                            tooltip: 'ردیف قبلی',
                            handler: me.onPreviousClick,
                            scope: me
                        }, {
                            xtype: 'button',
                            text: 'بعدی',
                            iconCls: 'icon-prev',
                            tooltip: 'ردیف بعدی',
                            handler: me.onNextClick,
                            scope: me
                        }, '-', {
                            xtype: 'checkbox',
                            hideLabel: true,
                            margin: '0 0 0 4px',
                            handler: me.regExpToggle,
                            scope: me
                        }, 'عبارت منظم', {
                            xtype: 'checkbox',
                            hideLabel: true,
                            margin: '0 0 0 4px',
                            handler: me.caseSensitiveToggle,
                            scope: me
                        }, 'حساس به حروف کوچک و بزرگ'
                    ]
                }
                ///////////////////////////////
            ],

            plugins: [
                Ext.create('Ext.grid.plugin.RowEditing', {
                    pluginId: 'gridplugin',
                    listeners: {
                        edit: {
                            fn: me.onRowEditingEdit,
                            scope: me
                        },
                        beforeedit: {
                            fn: me.onRowEditingBeforeEdit,
                            scope: me
                        },
                        canceledit: {
                            fn: me.onRowEditingCanceledit,
                            scope: me
                        },
                        validateedit: {
                            fn: me.onRowEditingValidateedit,
                            scope: me
                        }
                    }
                })
            ],

            listeners: {
				filterupdate:{
                    fn: me.onFilterUpdateRender,
                    scope: me
				},
                afterrender: {
                    fn: me.onGridpanelAfterRender,
                    scope: me
                },
                newBtnClick: {
                    fn: me.onNewBtnClick,
                    scope: me
                },
                destroy: {
                    fn: me.onDestroy,
                    scope: me
                }
            }
        });

        for (var i = 0; i < me.toolbarArray.length; i++) {
            me.dockedItems[1].items.push({
                xtype: 'button',
                text: me.toolbarArray[i].text,
                tooltip: me.toolbarArray[i].tooltip,
                iconCls: me.toolbarArray[i].iconCls,
                itemId: me.toolbarArray[i].itemId,
                disabled: me.toolbarArray[i].disabled,
                handler: function(button, e) {
                    for (var j = 0; j < me.toolbarArray.length; j++) {
                        if (this.text == me.toolbarArray[j].text)
                            break;
                    }
                    me.fireEvent(me.toolbarArray[j].event, me, e);
                }
            });
        }

        me.callParent(arguments);
    },

	onFilterUpdateRender:  function(grid,filters) {
		var jsonFilters = grid.getFilterData();

		 if(!filters || !filters.length) {
			var keys = grid.filters.keys;
		    for (var i = 0; i < keys.length; i++) {
		        di = keys[i];
				//this.store.proxy.extraParams[di] = null
				delete this.store.getProxy().extraParams[di];
		    }
		}
		Ext.each(jsonFilters, function(f) {
			var param = f.field;
			this.store.proxy.extraParams[param]=f.data.value;
		}, this);
	},
    onGridpanelAfterRender: function(component, context, eOpts) {
        if (!this.editable) {
            this.getPlugin('gridplugin').destroy();
            this.getView().refresh();
        }
        if (!this.hidesearchbar) {
            this.textField = this.down('textfield[name=searchField]');
            //this.statusBar = this.down('statusbar[name=searchStatusBar]');
        }
        for (var i = 0; i < context.scope.columns.length; i++) {
            context.scope.columns[i].filterable = true;
        }

    },
    onDestroy: function() {
        if (this.timer)
            window.clearInterval(this.timer);
    },
    tagsRe: /<[^>]*>/gm,

    // DEL ASCII code
    tagsProtect: '\x0f',

    // detects regexp reserved word
    regExpProtect: /\\|\/|\+|\\|\.|\[|\]|\{|\}|\?|\$|\*|\^|\|/gm,

    onNewBtnClick: function(button, e, eOpts) {
        if (this.showRowonNew && this.editable) {
            this.editType = 1;
            this.addFlag = "new";
            this.getPlugin('gridplugin').cancelEdit();
            var rec = Ext.create(this.model, this.addRecord);
            if (this.addflg != -1) {
                this.flg = this.addflg;
            }
            this.getStore().insert(0, rec);
            this.getPlugin('gridplugin').startEdit(0, 0);
        }
    },

    onRowEditingEdit: function(editor, context, eOpts) {
        var url = "";
        this.urlDetect = this.urlDetect || 'id';
        for (var i = 0; i < this.editkeyArray.length; i++) {
            this.editObject[this.editkeyArray[i][0]] = context.record.data[this.editkeyArray[i][1]];
        }
        if (this.addFlag == "")
            this.addFlag = "edit";
        if (context.record.data[this.urlDetect])
            url = this.editURL
        else
            url = this.addURL
        if (this.defaultEdit) {
            Ext.Ajax.request({
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                params: Ext.encode(this.editObject),
                scope: this,
                failure: function(result, request) {
                    Ext.MessageBox.alert(userStore.strings().findRecord("alias", "error").data.string, 'Cannot Send Request!');
                },
                success: function(result, request) {
                    var decoded = Ext.decode(result.responseText);
                    if (decoded.success && !decoded.errorMessage) {
                        context.grid.getStore().reload();
                    } else if (decoded.errorMessage) {
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: decoded.errorMessage
                        }).show();
                        context.grid.getStore().reload();
                    } else {
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: decoded.errorMessage
                        }).show();
                    }
                }
            });

            if (this.addflg != -1) {
                this.flg = this.editflg;
            }
        }
    },

    onRowEditingBeforeEdit: function(editor, context, eOpts) {
        if (!this.editType && this.disableEditRow) {
            return false;
        } else {
            if (!this.editType) {
                var di;
                for (var i = 0; i < context.grid.columns.length; i++) {
                    di = context.grid.columns[i].dataIndex;
                    if (context.record.data[di])
                        context.grid.columns[i].disableEditField ? (di != null ? context.grid.getPlugin('gridplugin').editor.form.findField(di).disable() : null) : (di != null ? context.grid.getPlugin('gridplugin').editor.form.findField(di).enable() : null);
                }
            } else {
                var di;
                for (var i = 0; i < context.grid.columns.length; i++) {
                    di = context.grid.columns[i].dataIndex;
                    if (context.record.data[di])
                        context.grid.columns[i].disableEditField ? (di != null ? context.grid.getPlugin('gridplugin').editor.form.findField(di).enable() : null) : (di != null ? context.grid.getPlugin('gridplugin').editor.form.findField(di).enable() : null);

                }
            }
        }
    },

    onRowEditingCanceledit: function(editor, context, eOpts) {
        this.editType = 0;
        var sm = context.grid.getSelectionModel().getLastSelected();
        if (sm.get(this.cancelObject) === "")
            context.grid.getStore().remove(sm);

        if (this.addflg != -1) {
            this.flg = this.editflg;
        }

    },

    onRowEditingValidateedit: function(editor, context, eOpts) {
        this.editType = 0;
    },



    setRefreshing: function(btn) {
        Ext.ComponentQuery.query('#refbtnId')[0].setText(btn.type ? btn.type + ' ثانیه' : 'دستی');
        if (this.timer)
            window.clearInterval(this.timer);
        if (btn.type) {
            var thisthis = this;
            this.timer = window.setInterval(function() {
                //alert("hereeee");
                thisthis.getStore().reload();
            }, btn.type * 1000);
        } else
            this.getStore().reload();
    },

    getSearchValue: function() {
        var me = this,
            value = me.textField.getValue();

        if (value === '') {
            return null;
        }
        if (!me.regExpMode) {
            value = value.replace(me.regExpProtect, function(m) {
                return '\\' + m;
            });
        } else {
            try {
                new RegExp(value);
            } catch (error) {
                /*
                              me.statusBar.setStatus({
                                  text: error.message,
                                  iconCls: 'x-status-error'
                              });*/
                return null;
            }
            // this is stupid
            if (value === '^' || value === '$') {
                return null;
            }
        }

        return value;
    },

    onTextFieldChange: function() {
        var me = this,
            count = 0;

        me.view.refresh();
        // reset the statusbar
        /*      
	me.statusBar.setStatus({
             text: me.defaultStatusText,
             iconCls: ''
         });
	*/
        me.searchValue = me.getSearchValue();
        me.indexes = [];
        me.currentIndex = null;

        if (me.searchValue !== null) {
            me.searchRegExp = new RegExp(me.searchValue, 'g' + (me.caseSensitive ? '' : 'i'));


            me.store.each(function(record, idx) {
                var td = Ext.fly(me.view.getNode(idx)).down('td'),
                    cell, matches, cellHTML;
                while (td) {
                    cell = td.down('.x-grid-cell-inner');
                    matches = cell.dom.innerHTML.match(me.tagsRe);
                    cellHTML = cell.dom.innerHTML.replace(me.tagsRe, me.tagsProtect);

                    // populate indexes array, set currentIndex, and replace wrap matched string in a span
                    cellHTML = cellHTML.replace(me.searchRegExp, function(m) {
                        count += 1;
                        if (Ext.Array.indexOf(me.indexes, idx) === -1) {
                            me.indexes.push(idx);
                        }
                        if (me.currentIndex === null) {
                            me.currentIndex = idx;
                        }
                        return '<span class="' + me.matchCls + '">' + m + '</span>';
                    });
                    // restore protected tags
                    Ext.each(matches, function(match) {
                        cellHTML = cellHTML.replace(me.tagsProtect, match);
                    });
                    // update cell html
                    cell.dom.innerHTML = cellHTML;
                    td = td.next();
                }
            }, me);

            // results found
            if (me.currentIndex !== null) {
                me.getSelectionModel().select(me.currentIndex);
                /*
                               me.statusBar.setStatus({
                                   text: count + ' matche(s) found.',
                                   iconCls: 'x-status-valid'
                               });*/
            }
        }

        // no results found
        if (me.currentIndex === null) {
            me.getSelectionModel().deselectAll();
        }

        // force textfield focus
        me.textField.focus();
    },

    onPreviousClick: function() {
        var me = this,
            idx;

        if ((idx = Ext.Array.indexOf(me.indexes, me.currentIndex)) !== -1) {
            me.currentIndex = me.indexes[idx - 1] || me.indexes[me.indexes.length - 1];
            me.getSelectionModel().select(me.currentIndex);
        }
    },

    /**
     * Selects the next row containing a match.
     * @private
     */
    onNextClick: function() {
        var me = this,
            idx;

        if ((idx = Ext.Array.indexOf(me.indexes, me.currentIndex)) !== -1) {
            me.currentIndex = me.indexes[idx + 1] || me.indexes[0];
            me.getSelectionModel().select(me.currentIndex);
        }
    },

    /**
     * Switch to case sensitive mode.
     * @private
     */
    caseSensitiveToggle: function(checkbox, checked) {
        this.caseSensitive = checked;
        this.onTextFieldChange();
    },

    /**
     * Switch to regular expression mode
     * @private
     */
    regExpToggle: function(checkbox, checked) {
        this.regExpMode = checked;
        this.onTextFieldChange();
    }
});
