Ext.define('Ext.ux.twinCombo', {
    extend: 'Ext.form.field.ComboBox',


    requires: [
        'Ext.form.field.Spinner'

    ],
    alias: 'widget.twincombo',

    customTriggerCls: '',
    winToShowID: '',
    windowIconCls: '',
    winConfigs: null,

    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {

            trigger2Cls: me.customTriggerCls

        });
        me.callParent(arguments);
    },

    /*onTrigger1Click : function() 
    { 
    	alert('One');	
    },*/

    onTrigger2Click: function() {
        var desktop = myGlobalAppVar.getDesktop();
        var win = desktop.getWindow(this.winToShowID);
        if (!win) {
            win = Ext.create(this.winToShowID, this.winConfigs);
            win.collapsed = false;
            win.collapsible = false;
            win.modal = true;
        }
        win.show();
    }

});
