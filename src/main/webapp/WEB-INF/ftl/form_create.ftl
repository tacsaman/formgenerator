<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create a new form</title>
</head>
<body>
<nav role="navigation">
    <ul>
        <li><a href="/">Home</a></li>
    </ul>
</nav>

<h1>Create a new form</h1>

<form role="form" name="form" action="" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <div>
        <label for="formName">Form Name</label>
        <input type="text" name="formName" id="for" value="${form.formName}" required autofocus/>
    </div>
    <div>
        <label for="label">Label</label>
        <input type="text" name="label" id="label" value="${form.elementName}" required autofocus/>
    </div>
    <div>
        <label for="size">Size</label>
        <input type="text" name="size" id="size" value="" required/>
    </div>
    <div>
        <label for="elementType">Type</label>
        <select name="elementType" id="elementType" required>
            <option <#if form.elementType == 'TEXTBOX'>selected</#if>>TEXTBOX</option>
            <option <#if form.elementType == 'RADIOBUTTON'>selected</#if>>RADIOBUTTON</option>
        </select>
    </div>
    <button type="submit">Save</button>
</form>

<@spring.bind "form" />
<#if spring.status.error>
<ul>
    <#list spring.status.errorMessages as error>
        <li>${error}</li>
    </#list>
</ul>
</#if>
</body>
</html>