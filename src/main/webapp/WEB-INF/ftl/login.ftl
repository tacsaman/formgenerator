<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>صفحه ورود کاربران</title>
		<link rel="stylesheet" href="login/resources/css/login.css" />
		<link rel="stylesheet" href="login/resources/css/jquery.ibutton.min.css" />
		<script src="login/resources/js/jquery-1.8.2.min.js"></script>
		<script src="login/resources/js/login.js"></script>
		<script src="login/resources/js/jquery.ibutton.min.js"></script>
		<script src="login/resources/js/jquery.metadata.js"></script>
		<script src="login/resources/js/jquery.easing.1.3.js"></script>
		<script src="login/RSA/login.js"></script>
	</head>
	<body>
	<div id="login-overlay">
		<div id="formContainer">
			<form id="login" method="post" action="/login">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <center><div class="err-label"></div></center>
				<input type="text" class="logfield username"  name="email" id="username" placeholder="--نام کاربری--" />
				<input type="password" class="logfield password" name="password" id="password" value="" placeholder="--رمز عبور--" />
				<input id="submitBtn" type="submit" name="submit" value="ورود" />
			</form>
			<form id="recover" method="post" action="./">
				<div id="loading-status">
				<div id="loading-spin"></div>
				<div id="loading-cmp"></div>				
				</div>
				<div class="err-label"></div>
				<input type="submit" name="submit" id="tryagain" value="Try Again" />
			</form>
		</div>
	</div>
    <div id="load">
    </div>    
    </body>
</html>

