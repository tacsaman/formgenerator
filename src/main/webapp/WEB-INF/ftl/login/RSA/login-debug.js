var token;

function get(id) {
    return document.getElementById(id);
}

function onWinLoad() {
    get('username').select();
}

function keypress(event) {
    var obj = get("username");
    var evt = event
    var returnCode = true;
    var keyCode;
    if ("which" in evt) { // NN4 & FF &amp; Opera
        keyCode = evt.which;
    } else if ("keyCode" in evt) { // Safari & IE4+
        keyCode = evt.keyCode;
    } else if ("keyCode" in window.event) { // IE4+
        keyCode = window.event.keyCode;
    } else if ("which" in window.event) {
        keyCode = evt.which;
    } else alert("the browser don't support");
    if (keyCode == 13) {
        //			evt.stopEvent();
        return onClick();
    }
    return true;
}

function GetXmlHttpObject() {
    var xmlHttp = null;
    try {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    } catch (e) {
        //Internet Explorer
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}

function onClick() {
    var usernameField = get("username");
    var username = usernameField.value;
    var pwdField = get("password");
    var pwd = pwdField.value;
    var msg = '';
    if (validate(username) === false) {
        msg = "Please enter the username";
    }
    if (validate(pwd) === false) {
        msg += msg ? " and password" : "Please enter the password";
    }
    if (msg) {
        get('err-label').innerHTML = msg + '.';
        return false;
    }
    username = encode(username);
    pwd = encode(pwd);
    username = encryptedString(key, username);
    pwd = encryptedString(key, pwd);
    refresh(username, pwd);
    stateChanged();
}

function onLoading(v) {
    get("username").disabled = v;
    get("password").disabled = v;
    get("submitBtn").disabled = v;
    if (!v) get("username").select();
    else get('err-label').innerHTML = '';
    get("loading").style.visibility = !v ? 'hidden' : 'visible';
}

function refresh(username, pass) {
    xmlHttp = GetXmlHttpObject();
    if (xmlHttp == null) {
        alert("Your browser does not support AJAX.\nPlease update your browser.");
        return;
    }
    onLoading(true);
    var ajaxreq = "id=login&name=" + username + "&word=" + pass;
    url = '/cgi-bin/logincgi.cgi?' + ajaxreq;
    xmlHttp.onreadystatechange = stateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send();
}

function stateChanged() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        var resp = eval('(' + xmlHttp.responseText + ')');

        if (resp.success == true) {
            //get('username').value = get('password').value = '';
            get('token').value = resp.token;
            //window.location = 'index.html#'+resp.token;
            get('login-form').submit();
        } else {
            onLoading(false);
            if (resp.errors) get('err-label').innerHTML = resp.errors;
            else get('err-label').innerHTML = "An unknown error occurred.";
        }

    }
}

function validate(field) {
    return field !== "";
}
