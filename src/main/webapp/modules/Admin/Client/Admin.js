Ext.define('MyDesktop.Modules.Admin.Client.Admin', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'admin-win',
    init : function(){
        this.launcher = {
            text: 'مدیریت',
            iconCls:'icon-management',
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('admin-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'admin-win',									
                title:'مدیریت',								
                width:1000,										
                height:550,											
                iconCls: 'icon-management',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Admin/Client/tree-data.json' , prefix: 'Admin'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
