Ext.define('MyDesktop.Modules.Admin.Client.subModule.Settings.view.MainView', {
	extend: 'Ext.container.Container',
	requires: [
		'Ext.ux.customActionCol'
	],
	title: 'تنظیمات',
	itemId: 'admin-settings-panel',
	layout: 'fit',
	initComponent: function() {
		var me = this;
		settingsForm = this;
		settingsForm.connection = this.connection;
		Ext.applyIf(me, {
			items: [{
				xtype: 'panel',
				itemId: 'admin-settings-panel-main',
				layout: 'fit',
				items: [{
					xtype: 'customform',
					layout: 'auto',
					autoLoad: false,
					autoScroll: true,
					layout: 'auto',
					bodyPadding: 10,
					title: '',
					itemId: 'admin-settings-form',
					submitUrl: '',
					saveCallback: settingsForm.callback,
					saveItemId: 'admin-settings-save-btn',
					hideResetBtn: true,
					hideSaveBtn: true,
					defaultSave: false,
					defaultReset: true,
					items: [{
						xtype: 'container',
						margin: '10 0 10 0',
						layout: 'table',
						items: [{
							xtype: 'datefield',
							flex: 1,
							fieldLabel: 'تاریخ',
							itemId: 'admin-settings-setDate-txt',
							name: 'value',
						}, {
							xtype: 'button',
							itemId: 'admin-settings-setDate-btn',
							text: 'تنظیم تاریخ'
						}]
					}, {
						xtype: 'container',
						margin: '10 0 10 0',
						layout: 'table',
						items: [{
							xtype: 'button',
							itemId: 'admin-settings-getDate-btn',
							text: 'نمایش تاریخ'
						}, {
							xtype: 'label',
							flex: 1,
							fieldLabel: 'تاریخ',
							itemId: 'admin-settings-getDate-txt',
							name: 'value',
						}]
					}]
				}]
			}]
		});
		me.callParent(arguments);
	}

});
