Ext.define('MyDesktop.Modules.Admin.Client.subModule.Settings.controller.init', {
    extend: 'Ext.app.Controller',

    init: function(application) {

        var controller=Ext.create('MyDesktop.Modules.Admin.Client.subModule.Settings.controller.MainController');
        controller.init();
    }

});
