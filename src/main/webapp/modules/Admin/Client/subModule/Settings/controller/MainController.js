Ext.define('MyDesktop.Modules.Admin.Client.subModule.Settings.controller.MainController', {
    extend: 'Ext.app.Controller',
    onSetDateclick: function(dv, record, item, index, e) {
        var date = Ext.ComponentQuery.query('#admin-settings-setDate-txt')[0];
		var merge = Ext.apply({
				"date": Ext.Date.format(Ext.ComponentQuery.query('#admin-settings-setDate-txt')[0].getValue(), 'M d, Y h:m:s A')
		});
		var ajax = Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json'
			},
			url: 'cxf/rest/date/setServerDate',
			method: 'POST',
			async: false,
			params: Ext.encode(merge),
			success: function(response) {
				var decoded = Ext.decode(response.responseText);
				var me = this;
				if (decoded.errorMessage){
					Ext.create('widget.uxNotification', {
						title: 'وضعیت',
						corner: 'tr',
						stickOnClick: false,
						manager: 'demo1',
						width: 250,
						height: 100,
						iconCls: 'ux-notification-icon-information',
						html: decoded.errorMessage
					}).show();
				}
				else if (decoded.success && decoded.data){ 
				}
			}
		});
    },
    onGetDateclick: function(dv, record, item, index, e) {
		var ajax = Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json'
			},
			url: 'cxf/rest/date/getServerDate',
			method: 'GET',
			async: false,
			success: function(response) {
				var decoded = Ext.decode(response.responseText);
				var me = this;
				if (decoded.errorMessage){
					Ext.create('widget.uxNotification', {
						title: 'وضعیت',
						corner: 'tr',
						stickOnClick: false,
						manager: 'demo1',
						width: 250,
						height: 100,
						iconCls: 'ux-notification-icon-information',
						html: decoded.errorMessage
					}).show();
				}
				else if (decoded.success && decoded.data){ 
					console.log(decoded)
					Ext.ComponentQuery.query('#admin-settings-getDate-txt')[0].setText(decoded.data);
				}
			}
		});    },
    init: function(application) {
        this.control({
            "#admin-settings-setDate-btn": {
                click: this.onSetDateclick
            },
            "#admin-settings-getDate-btn": {
                click: this.onGetDateclick
            }
        });
    }


});
