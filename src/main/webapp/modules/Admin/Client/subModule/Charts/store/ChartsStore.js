Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.store.ChartsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChartsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				
      	    model: 'MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChartsModel', 
            storeId: 'ChartsStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/orgPos/getRootNodes',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
