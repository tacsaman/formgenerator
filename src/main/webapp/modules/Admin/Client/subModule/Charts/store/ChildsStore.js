Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.store.ChildsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChildsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
      	    model: 'MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChildsModel', 
            storeId: 'ChartsStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/orgPos/getChildren',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
