Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.controller.MainController', {
    extend: 'Ext.app.Controller',
    onitemclick: function(dv, record, item, index, e) {
        var childsTree = Ext.ComponentQuery.query('#Admin-Charts-childs-grid')[0];
        var chartsGridRow = Ext.ComponentQuery.query('#Admin-Charts-grid')[0].getSelectionModel().getSelection();
        childsTree.store.proxy.extraParams.id = chartsGridRow[0].data.id;
        childsTree.store.reload();
    },
    onaftereditChildsgrid: function() {
        var chart = Ext.ComponentQuery.query('#Admin-Charts-grid')[0].getSelectionModel().getSelection();
console.log(chart[0].data.id)
        var Child = Ext.ComponentQuery.query('#Admin-Charts-childs-grid')[0];//.getSelectionModel().getSelection();
        Child.editObject.parent = chart[0].data.id;
console.log(Child.editObject)
    },
    init: function(application) {
        this.control({
            "#Admin-Charts-grid": {
                itemclick: this.onitemclick
            },
            "#Admin-Charts-childs-grid": {
                beforeedit: this.onaftereditChildsgrid
            },
        });
    }


});
