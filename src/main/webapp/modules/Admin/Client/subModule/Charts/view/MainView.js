Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'جایگاه سازمانی',
    itemId: 'admin-charts-panel',
    layout: 'fit',
    initComponent: function() {
        var me = this;
        zonesForm = this;
        zonesForm.connection = this.connection;
        var ChartsStore = Ext.create('MyDesktop.Modules.Admin.Client.subModule.Charts.store.ChartsStore');
        var ChildsStore = Ext.create('MyDesktop.Modules.Admin.Client.subModule.Charts.store.ChildsStore');
        Ext.applyIf(me, {
            items: [{
                xtype: 'panel',
                itemId: 'admin-charts-panel-main',
                layout: 'border',
                items: [{
                    xtype: 'panel',
                    region: 'center',
                    layout: 'fit',
                    items: [{
                        id: 'Admin-Charts-grid',
                        xtype: 'customgrid',
                        store: ChartsStore,
                        model: 'MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChartsModel',
                        forcefit: true,
                        itemId: 'Admin-Charts-grid',
                        newBtnItemId: 'admin-Admin-grid-addBtn',
                        newBtnText: 'اضافه کردن جایگاه سازمانی',
                        newBtnTip: 'اضافه کردن جایگاه سازمانی',
                        hidesearchbar: true,
                        hidePaging: true,
                        hideRefreshBtn: false,
                        editable: true,
                        cancelObject: 'displayName',
                        showRowonNew: true,
                        editkeyArray: [
                            ['id', 'id'],
                            ['displayName', 'displayName']
                        ],
                        editObject: {},
                        addRecord: {
                            displayName: ''
                        },
                        editURL: 'cxf/rest/orgPos/update',
                        addURL: 'cxf/rest/orgPos/save',
                        columns: [{
                            xtype: 'gridcolumn',
                            dataIndex: 'id',
                            flex: 1,
                            text: 'کد',
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'parent',
                            flex: 1,
                            text: 'پدر',
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'displayName',
                            flex: 1,
                            text: 'نام',
                            editor: {
                                xtype: 'textfield'
                            }
                        }, {
                            xtype: 'customactioncol',
                            flex: 1,
                            itemId: 'admin-Admin-grid-customactioncolumn',
                            deleteurl: 'cxf/rest/orgPos/delete',
                            deletetip: 'حذف جایگاه سازمانی',
                            alwaysShowDelete: true,
                            text: 'عملیات',
                            deleteobject: {},
                            deletekeyArray: [
                                [
                                    'id', 'id'
                                ]
                            ],
                            deletable: true,
                            editable: false,
                            defaultCustomEdit: false
                        }]
                    }]
                }, {
                    xtype: 'panel',
                    id: 'admin-Charts-Childs-grid-panel',
                    height: 200,
                    layout: {
                        type: 'fit'
                    },
                    collapsed: true,
                    collapsible: true,
                    region: 'south',
                    items: [{
                        xtype: 'panel',
                        title: 'زیر مجموعه',
                        layout: 'fit',
                        width: 300,
                        autoScroll: true,
                        items: [{
                            id: 'admin-Charts-Childs-grid',
                            xtype: 'customgrid',
                            //defaultEdit: true,
                            store: ChildsStore,
                            model: 'MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChildsModel',
                            forcefit: true,
                            itemId: 'Admin-Charts-childs-grid',
		                    newBtnItemId: 'admin-Admin-grid-childs-addBtn',
		                    newBtnText: 'اضافه کردن زیرمجموعه',
		                    newBtnTip: 'اضافه کردن زیرمجموعه',
		                    hidesearchbar: true,
		                    hidePaging: true,
		                    hideRefreshBtn: false,
		                    editable: true,
		                    cancelObject: 'displayName',
		                    showRowonNew: true,
		                    editkeyArray: [
		                        ['id', 'id'],
		                        ['displayName', 'displayName']
		                    ],
		                    editObject: {},
		                    addRecord: {
		                        displayName: ''
		                    },
		                    editURL: 'cxf/rest/orgPos/update',
		                    addURL: 'cxf/rest/orgPos/save',

		                    columns: [{
		                        xtype: 'gridcolumn',
		                        dataIndex: 'id',
		                        flex: 1,
		                        text: 'کد',
		                    }, {
		                        xtype: 'gridcolumn',
		                        dataIndex: 'displayName',
		                        flex: 1,
		                        text: 'نام',
		                        editor: {
		                            xtype: 'textfield'
		                        }
		                    }, {
		                        xtype: 'customactioncol',
		                        flex: 1,
		                        itemId: 'admin-Admin-childs-grid-customactioncolumn',
		                        deleteurl: 'cxf/rest/orgPos/delete',
		                        deletetip: 'حذف زیرمجموعه',
		                        alwaysShowDelete: true,
		                        text: 'عملیات',
		                        deleteobject: {},
		                        deletekeyArray: [
		                            [
		                                'id', 'id'
		                            ]
		                        ],
		                        deletable: true,
		                        editable: false,
		                        defaultCustomEdit: false
		                    }]
                        }]
                    }]
                }]
            }]
        });
        me.callParent(arguments);
    }

});
