Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.controller.init', {
    extend: 'Ext.app.Controller',

    init: function(application) {

        var controller=Ext.create('MyDesktop.Modules.Admin.Client.subModule.Charts.controller.MainController');
        controller.init();
    }

});
