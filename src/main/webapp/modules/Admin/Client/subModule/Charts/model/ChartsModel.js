Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChartsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
		{
            name: 'treeName'
        },
        {
            name: 'displayName'
        },
        {
            name: 'parent'
        }
    ]
});
