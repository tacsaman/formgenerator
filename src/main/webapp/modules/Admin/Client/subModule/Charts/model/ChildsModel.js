Ext.define('MyDesktop.Modules.Admin.Client.subModule.Charts.model.ChildsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
		{
            name: 'displayName'
        }
    ]
});
