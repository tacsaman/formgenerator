Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.controller.init', {
    extend: 'Ext.app.Controller',

    init: function(application) {

        var controller=Ext.create('MyDesktop.Modules.Admin.Client.subModule.Roles.controller.MainController');
        controller.init();
    }

});
