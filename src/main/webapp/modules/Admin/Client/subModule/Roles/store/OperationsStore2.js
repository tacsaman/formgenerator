Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.store.OperationsStore', {
    extend: 'Ext.data.TreeStore',

    requires: [
        'MyDesktop.Modules.Admin.Client.subModule.Roles.model.OperationsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				
      	    model: 'MyDesktop.Modules.Admin.Client.subModule.Roles.model.RolesModel', 
            storeId: 'OperationsStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/role/getRolePermissions',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});

