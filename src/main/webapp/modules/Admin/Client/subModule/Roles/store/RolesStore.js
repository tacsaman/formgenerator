Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.store.RolesStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Admin.Client.subModule.Roles.model.RolesModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
		var totalCount=-1;
        me.callParent([Ext.apply({
            autoLoad: true,
      	    model: 'MyDesktop.Modules.Admin.Client.subModule.Roles.model.RolesModel', 
            storeId: 'RolesStore',
            proxy: {
                type: 'ajax',
				paramsAsJson:true,
				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                method:'POST',
                url:'cxf/rest/role/searchByExample',
                reader: {
                    type: 'json',root: 'data'
                }
            }
			,listeners: {
				load: function(store, operation, eOpts) {
				if (totalCount == -1)
					var ajax = Ext.Ajax.request({
						headers: {'Content-Type': 'application/json'},
						url: 'cxf/rest/role/count',
						method: 'GET',
						async: false,
						success: function(response) {
							var decoded = Ext.decode(response.responseText);
							store.totalCount=decoded.data;
							totalCount=decoded.data;
						}
					});
				else
					store.totalCount=totalCount;
				}
			}
        }, cfg)]);
    }
});
