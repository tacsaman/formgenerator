Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.controller.MainController', {
    extend: 'Ext.app.Controller',
    onitemclick: function(dv, record, item, index, e) {
        var role = Ext.ComponentQuery.query('#Admin-Roles-grid')[0].getSelectionModel().getSelection();
        var operationTree = Ext.ComponentQuery.query('#admin-role-operation-tree')[0];
        operationTree.store.proxy.extraParams.roleId = role[0].data.id;
        operationTree.store.load();
    },
    init: function(application) {
        this.control({
            "#Admin-Roles-grid": {
                itemclick: this.onitemclick
            }
        });
    }


});
