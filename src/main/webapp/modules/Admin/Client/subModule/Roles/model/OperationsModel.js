Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.model.OperationsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'roleId'
        },
        {
            name: 'operation'
        },
        {
            name: 'checked',type:'boolean'
        },
        {
            name: 'leaf',type:'boolean'
        },
        {
            name: 'type'
        },
        {
            name: 'text'
        },
        {
            name: 'type'
        }

    ]
});
