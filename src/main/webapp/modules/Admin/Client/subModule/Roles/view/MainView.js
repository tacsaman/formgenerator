Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.view.MainView', {
	extend: 'Ext.container.Container',
	requires: [
		'Ext.ux.customActionCol'
	],
	title: 'نقش',
	itemId: 'admin-roles-panel',
	layout: 'fit',
	initComponent: function() {
		var me = this;
		zonesForm = this;
		zonesForm.connection = this.connection;
		var RolesStore = Ext.create('MyDesktop.Modules.Admin.Client.subModule.Roles.store.RolesStore');
		var OperationsStore = Ext.create('MyDesktop.Modules.Admin.Client.subModule.Roles.store.OperationsStore');
		Ext.applyIf(me, {
			items: [{
				xtype: 'panel',
				itemId: 'admin-roles-panel-main',
				layout: 'border',
				items: [{
					xtype: 'panel',
					region: 'center',
					layout: 'fit',
					items: [{
						id: 'Admin-Roles-grid',
						xtype: 'customgrid',
						store: RolesStore,
						model: 'MyDesktop.Modules.Admin.Client.subModule.Roles.model.RolesModel',
						forcefit: true,
						itemId: 'Admin-Roles-grid',
						newBtnItemId: 'admin-Admin-grid-addBtn',
						newBtnText: 'اضافه کردن نقش',
						newBtnTip: 'اضافه کردن نقش',
						hidesearchbar: true,
						hidePaging: false,
						hideRefreshBtn: false,
						editable: true,
						cancelObject: 'name',
						showRowonNew: true,
						editkeyArray: [
							['id', 'id'],
							['name', 'name'],
							['description', 'description'],
							['enabled', 'enabled']
						],
						editObject: {},
						addRecord: {
							name: '',
							description: ''
						},
						cancelObject: 'name',
						editURL: 'cxf/rest/role/update',
						addURL: 'cxf/rest/role/save',

						columns: [{
							xtype: 'gridcolumn',
							dataIndex: 'id',
							flex: 1,
							text: 'کد',
						}, {
							xtype: 'gridcolumn',
							dataIndex: 'name',
							flex: 1,
							text: 'نام',
							editor: {
								xtype: 'textfield',
								allowBlank: false,
							}
						}, {
							xtype: 'gridcolumn',
							dataIndex: 'description',
							flex: 1,
							text: 'توصیف',
							editor: {
								xtype: 'textfield',
								allowBlank: false,
							}
						}, {
							xtype: 'gridcolumn',
							dataIndex: 'enabled',
							flex: 1,
							text: 'فعال',
							editor: {
								xtype: 'checkboxfield',
								name: 'enabled'
							}
						}, {
							xtype: 'customactioncol',
							flex: 1,
							itemId: 'admin-Admin-grid-customactioncolumn',
							deleteurl: 'cxf/rest/role/delete',
							deletetip: 'حذف نقش',
							alwaysShowDelete: true,
							text: 'عملیات',
							deleteobject: {},
							deletekeyArray: [
								[
									'id', 'id'
								]
							],
							deletable: true,
							editable: false,
							defaultCustomEdit: false
						}]
					}]
				}, {
					xtype: 'panel',
					id: 'admin-Roles-Roles-grid-panel',
					height: 200,
					layout: {
						type: 'fit'
					},
					collapsed: true,
					collapsible: true,
					region: 'south',
					items: [{
						id: 'admin-role-operation-tree',
						itemId: 'admin-role-operation-tree',
						rootVisible: false,
						xtype: 'treepanel',
						layout: 'fit',
						store: OperationsStore,
						listeners: {
							beforeload: function(store, operation, eOpts) {
								var nodeType = operation.node.raw.type;
								if (operation.node.raw.operation) {
									if (nodeType == "operation")
										operation.params.operationTreeName = operation.node.raw.operation.operationTreeName;
									else {
										operation.params.operationTreeName = operation.node.raw.operation.operationTreeName;
										operation.params.targetTreeName = operation.node.raw.target.targetTreeName;
									}
								}
							},
							checkchange: this.onCheckChange
						},
						tbar: [{
							text: 'ذخیره',
							scope: this,
							handler: this.onCheckedNodesClick
						}]
					}]
				}]
			}]
		});
		me.callParent(arguments);
	},
	onCheckChange: function(node, checked) {
		if (node.get('leaf') && checked) {
			node = node.parentNode;
			var siblingStateEqual = true;
			node.cascadeBy(function(n) {
				if (n != node) {
					if (n.get('checked') != checked) {
						siblingStateEqual = false;
					}
				}
			});

			if (siblingStateEqual == checked) {
				node.set('checked', checked);
			}

		} else {
			if (checked == false) {
				if (node.parentNode != null && node.parentNode.data.checked == true) {
					node.parentNode.set('checked', checked);
					arguments.callee(node.parentNode, false)
				}
			} else {
				node.cascadeBy(function(n) {
					n.set('checked', checked);
				});
			}
		}

	},
	onCheckedNodesClick: function() {
		var box = Ext.MessageBox.wait('Please wait...', 'Saving');
		var records = Ext.ComponentQuery.query('#admin-role-operation-tree')[0].getView().getChecked(),
			name = [];
		names = [];

		Ext.Array.each(records, function(rec) {
			if (!rec.parentNode.data.checked)
				name.push(rec.raw);

		});

		//		names.push({"operation":name});
		Ext.Ajax.request({
			url: 'cxf/rest/role/updateRolePermissions',
			headers: {
				'Content-Type': 'application/json'
			},
			params: Ext.encode(name),
			scope: this,
			success: function(result, request) {
				if (result.status == 200) {
					var decoded = Ext.decode(result.responseText);
					if (!decoded.success) Ext.Msg.alert('خطا', decoded.errorMessage);
				}
				box.hide();
			},
			failure: function(response) {
				console.log(response)
				box.hide();
			}
		});
	}


});
