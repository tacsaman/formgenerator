Ext.define('MyDesktop.Modules.Admin.Client.subModule.Roles.model.RolesModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'name'
        },
        {
            name: 'description'
        },
        {
            name: 'enabled',type:'boolean'
        }    ]
});
