Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'کاربر',
    itemId: 'admin-users-panel',
    layout: 'fit',
    initComponent: function() {
        var me = this;
        zonesForm = this;
        zonesForm.connection = this.connection;
        var UsersStore = Ext.create('MyDesktop.Modules.Admin.Client.subModule.Users.store.UsersStore');
        var RolesStore = Ext.create('MyDesktop.Modules.Admin.Client.subModule.Users.store.RolesStore');
        Ext.applyIf(me, {
            items: [{
                xtype: 'panel',
                itemId: 'admin-users-panel-main',
                layout: 'border',
                items: [{
                    xtype: 'panel',
                    region: 'center',
                    layout: 'fit',
                    items: [{
                        id: 'Admin-Users-grid',
                        xtype: 'customgrid',
                        store: UsersStore,
                        model: 'MyDesktop.Modules.Admin.Client.subModule.Users.model.UsersModel',
                        forcefit: true,
                        itemId: 'Admin-Users-grid',
                        newBtnItemId: 'admin-Admin-grid-addBtn',
                        newBtnText: 'اضافه کردن کاربر',
                        newBtnTip: 'اضافه کردن کاربر',
                        hidesearchbar: false,
                        hidePaging: false,
                        hideRefreshBtn: false,
                        editable: true,
                        cancelObject: 'username',
                        showRowonNew: true,
                        editkeyArray: [
                            ['id', 'id'],
                            ['username', 'username'],
                            ['email', 'email'],
                            ['lastName', 'lastName'],
                            ['password', 'password'],
                            ['enabled', 'enabled']
                        ],
                        editObject: {},
                        addRecord: {
                            username: '',
                            email: '',
                            lastName: '',
                            password: ''
                        },
                        editURL: 'cxf/rest/user/update',
                        addURL: 'cxf/rest/user/save',
                        columns: [{
                            xtype: 'gridcolumn',
                            dataIndex: 'enabled',
                            flex: 1,
                            text: 'فعال',
                            editor: {
                                xtype: 'checkboxfield',
                                id: 'Admin-Users-chkbox',
                                itemId: 'Admin-Users-chkbox',
                                name: 'enabled'
                            }
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'id',
                            flex: 1,
                            text: 'کد',
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'username',
                            flex: 1,
                            text: 'نام کاربری',
                            filterable: true,
                            filter: {
                                type: 'string'
                                    // specify disabled to disable the filter menu
                                    //, disabled: true
                            },
                            editor: {
                                xtype: 'textfield',
                                allowBlank: false,
                            }
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'email',
                            flex: 1,
                            text: 'نام',
                            editor: {
                                xtype: 'textfield'
                            }
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'lastName',
                            flex: 1,
                            text: 'نام خانوادگی',
                            editor: {
                                xtype: 'textfield'
                            }
                        }, {
                            xtype: 'gridcolumn',
                            dataIndex: 'password',
                            flex: 1,
                            text: 'رمز عبور',
                            editor: {
                                xtype: 'textfield'
                            }
                        }, {
                            xtype: 'customactioncol',
                            flex: 1,
                            itemId: 'admin-Admin-grid-customactioncolumn',
                            deleteurl: 'cxf/rest/user/delete',
                            deletetip: 'حذف کاربر',
                            alwaysShowDelete: true,
                            text: 'عملیات',
                            deleteobject: {},
                            deletekeyArray: [
                                [
                                    'id', 'id'
                                ]
                            ],
                            deletable: true,
                            editable: false,
                            defaultCustomEdit: false
                        }]
                    }]
                }, {
                    xtype: 'panel',
                    id: 'admin-Users-Roles-grid-panel',
                    height: 200,
                    layout: {
                        type: 'fit'
                    },
                    collapsed: true,
                    collapsible: true,
                    region: 'south',
                    items: [{
                        xtype: 'panel',
                        title: 'نقش',
                        layout: 'fit',
                        width: 300,
                        autoScroll: true,
                        items: [{
                            id: 'admin-Users-Roles-grid',
                            xtype: 'customgrid',
                            //defaultEdit: true,
                            store: RolesStore,
                            model: 'MyDesktop.Modules.Admin.Client.subModule.Users.model.RolesModel',
                            forcefit: true,
                            itemId: 'Admin-Users-roles-grid',
                            hidesearchbar: true,
                            hidePaging: true,
                            hideNewBtn: true,
                            hideRefreshBtn: false,
                            editable: true,
                            showRowonNew: true,
                            editObject: {},
                            editkeyArray: [
                                ['assigned', 'selected']
                            ],

                            editURL: 'cxf/rest/user/assignRole',

                            columns: [{
                                xtype: 'gridcolumn',
                                dataIndex: 'selected',
                                flex: 1,
                                text: 'فعال',
                                editor: {
                                    xtype: 'checkboxfield',
                                    id: 'Admin-Users-roles-chkbox',
                                    itemId: 'Admin-Users-roles-chkbox',
                                    name: 'selected'
                                }
                            }, {
                                xtype: 'gridcolumn',
                                dataIndex: 'name',
                                flex: 1,
                                text: 'نام',
                            }, {
                                xtype: 'gridcolumn',
                                dataIndex: 'description',
                                flex: 1,
                                text: 'توصیف',

                            }]
                        }]
                    }]
                }]
            }]
        });
        me.callParent(arguments);
    }

});
