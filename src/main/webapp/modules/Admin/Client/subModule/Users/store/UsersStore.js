Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.store.UsersStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Admin.Client.subModule.Users.model.UsersModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
		var totalCount=-1;
        me.callParent([Ext.apply({
            autoLoad: true,
      	    model: 'MyDesktop.Modules.Admin.Client.subModule.Users.model.UsersModel', 
            proxy: {
                type: 'ajax',
				paramsAsJson:true,
				actionMethods: {create: 'POST', read: 'GET', update: 'POST', destroy: 'POST'},
                method:'GET',
                url:'usersList',
                reader: {
                    type: 'json',root: 'data'
                }
            }
			,listeners: {
				load: function(store, operation, eOpts) {
				if (totalCount == -1)
					var ajax = Ext.Ajax.request({
						headers: {'Content-Type': 'application/json'},
						url: 'cxf/rest/user/count',
						method: 'GET',
						async: false,
						success: function(response) {
							var decoded = Ext.decode(response.responseText);
							store.totalCount=decoded.data;
							totalCount=decoded.data;
						}
					});
				else
					store.totalCount=totalCount;
				}
			}
        }, cfg)]);
    }
});
