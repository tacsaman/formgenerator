Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.controller.MainController', {
    extend: 'Ext.app.Controller',
    onitemclick: function(dv, record, item, index, e) {
        var rolesTree = Ext.ComponentQuery.query('#Admin-Users-roles-grid')[0];
        var usersGridRow = Ext.ComponentQuery.query('#Admin-Users-grid')[0].getSelectionModel().getSelection();
        rolesTree.store.proxy.extraParams.id = usersGridRow[0].data.id;
        rolesTree.store.reload();
    },
    onaftereditRolesgrid: function() {
        var user = Ext.ComponentQuery.query('#Admin-Users-grid')[0].getSelectionModel().getSelection();
        var role = Ext.ComponentQuery.query('#Admin-Users-roles-grid')[0];//.getSelectionModel().getSelection();
        var roleRow = role.getSelectionModel().getSelection();
        role.editObject.id = user[0].data.id;
        role.editObject.roleId = roleRow[0].data.id;
    },
    init: function(application) {
        this.control({
            "#Admin-Users-grid": {
                itemclick: this.onitemclick
            },
            "#Admin-Users-roles-grid": {
                beforeedit: this.onaftereditRolesgrid
            },
        });
    }


});
