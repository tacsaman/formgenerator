Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.model.UsersModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id',type:'int'
        },
	{
            name: 'username'
        },
        {
            name: 'email'
        },
        {
            name: 'lastName'
        },
        {
            name: 'password'
        },
        {
            name: 'enabled',type:'boolean'
        }

    ]
});
