Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.model.RolesModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'name'
        },
        {
            name: 'description'
        },
        {
            name: 'selected',type:'boolean'
        }
    ]
});
