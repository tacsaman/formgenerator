Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.store.RolesStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Admin.Client.subModule.Users.model.RolesModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
      	    model: 'MyDesktop.Modules.Admin.Client.subModule.Users.model.RolesModel', 
            storeId: 'UsersStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/user/getUserRoles',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
