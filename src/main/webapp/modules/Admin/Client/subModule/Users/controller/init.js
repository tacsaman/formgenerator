Ext.define('MyDesktop.Modules.Admin.Client.subModule.Users.controller.init', {
    extend: 'Ext.app.Controller',

    init: function(application) {

        var controller=Ext.create('MyDesktop.Modules.Admin.Client.subModule.Users.controller.MainController');
        controller.init();
    }

});
