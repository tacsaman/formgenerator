Ext.define('MyDesktop.Modules.Document.Client.Document', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'document-win',
    init : function(){
        this.launcher = {
            text: 'مستندات',								
            iconCls:'icon-document',								
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('document-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'document-win',									
                title:'مستندات',								
                width:1000,										
                height:550,											
                iconCls: 'icon-document',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Document/Client/tree-data.json' , prefix: 'Document'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
