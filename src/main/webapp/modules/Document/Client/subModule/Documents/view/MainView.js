Ext.define('MyDesktop.Modules.Document.Client.subModule.Documents.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'مستندات',
	 itemId: 'document-documents-panel',

    layout: 'fit',
	initComponent: function() {
	var DocumentsStore = Ext.create('MyDesktop.Modules.Document.Client.subModule.Documents.store.DocumentsStore');	
	var VersionsStore = Ext.create('MyDesktop.Modules.Document.Client.subModule.Documents.store.VersionsStore');
 	var me = this;
	Ext.applyIf(me, {
            items: [{
                xtype: 'panel',
                itemId: 'document-documents-panel-main',
                layout: 'border',
                items: [{
                    xtype: 'panel',
                    region: 'center',
                    layout: 'fit',
                    items: [{
                        id: 'Document-Documents-grid',
                        xtype: 'customgrid',
                        store: DocumentsStore,
                        model: 'MyDesktop.Modules.Document.Client.subModule.Documents.model.DocumentsModel',
                        forcefit: true,
                        itemId: 'Document-Documents-grid',
                        hidesearchbar: true,
					    hideNewBtn: true,
                        hidePaging: true,
                        hideRefreshBtn: false,
                        editable: false,
                        showRowonNew: false,
                        columns: [{
                            xtype: 'gridcolumn',
                            dataIndex: 'code',
                            flex: 1,
                            text: 'کد',
						},{
                            xtype: 'gridcolumn',
                            dataIndex: 'documentName',
                            flex: 1,
                            text: 'نام مستند',
						},{
                            xtype: 'gridcolumn',
                            dataIndex: 'documentType',
                            flex: 1,
                            text: 'نوع مستند',
                        }]
                    }]
                }, {
                    xtype: 'panel',
                    id: 'document-Documents-Versions-grid-panel',
                    height: 200,
                    layout: {
                        type: 'fit'
                    },
                    collapsed: true,
                    collapsible: true,
                    region: 'south',
                    items: [{
                        xtype: 'panel',
                        title: 'ورژن',
                        layout: 'fit',
                        width: 300,
                        autoScroll: true,
                        items: [{
                            id: 'document-Documents-Versions-grid',
                            xtype: 'customgrid',
                            //defaultEdit: true,
                            store: VersionsStore,
                            model: 'MyDesktop.Modules.Document.Client.subModule.Documents.model.VersionsModel',
                            forcefit: true,
                            itemId: 'document-Documents-Versions-grid',
                            hidesearchbar: false,
                            hidePaging: true,
						    hideNewBtn: true,
                            hideRefreshBtn: true,
                            editable: false,
                            showRowonNew: false,
		                    columns: [{
		                        xtype: 'gridcolumn',
		                        dataIndex: 'code',
		                        flex: 1,
		                        text: 'کد',
							},{
		                        xtype: 'gridcolumn',
		                        dataIndex: 'documentName',
		                        flex: 1,
		                        text: 'نام مستند',
							}]
                        }]
                    }]
                }]
            }]
        });
        me.callParent(arguments);
    }

});
