Ext.define('MyDesktop.Modules.Document.Client.subModule.Documents.model.DocumentsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'documentType'
        },
	{
            name: 'code'
        }
	,{
            name: 'documentName'
        }
    ]
});
