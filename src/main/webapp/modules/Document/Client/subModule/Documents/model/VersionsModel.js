Ext.define('MyDesktop.Modules.Document.Client.subModule.Documents.model.VersionsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
      
	{
            name: 'code'
        }
	,{
            name: 'documentName'
        }
    ]
});
