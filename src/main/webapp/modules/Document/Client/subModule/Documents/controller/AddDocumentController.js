Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Documents.controller.AddDocumentController', {
    extend: 'Ext.app.Controller',
    onDocumentsSaveBtnClick: function() {
	},
    onDocumentsNewBtnClick: function() {
        var DocumentAddForm = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Documents.view.DocumentAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        DocumentAddForm.show();
    },
    onDocumentsTayidBtnClick: function() {
    	if (DocumentAddForm.eventName == "formTasvib")
 			var variables = {tasvib:"true"}; 
 		else
 			var variables = {tayid:"true"}; 
		variables = {taskId:DocumentAddForm.taskId,variables:variables}
                var ajax = Ext.Ajax.request({
	            headers: { 'Content-Type': 'application/json' },
                    url: 'cxf/rest/cartable/completeTask',
                    method: 'POST',
                    async: false,
                    params:  Ext.encode(variables),
                    success: function (response) {
                	var decoded = Ext.decode(response.responseText);
				}
			});
	},
    onDocumentsRadBtnClick: function() {
    	if (DocumentAddForm.eventName == "formTasvib")
 			var variables = {tasvib:"false"}; 
 		else
 			var variables = {tayid:"false"}; 
		variables = {taskId:DocumentAddForm.taskId,variables:variables}
                var ajax = Ext.Ajax.request({
	            headers: { 'Content-Type': 'application/json' },
                    url: 'cxf/rest/cartable/completeTask',
                    method: 'POST',
                    async: false,
                    params:  Ext.encode(variables),
                    success: function (response) {
                	var decoded = Ext.decode(response.responseText);
			}
		});
	},
    loadForm: function() {
        var form = Ext.ComponentQuery.query('#cartable-Documents-DocumentsAdd-form')[0];
        thisthis = this;
        if (DocumentAddForm.memberId) {
		var docId= [];
		var docId = {id:DocumentAddForm.memberId}; 
                var ajax = Ext.Ajax.request({
	            headers: { 'Content-Type': 'application/json' },
                    url: 'cxf/rest/document/getById',
                    method: 'GET',
                    async: false,
                    params:  docId,
                    success: function (response) {
                	var decoded = Ext.decode(response.responseText);
				Ext.ComponentQuery.query('#Cartable-Documents-DocumentAdd-documentName-txt')[0].setValue(decoded.data.documentName)
			Ext.ComponentQuery.query('#Cartable-Documents-DocumentAdd-changeCause-txtarea')[0].setValue(decoded.data.changeCause)
			Ext.ComponentQuery.query('#Cartable-Documents-DocumentAdd-documentType-cmb')[0].setValue(decoded.data.documentType)
			Ext.ComponentQuery.query('#Cartable-Documents-DocumentAdd-documentBody-htmleditor')[0].setValue(decoded.data.documentBody)
			Ext.ComponentQuery.query('#cartable-Documents-DocumentAdd-tayid-btn')[0].show()
			Ext.ComponentQuery.query('#cartable-Documents-DocumentAdd-rad-btn')[0].show()
        var form = Ext.ComponentQuery.query('#cartable-Documents-DocumentsAdd-form')[0].submitUrl= 'cxf/rest/document/update';
			}
		});
	} else {
		Ext.ComponentQuery.query('#cartable-Documents-DocumentAdd-tayid-btn')[0].hide()
		Ext.ComponentQuery.query('#cartable-Documents-DocumentAdd-rad-btn')[0].hide()
        }

        form.on({
            actioncomplete: function(form, action) {
                if (action.type == 'load') {
                        var contact = action.result.data;
                        Ext.ComponentQuery.query('#object-Addresses-AddressesAdd-AddressName-txt')[0].setReadOnly(true);
                        Ext.ComponentQuery.query('#object-Addresses-AddressesAdd-addressType-cmb')[0].setReadOnly(true);
                }
            }
        })
    },
    init: function(application) {

        this.control({
            "#cartable-Documents-DocumentAdd-tayid-btn": {
                click: this.onDocumentsTayidBtnClick
            },
            "#cartable-Documents-DocumentAdd-rad-btn": {
                click: this.onDocumentsRadBtnClick
            },
            "#cartable-Documents-DocumentAdd-save-btn": {
                click: this.onDocumentsNewBtnClick
            },
            "#cartable-Documents-DocumentsAdd-form": {
                afterrender: this.loadForm
            }
        });
    }


});
