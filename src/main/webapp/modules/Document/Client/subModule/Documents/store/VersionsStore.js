Ext.define('MyDesktop.Modules.Document.Client.subModule.Documents.store.VersionsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Document.Client.subModule.Documents.model.VersionsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
				model: 'MyDesktop.Modules.Document.Client.subModule.Documents.model.VersionsModel', 
            storeId: 'RegionStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/finalDoc/getHistoryOfDocument',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
