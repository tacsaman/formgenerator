Ext.define('MyDesktop.Modules.Document.Client.subModule.Documents.controller.MainController', {
    extend: 'Ext.app.Controller',
    onDocumentsNewBtnClick: function() {
        var DocumentAddForm = Ext.create('MyDesktop.Modules.Document.Client.subModule.Documents.view.DocumentAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        DocumentAddForm.show();
    },
    onitemclick: function(dv, record, item, index, e) {
        var rolesTree = Ext.ComponentQuery.query('#document-Documents-Versions-grid')[0];
        var usersGridRow = Ext.ComponentQuery.query('#Document-Documents-grid')[0].getSelectionModel().getSelection();
        rolesTree.store.proxy.extraParams.id = usersGridRow[0].data.id;
        rolesTree.store.reload();
    },

    init: function(application) {
        this.control({
            "#document-documents-grid-addBtn": {
                click: this.onDocumentsNewBtnClick
            }
            ,"#Document-Documents-grid": {
                itemclick: this.onitemclick
            },


        });
    }
});
