Ext.define('MyDesktop.Modules.Cartable.Client.Cartable', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'cartable-win',
    init : function(){
        this.launcher = {
            text: 'کارتابل',								
            iconCls:'icon-cartable',								
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('cartable-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'cartable-win',									
                title:'کارتابل',								
                width:1000,										
                height:550,											
                iconCls: 'icon-cartable',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Cartable/Client/tree-data.json' , prefix: 'Cartable'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
