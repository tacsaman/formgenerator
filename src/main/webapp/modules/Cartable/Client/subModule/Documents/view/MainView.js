Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Documents.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'مستندات',
	 itemId: 'cartable-documents-panel',
    layout: 'fit',
	initComponent: function() {
 	var me = this;
	zonesForm = this;
	zonesForm.connection = this.connection;
	var DocumentsStore = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Documents.store.DocumentsStore');
	Ext.applyIf(me, {
            items: [{
                xtype: 'customgrid',
                dock: 'top',
                title: '',
                forcefit: true,
		cancelObject:'name',
                itemId: 'cartable-documents-grid',
                newBtnItemId: 'cartable-documents-grid-addBtn',
                newBtnText: 'اضافه کردن مستند',
                newBtnTip: 'اضافه کردن مستند',
		hidesearchbar: true,
		hidePaging: true,
                hideRefreshBtn: false,
                editable: false,
                showRowonNew: false,
		disableEditRow: true,
                store: DocumentsStore,
                model: 'MyDesktop.Modules.Cartable.Client.subModule.Documents.model.DocumentsModel',
		addURL: 'cxf/rest/switchType/save',
		editURL: 'cxf/rest/switchType/update',	
                columns: [{
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'کد',
                    flex: 1,
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'documentName',
                    text: 'نام',
                    flex: 1,
                }]
            }]
        });
        me.callParent(arguments);
    }

});
