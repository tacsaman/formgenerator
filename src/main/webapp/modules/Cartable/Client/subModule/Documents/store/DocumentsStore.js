Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Documents.store.DocumentsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Cartable.Client.subModule.Documents.model.DocumentsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				model: 'MyDesktop.Modules.Cartable.Client.subModule.Documents.model.DocumentsModel', 
            storeId: 'RegionStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/document/getAll',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
