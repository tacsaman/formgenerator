Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Documents.controller.MainController', {
    extend: 'Ext.app.Controller',
    onDocumentsNewBtnClick: function() {
        var DocumentAddForm = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Documents.view.DocumentAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        DocumentAddForm.show();
    },


    init: function(application) {
        this.control({
            "#cartable-documents-grid-addBtn": {
                click: this.onDocumentsNewBtnClick
            }


        });
    }
});
