Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Documents.view.DocumentAddForm', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.ux.twinCombo'
    ],
    width: 930,
    title: 'تایید مستند',
	 modal: true,
    initComponent: function(arguments) {
        var me = this;
        DocumentAddForm = this;
        DocumentAddForm.formData = this.selectedRecord;
        DocumentAddForm.configType = this.configType;
        DocumentAddForm.saveFlag = this.saveFlag;
        DocumentAddForm.memberId = this.memberId;
        DocumentAddForm.taskId = this.taskId;
        DocumentAddForm.eventName = this.eventName;
        Ext.applyIf(me, {
            items: [{
                xtype: 'customform',
                layout: 'auto',
		autoLoad:false,
                autoScroll: true,
                layout: 'auto',
                bodyPadding: 10,
                title: '',
		itemId:'cartable-Documents-DocumentsAdd-form',
                submitUrl: 'cxf/rest/document/save',
		//saveCallback: DocumentAddForm.callback,
                saveItemId: 'cartable-Documents-DocumentAdd-save-btn',
                hideResetBtn: true,
               // defaultSave: false,
                defaultReset: true,
		toolbarArray: [{
		    text: 'تایید',
		    itemId: 'cartable-Documents-DocumentAdd-tayid-btn',
//		    iconCls: 'icon-save',
		    hidden: true
		}, {
		    text: 'رد',
		    itemId: 'cartable-Documents-DocumentAdd-rad-btn',
//		    iconCls: 'icon-cancel',
		    hidden: true
		}],

                items: [
		{
                        xtype: 'textfield',
                        itemId: 'Cartable-Documents-DocumentAdd-documentName-txt',
                        width: 280,
                        fieldLabel: 'نام',
                        name: 'documentName'
		},
		{
                            xtype: 'combobox',
                            itemId: 'Cartable-Documents-DocumentAdd-documentType-cmb',
                            fieldLabel: 'نوع مستند',
                            name: 'documentType',
                            displayField: 'name',
                            allowBlank: false,
                            emptyText: 'select ...',
                            queryMode: 'local',
                            store: [
                                ['word', 'word'],
                                ['excel', 'excel']
                            ],
                            typeAhead: true
		},
		{
                                xtype: 'textareafield',
                                height: 100,
                                width: 426,
                                itemId: 'Cartable-Documents-DocumentAdd-changeCause-txtarea',
                                id: 'Cartable-Documents-DocumentAdd-changeCause-txtarea',
                                fieldLabel: 'علت ایجاد',
                                name: 'changeCause'
		},{
										xtype: 'htmleditor',
                                height: 300,
                                width: 826,
                                fontFamilies:['Arial', 'Courier New', 'Tahoma', 'Times New Roman', 'Verdana','Lotus','B Nazanin'],
                                itemId: 'Cartable-Documents-DocumentAdd-documentBody-htmleditor',
                                id: 'Cartable-Documents-DocumentAdd-documentBody-htmleditor',
                                fieldLabel: 'متن',
                                name: 'documentBody'

		}
		]
            }]
        });

        me.callParent(arguments);
    }

});
/*Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Documents.view.DocumentAddForm', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox'
    ],
    width: 500,
    title: 'Add Documents',



    initComponent: function(arguments) {
        var me = this;
        DocumentAddForm = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'customform',
                layout: 'auto',
                autoScroll: true,
                itemId: 'cartable-Documents-DocumentsAdd-form',
                layout: 'auto',
                bodyPadding: 10,
                title: '',
                url: '/cgi-bin/cartablescgi.cgi',
                saveId: 'setZone',
                saveType: 'add',
		//saveCallback: DocumentAddForm.callback,
               // extraParams: {
               //     flag: DocumentAddForm.saveFlag
               // },
                saveItemId: 'cartable-Documents-DocumentsAdd-save-btn',
                //loadId: 'viewZone',
                hideResetBtn: true,
                //loadType: 'form',
                defaultSave: true,
                defaultReset: true,
                defaultafterrender: true,
                items: [
                {
                    xtype: 'fieldset',
                    height: 71,
                    padding: '5 5 5 5',
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'cartable-Documents-DocumentsAdd-ZoneName-txt',
                            fieldLabel: 'Name',
                            name: 'ZoneName',
			    vtype:'name'
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'cartable-Documents-DocumentsAdd-ZoneComment-txt',
                            fieldLabel: 'Comment',
                            name: 'ZoneComment',
			    vtype:'comment'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    height: 100,
                    layout: 'column',
                    collapsible: true,
                    title: 'Zone',
                    items: [
                        {
                            xtype: 'fieldset',
                            border: 0,
                            height: 25,
                            width: 400,
                            layout: 'column',
                            title: '',
                            items: [
                                {
                                    xtype: 'radiofield',
                                    itemId: 'cartable-Documents-DocumentsAdd-IntraRB-radio',
                                    padding: '0 50 0 0',
                                    fieldLabel: '',
                                    name: 'ZoneRadio',
                                    boxLabel: 'Route: ',
                                    boxLabelAlign: 'before',
                                    inputValue: 'Route'
                                },
                                {
                                    xtype: 'checkboxfield',
                                    anchor: '100%',
                                    itemId: 'cartable-Documents-DocumentsAdd-IntraZoneBlocking-chkbox',
                                    fieldLabel: '',
                                    labelWidth: 130,
                                    name: 'IntraZoneBlocking',
                                    boxLabel: 'Intra Zone Blocking',
                                    boxLabelAlign: 'before'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            border: 0,
                            height: 25,
                            width: 400,
                            layout: 'column',
                            title: '',
                            items: [
                                {
                                    xtype: 'radiofield',
                                    itemId: 'cartable-Documents-DocumentsAdd-InterfaceRB-radio',
                                    padding: '0 50 0 0',
                                    fieldLabel: '',
                                    name: 'ZoneRadio',
                                    boxLabel: 'Bridge: ',
                                    boxLabelAlign: 'before',
                                    inputValue: 'Bridge'
                                }
                            ]
                        }
                    ]
                }
            ]
            }]
        });

        me.callParent(arguments);
    }

});*/
