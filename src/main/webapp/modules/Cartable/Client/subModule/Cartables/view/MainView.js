Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.view.MainView', {
	extend: 'Ext.container.Container',
	requires: [
		'Ext.ux.customActionCol'
	],
	title: 'کارتابل شخصی',
	itemId: 'cartable-cartables-panel',
	layout: 'fit',
	initComponent: function() {
		var me = this;
		zonesForm = this;
		zonesForm.connection = this.connection;
		var CartablesStore = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Cartables.store.CartablesStore');
		Ext.applyIf(me, {
			items: [{
				xtype: 'customgrid',
				dock: 'top',
				title: '',
				forcefit: true,
				itemId: 'cartable-cartables-grid',
				hideNewBtn: false,
				hidesearchbar: true,
				hidePaging: true,
				hideRefreshBtn: false,
				newBtnItemId: 'cartable-cartables-grid-addBtn',
				newBtnText: 'درخواست مرخصی',
				newBtnTip: 'درخواست مرخصی',
				editable: false,
				showRowonNew: false,
				store: CartablesStore,
				model: 'MyDesktop.Modules.Cartable.Client.subModule.Cartables.model.CartablesModel',
				addURL: '',
				editURL: '',
				columns: [{
					xtype: 'gridcolumn',
					dataIndex: 'id',
					text: 'کد',
					flex: 1,
				}, {
					xtype: 'gridcolumn',
					dataIndex: 'name',
					text: 'نام',
					flex: 1,
				}, {
					xtype: 'actioncolumn',
					width: 50,
					text: 'عملیات',
					tooltip: '',
					iconCls: 'active-check',
					handler: function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						/*ticketmanagement.createTicketCommentWindow(rec.get('tbl_person_branch_id'), rec.get('tbl_product_branch_id'));
						 Ext.Msg.alert('متن توضیح', rec.get('id'));
						 Ext.create('widget.uxNotification', {
						 title: 'وضعیت',
						 corner: 'tr',
						 stickOnClick: false,
						 manager: 'demo1',
						 width: 250,
						 height: 100,
						 iconCls: 'ux-notification-icon-information',
						 html: "data.message"
						 }).show();*/
						var taskId = [];
						var taskId = {
							taskId: rec.get('id')
						};
						//taskId['taskId'] = rec.get('id');
						var ajax = Ext.Ajax.request({
							headers: {
								'Content-Type': 'application/json'
							},
							url: 'cxf/rest/cartable/goToTaskForm',
							method: 'POST',
							async: false,
							params: Ext.encode(taskId),
							success: function(response) {
								var decoded = Ext.decode(response.responseText);
								var OfTimeRequestForm = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Cartables.view.OfTimeRequest', {
									configType: '1',
									selectedRecord: '',
									//saveFlag: '0',
									taskId: decoded.data.taskId,
									//eventName: decoded.data.eventName,
									memberId: decoded.data.variables.OffTimeId,
									//Class: 'cartable'
								});
								OfTimeRequestForm.show();
							}
						}, this);
					}
				}]
			}]
		});
		me.callParent(arguments);
	}

});