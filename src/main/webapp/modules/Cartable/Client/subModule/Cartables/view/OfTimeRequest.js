Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.view.OfTimeRequest', {
	extend: 'Ext.window.Window',
	width: 550,
	height: 400,
	title: 'درخواست مرخصی',
	itemId:'cartable-oftimerequest-win',
	modal: true,
	layout: 'fit',

	initComponent: function(arguments) {
		var me = this;
		ofTimeRequest = this;
		ofTimeRequest.configType = this.configType;
		ofTimeRequest.selectedRecord = this.selectedRecord;
		ofTimeRequest.taskId = this.taskId;
		ofTimeRequest.memberId = this.memberId;
		var Kind = Ext.create('Ext.data.Store', {
			fields: ['id', 'name'],
			data : [
				{"id":"Daily", "name":"مرخصی روزانه"},
				{"id":"Hourly", "name":"مرخصی ساعتی"}
			]
		});
		//var ProvinceStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.City.store.ProvinceStore');
		//var ProvinceStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Province.store.ProvinceStore');

		Ext.applyIf(me, {
			items: [{
				xtype: 'customform',
				layout: 'auto',
				autoLoad: false,
				autoScroll: true,
				bodyPadding: 10,
				itemId: 'cartable-oftimerequest-form',
				saveItemId: 'cartable-oftimerequest-save-btn',
				//submitUrl: 'cxf/rest/offTime/saveBpm',
				//saveCallback: this.callback,
				//saveItemId: 'base-city-CityAdd-save-btn',
				hideResetBtn: true,
				defaultSave: false,
				defaultReset: true,
				items: [{
					xtype: 'combobox',
					itemId: 'cartable-oftimerequest-ofTimeKind-cmb',
					fieldLabel: 'نوع مرخصی',
					name: 'offTimeType',
					displayField: 'name',
					valueField: 'id',
					allowBlank: false,
					emptyText: 'انتخاب کنید',
					queryMode: 'local',
					store: Kind,
					typeAhead: true
				},{
					xtype: 'timefield',
					itemId: 'cartable-oftimerequest-inTime-cmb',
					name: 'inTime',
					fieldLabel: 'از ساعت',
					minValue: '8:00 AM',
					maxValue: '8:00 PM',
					increment: 30,
					anchor: '100%',
					allowBlank: true,
					hidden: true
				},{
					xtype: 'datefield',
					anchor: '100%',
					fieldLabel: 'از تاریخ',
					name: 'dateFrom',
					allowBlank: false,
					itemId: 'cartable-oftimerequest-fromdate-cmb',
					//maxValue: new Date()  // limited to the current date or prior
				},{
					xtype: 'timefield',
					itemId: 'cartable-oftimerequest-outTime-cmb',
					name: 'outTime',
					fieldLabel: 'تا ساعت',
					minValue: '8:00 AM',
					maxValue: '8:00 PM',
					increment: 30,
					anchor: '100%',
					allowBlank: true,
					hidden: true
				}, {
					xtype: 'datefield',
					anchor: '100%',
					fieldLabel: 'تا تاریخ',
					name: 'dateTo',
					allowBlank: false,
					itemId: 'cartable-oftimerequest-todate-cmb',
					//value: new Date()  // defaults to today
				},{
					xtype: 'textareafield',
					grow: true,
					name: 'comments',
					fieldLabel: 'توضیحات',
					height: 100,
					width: 450,
					itemId: 'cartable-oftimerequest-message-cmb',
					//anchor    : '100%'
				}]
			}]
		});

		me.callParent(arguments);
	}

});
