Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.model.CartablesModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'name'
        }
    ]
});
