Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.MainController');
        controller.init();
        var OfTimeRequestController = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.OfTimeRequestController');
        OfTimeRequestController.init();
    }
});