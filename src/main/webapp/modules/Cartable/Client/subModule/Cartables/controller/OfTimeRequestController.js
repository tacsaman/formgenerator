Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.OfTimeRequestController', {
    extend: 'Ext.app.Controller',

    onChangeofTimeKind: function(newValue, oldValue, eOpts) {
        if (newValue.value == "Hourly") {
            Ext.ComponentQuery.query('#cartable-oftimerequest-inTime-cmb')[0].setVisible(true);
            Ext.ComponentQuery.query('#cartable-oftimerequest-inTime-cmb')[0].allowBlank = false;
            Ext.ComponentQuery.query('#cartable-oftimerequest-outTime-cmb')[0].setVisible(true);
            Ext.ComponentQuery.query('#cartable-oftimerequest-outTime-cmb')[0].allowBlank = false;
        } else {
            Ext.ComponentQuery.query('#cartable-oftimerequest-inTime-cmb')[0].setVisible(false);
            Ext.ComponentQuery.query('#cartable-oftimerequest-inTime-cmb')[0].allowBlank = true;
            Ext.ComponentQuery.query('#cartable-oftimerequest-outTime-cmb')[0].setVisible(false);
            Ext.ComponentQuery.query('#cartable-oftimerequest-outTime-cmb')[0].allowBlank = true;
        }
    },
    callback: function() {
        //Ext.ComponentQuery.query('#base-city-grid')[0].getStore().reload();
    },
    onSaveOffTiem: function() {
        var util = Ext.create('MyDesktop.Util.util');
        var Kind = Ext.ComponentQuery.query('#cartable-oftimerequest-ofTimeKind-cmb')[0].getValue();
        var inTime = "";
        var outTime = "";
        if (Kind == 'Hourly') {
            inTime = Ext.Date.format(Ext.ComponentQuery.query('#cartable-oftimerequest-inTime-cmb')[0].getValue(), 'H:i:s A');
            outTime = Ext.Date.format(Ext.ComponentQuery.query('#cartable-oftimerequest-outTime-cmb')[0].getValue(), 'H:i:s A');
        }
        else{
            inTime = "00:00:00 AM";
            outTime = "00:00:00 AM";
        }

        var Mydate = Ext.Date.format(Ext.ComponentQuery.query('#cartable-oftimerequest-fromdate-cmb')[0].getValue() , 'M d Y');
        var fromdate = [Mydate.slice(0, 6), "," , Mydate.slice(6)].join('');
        fromdate += " " + inTime;

        var MyDate = Ext.util.Format.date(Ext.ComponentQuery.query('#cartable-oftimerequest-todate-cmb')[0].getValue() , 'M d Y');
        var todate = [MyDate.slice(0, 6), "," , MyDate.slice(6)].join('');
        todate += " " + outTime;

        var message = Ext.ComponentQuery.query('#cartable-oftimerequest-message-cmb')[0].getValue();
        var username = Ext.getCmp('start-menu-id').title;
        var offTimeRequestForm = Ext.ComponentQuery.query('#cartable-oftimerequest-form')[0];
        console.log(offTimeRequestForm.isValid())
        if (offTimeRequestForm.isValid()) {
            var merge = {};
            var merge = Ext.apply({
                dateFrom: fromdate,
                dateTo: todate,
                comments: message,
                offTimeType: Kind,
                taskId: ofTimeRequest.taskId
            });
            var url = 'cxf/rest/offTime/saveBpm';
            var util = Ext.create('MyDesktop.Util.util', {
                callBack: function() {},
                url: url,
                data: merge
            });
            util.sendRequest();
        }
    },

    loadFormOffTime: function(){
        var offTimeForm = Ext.ComponentQuery.query('#cartable-oftimerequest-form')[0];
        if(ofTimeRequest.memberId){
            offTimeForm.load({
                params: {
                    id: ofTimeRequest.memberId
                },
                url: 'cxf/rest/offTime/getById',
                method: "GET"
            })
            offTimeForm.on({
                actioncomplete: function(form, action) {
                    if (action.type == 'load') {
                        var contact = action.result.data;
                        var dateFrom = Ext.Date.parse((contact.dateFrom.substring(0,12).replace("," , "")).replace(/ /g, "-"), 'M-d-Y');
                        var inTime = (contact.dateFrom.substring(13));
                        var T1 = inTime.substring(0,(inTime.search('M'))-5);
                        var P1 = inTime.substring((inTime.search('M'))-1 , inTime.length)
                        var LastInTime =T1 + " " +P1;
                        var dateTo = Ext.Date.parse((contact.dateTo.substring(0,12).replace("," , "")).replace(/ /g, "-"), 'M-d-Y');
                        var outTime = (contact.dateTo.substring(13));
                        var T2 = outTime.substring(0,(outTime.search('M'))-5);
                        var P2 = outTime.substring((outTime.search('M'))-1 , outTime.length)
                        var LastOutTime = T2 + " " + P2;
                        if(contact.offTimeType == "Hourly"){
                            Ext.ComponentQuery.query('#cartable-oftimerequest-inTime-cmb')[0].setValue(LastInTime);
                            Ext.ComponentQuery.query('#cartable-oftimerequest-outTime-cmb')[0].setValue(LastOutTime);
                        }
                        Ext.ComponentQuery.query('#cartable-oftimerequest-fromdate-cmb')[0].setValue(dateFrom);
                        Ext.ComponentQuery.query('#cartable-oftimerequest-todate-cmb')[0].setValue(dateTo)
                    }
                }
            })

        }
    },
    init: function(application) {
        this.control({
            "#cartable-oftimerequest-ofTimeKind-cmb": {
                change: this.onChangeofTimeKind
            },
            "#cartable-oftimerequest-save-btn-submit": {
                click: this.onSaveOffTiem
            },
            "#cartable-oftimerequest-form": {
                afterrender: this.loadFormOffTime
            }
        });
    }
});