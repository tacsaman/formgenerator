Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.MainController', {
    extend: 'Ext.app.Controller',
    onClickOfTimeRequest: function() {
        var ajax = Ext.Ajax.request({
            url: 'cxf/rest/bpm/startOffTimeProcess',
            method: 'GET',
            success: function(response) {
                Ext.ComponentQuery.query('#cartable-cartables-grid')[0].getStore().reload();
            }
        }, this);
    },
    init: function(application) {
        this.control({
            "#cartable-cartables-grid-addBtn": {
                click: this.onClickOfTimeRequest
            }
        });
    }
});
