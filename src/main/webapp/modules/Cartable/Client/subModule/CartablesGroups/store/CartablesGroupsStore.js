Ext.define('MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.store.CartablesGroupsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.model.CartablesGroupsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				model: 'MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.model.CartablesGroupsModel', 
            storeId: 'RegionStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/cartable/getCandidateUserTaskList',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
