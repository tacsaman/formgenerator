Ext.define('MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'کارتابل گروهی',
	 itemId: 'cartable-cartablesGroups-panel',
    layout: 'fit',
	initComponent: function() {
 	var me = this;
	zonesForm = this;
	zonesForm.connection = this.connection;
	var CartablesStore = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.store.CartablesGroupsStore');
	Ext.applyIf(me, {
            items: [{
                xtype: 'customgrid',
                dock: 'top',
                title: '',
                forcefit: true,
		cancelObject:'name',
                itemId: 'cartable-cartablesGroups-grid',
		hideNewBtn: true,
		hidesearchbar: true,
		hidePaging: true,
                hideRefreshBtn: false,
                editable: false,
                showRowonNew: true,
                store: CartablesStore,
                model: 'MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.model.CartablesGroupsModel',
		addURL: '',
		editURL: '',	
                columns: [{
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'کد',
                    flex: 1,
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'name',
                    text: 'نام',
                    flex: 1,
		},{
		    xtype: 'actioncolumn',
		    width:50,
			text:'عملیات',
		    tooltip: '',
		    iconCls: 'active-check',
		    handler: function(grid, rowIndex, colIndex) {
		        var rec = grid.getStore().getAt(rowIndex);
		        /*ticketmanagement.createTicketCommentWindow(rec.get('tbl_person_branch_id'), rec.get('tbl_product_branch_id'));
		        Ext.Msg.alert('متن توضیح', rec.get('id'));
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: "data.message"
                        }).show();*/
			var taskId= [];
			var taskId = {taskId:rec.get('id')}; 
			//taskId['taskId'] = rec.get('id');
			var ajax = Ext.Ajax.request({
				headers: { 'Content-Type': 'application/json' },
				url: 'cxf/rest/cartable/goToTaskForm',
				method: 'POST',
				async: false,
				params:  Ext.encode(taskId),
				success: function (response) {
					var decoded = Ext.decode(response.responseText);
					var CentersAddForm = Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.view.DocumentAddForm', {
						configType: '1',
						selectedRecord: '',
						saveFlag: '0',
						taskId:decoded.data.taskId,
						eventName:decoded.data.eventName,
						memberId:decoded.data.variables.documentId,
						Class:'cartableGroups'

					});
					CentersAddForm.show();
				}
            }, this);
		    }
		}]
            }]
        });
        me.callParent(arguments);
    }

});
