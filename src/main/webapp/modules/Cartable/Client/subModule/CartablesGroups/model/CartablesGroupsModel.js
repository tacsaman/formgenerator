Ext.define('MyDesktop.Modules.Cartable.Client.subModule.CartablesGroups.model.CartablesGroupsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'name'
        }
    ]
});
