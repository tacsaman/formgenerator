Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.controller.MainController', {
    extend: 'Ext.app.Controller',
    onNotificationsNewBtnClick: function() {
        var NotificationAddForm = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Notifications.view.NotificationAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        NotificationAddForm.show();
    },


    init: function(application) {
        this.control({
            "#cartable-notifications-grid-addBtn": {
                click: this.onNotificationsNewBtnClick
            }


        });
    }
});
