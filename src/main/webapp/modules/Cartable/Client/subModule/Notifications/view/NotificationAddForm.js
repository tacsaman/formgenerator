Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.view.NotificationAddForm', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.ux.twinCombo'
    ],
    width: 930,
    title: 'تایید مستند',
	 modal: true,
    initComponent: function(arguments) {
        var me = this;
        NotificationAddForm = this;
        NotificationAddForm.formData = this.selectedRecord;
        NotificationAddForm.configType = this.configType;
        NotificationAddForm.saveFlag = this.saveFlag;
        NotificationAddForm.memberId = this.memberId;
        NotificationAddForm.taskId = this.taskId;
        NotificationAddForm.eventName = this.eventName;
        Ext.applyIf(me, {
            items: [{
                xtype: 'customform',
                layout: 'auto',
		autoLoad:false,
                autoScroll: true,
                layout: 'auto',
                bodyPadding: 10,
                title: '',
		itemId:'cartable-Notifications-NotificationsAdd-form',
                submitUrl: 'cxf/rest/notification/save',
		//saveCallback: NotificationAddForm.callback,
                saveItemId: 'cartable-Notifications-NotificationAdd-save-btn',
                hideResetBtn: true,
               // defaultSave: false,
                defaultReset: true,
		toolbarArray: [{
		    text: 'تایید',
		    itemId: 'cartable-Notifications-NotificationAdd-tayid-btn',
//		    iconCls: 'icon-save',
		    hidden: true
		}, {
		    text: 'رد',
		    itemId: 'cartable-Notifications-NotificationAdd-rad-btn',
//		    iconCls: 'icon-cancel',
		    hidden: true
		}],

                items: [
		{
                        xtype: 'textfield',
                        itemId: 'Cartable-Notifications-NotificationAdd-notificationName-txt',
                        width: 280,
                        fieldLabel: 'نام',
                        name: 'notificationName'
		},
		{
                            xtype: 'combobox',
                            itemId: 'Cartable-Notifications-NotificationAdd-notificationType-cmb',
                            fieldLabel: 'نوع مستند',
                            name: 'notificationType',
                            displayField: 'name',
                            allowBlank: false,
                            emptyText: 'select ...',
                            queryMode: 'local',
                            store: [
                                ['word', 'word'],
                                ['excel', 'excel']
                            ],
                            typeAhead: true
		},
		{
                                xtype: 'textareafield',
                                height: 100,
                                width: 426,
                                itemId: 'Cartable-Notifications-NotificationAdd-changeCause-txtarea',
                                id: 'Cartable-Notifications-NotificationAdd-changeCause-txtarea',
                                fieldLabel: 'علت ایجاد',
                                name: 'changeCause'
		},{
										xtype: 'htmleditor',
                                height: 300,
                                width: 826,
                                fontFamilies:['Arial', 'Courier New', 'Tahoma', 'Times New Roman', 'Verdana','Lotus','B Nazanin'],
                                itemId: 'Cartable-Notifications-NotificationAdd-notificationBody-htmleditor',
                                id: 'Cartable-Notifications-NotificationAdd-notificationBody-htmleditor',
                                fieldLabel: 'متن',
                                name: 'notificationBody'

		}
		]
            }]
        });

        me.callParent(arguments);
    }

});
/*Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.view.NotificationAddForm', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox'
    ],
    width: 500,
    title: 'Add Notifications',



    initComponent: function(arguments) {
        var me = this;
        NotificationAddForm = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'customform',
                layout: 'auto',
                autoScroll: true,
                itemId: 'cartable-Notifications-NotificationsAdd-form',
                layout: 'auto',
                bodyPadding: 10,
                title: '',
                url: '/cgi-bin/cartablescgi.cgi',
                saveId: 'setZone',
                saveType: 'add',
		//saveCallback: NotificationAddForm.callback,
               // extraParams: {
               //     flag: NotificationAddForm.saveFlag
               // },
                saveItemId: 'cartable-Notifications-NotificationsAdd-save-btn',
                //loadId: 'viewZone',
                hideResetBtn: true,
                //loadType: 'form',
                defaultSave: true,
                defaultReset: true,
                defaultafterrender: true,
                items: [
                {
                    xtype: 'fieldset',
                    height: 71,
                    padding: '5 5 5 5',
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'cartable-Notifications-NotificationsAdd-ZoneName-txt',
                            fieldLabel: 'Name',
                            name: 'ZoneName',
			    vtype:'name'
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'cartable-Notifications-NotificationsAdd-ZoneComment-txt',
                            fieldLabel: 'Comment',
                            name: 'ZoneComment',
			    vtype:'comment'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    height: 100,
                    layout: 'column',
                    collapsible: true,
                    title: 'Zone',
                    items: [
                        {
                            xtype: 'fieldset',
                            border: 0,
                            height: 25,
                            width: 400,
                            layout: 'column',
                            title: '',
                            items: [
                                {
                                    xtype: 'radiofield',
                                    itemId: 'cartable-Notifications-NotificationsAdd-IntraRB-radio',
                                    padding: '0 50 0 0',
                                    fieldLabel: '',
                                    name: 'ZoneRadio',
                                    boxLabel: 'Route: ',
                                    boxLabelAlign: 'before',
                                    inputValue: 'Route'
                                },
                                {
                                    xtype: 'checkboxfield',
                                    anchor: '100%',
                                    itemId: 'cartable-Notifications-NotificationsAdd-IntraZoneBlocking-chkbox',
                                    fieldLabel: '',
                                    labelWidth: 130,
                                    name: 'IntraZoneBlocking',
                                    boxLabel: 'Intra Zone Blocking',
                                    boxLabelAlign: 'before'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            border: 0,
                            height: 25,
                            width: 400,
                            layout: 'column',
                            title: '',
                            items: [
                                {
                                    xtype: 'radiofield',
                                    itemId: 'cartable-Notifications-NotificationsAdd-InterfaceRB-radio',
                                    padding: '0 50 0 0',
                                    fieldLabel: '',
                                    name: 'ZoneRadio',
                                    boxLabel: 'Bridge: ',
                                    boxLabelAlign: 'before',
                                    inputValue: 'Bridge'
                                }
                            ]
                        }
                    ]
                }
            ]
            }]
        });

        me.callParent(arguments);
    }

});*/
