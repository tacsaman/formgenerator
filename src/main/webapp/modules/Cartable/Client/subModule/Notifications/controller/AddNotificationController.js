Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.controller.AddNotificationController', {
    extend: 'Ext.app.Controller',
    onNotificationsSaveBtnClick: function() {
	},
    onNotificationsNewBtnClick: function() {
        var NotificationAddForm = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Notifications.view.NotificationAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        NotificationAddForm.show();
    },
    onNotificationsTayidBtnClick: function() {
    	if (NotificationAddForm.eventName == "formTasvib")
 			var variables = {tasvib:"true"}; 
 		else
 			var variables = {tayid:"true"}; 
		variables = {taskId:NotificationAddForm.taskId,variables:variables}
                var ajax = Ext.Ajax.request({
	            headers: { 'Content-Type': 'application/json' },
                    url: 'cxf/rest/cartable/completeTask',
                    method: 'POST',
                    async: false,
                    params:  Ext.encode(variables),
                    success: function (response) {
                	var decoded = Ext.decode(response.responseText);
				}
			});
	},
    onNotificationsRadBtnClick: function() {
    	if (NotificationAddForm.eventName == "formTasvib")
 			var variables = {tasvib:"false"}; 
 		else
 			var variables = {tayid:"false"}; 
		variables = {taskId:NotificationAddForm.taskId,variables:variables}
                var ajax = Ext.Ajax.request({
	            headers: { 'Content-Type': 'application/json' },
                    url: 'cxf/rest/cartable/completeTask',
                    method: 'POST',
                    async: false,
                    params:  Ext.encode(variables),
                    success: function (response) {
                	var decoded = Ext.decode(response.responseText);
			}
		});
	},
    loadForm: function() {
        var form = Ext.ComponentQuery.query('#cartable-Notifications-NotificationsAdd-form')[0];
        thisthis = this;
        if (NotificationAddForm.memberId) {
		var docId= [];
		var docId = {id:NotificationAddForm.memberId}; 
                var ajax = Ext.Ajax.request({
	            headers: { 'Content-Type': 'application/json' },
                    url: 'cxf/rest/notification/getById',
                    method: 'GET',
                    async: false,
                    params:  docId,
                    success: function (response) {
                	var decoded = Ext.decode(response.responseText);
				Ext.ComponentQuery.query('#Cartable-Notifications-NotificationAdd-notificationName-txt')[0].setValue(decoded.data.notificationName)
			Ext.ComponentQuery.query('#Cartable-Notifications-NotificationAdd-changeCause-txtarea')[0].setValue(decoded.data.changeCause)
			Ext.ComponentQuery.query('#Cartable-Notifications-NotificationAdd-notificationType-cmb')[0].setValue(decoded.data.notificationType)
			Ext.ComponentQuery.query('#Cartable-Notifications-NotificationAdd-notificationBody-htmleditor')[0].setValue(decoded.data.notificationBody)
			Ext.ComponentQuery.query('#cartable-Notifications-NotificationAdd-tayid-btn')[0].show()
			Ext.ComponentQuery.query('#cartable-Notifications-NotificationAdd-rad-btn')[0].show()
        var form = Ext.ComponentQuery.query('#cartable-Notifications-NotificationsAdd-form')[0].submitUrl= 'cxf/rest/notification/update';
			}
		});
	} else {
		Ext.ComponentQuery.query('#cartable-Notifications-NotificationAdd-tayid-btn')[0].hide()
		Ext.ComponentQuery.query('#cartable-Notifications-NotificationAdd-rad-btn')[0].hide()
        }

        form.on({
            actioncomplete: function(form, action) {
                if (action.type == 'load') {
                        var contact = action.result.data;
                        Ext.ComponentQuery.query('#object-Addresses-AddressesAdd-AddressName-txt')[0].setReadOnly(true);
                        Ext.ComponentQuery.query('#object-Addresses-AddressesAdd-addressType-cmb')[0].setReadOnly(true);
                }
            }
        })
    },
    init: function(application) {

        this.control({
            "#cartable-Notifications-NotificationAdd-tayid-btn": {
                click: this.onNotificationsTayidBtnClick
            },
            "#cartable-Notifications-NotificationAdd-rad-btn": {
                click: this.onNotificationsRadBtnClick
            },
            "#cartable-Notifications-NotificationAdd-save-btn": {
                click: this.onNotificationsNewBtnClick
            },
            "#cartable-Notifications-NotificationsAdd-form": {
                afterrender: this.loadForm
            }
        });
    }


});
