Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.model.NotificationsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
	{
            name: 'documentType'
        }
	,{
            name: 'documentName'
        }
	,{
            name: 'changeCause'
        }
	,{
            name: 'documentBody'
        }

    ]
});
