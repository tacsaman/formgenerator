Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'اعلان ها',
	 itemId: 'cartable-notifications-panel',
    layout: 'fit',
	initComponent: function() {
 	var me = this;
	zonesForm = this;
	zonesForm.connection = this.connection;
	var NotificationsStore = Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Notifications.store.NotificationsStore');
	Ext.applyIf(me, {
            items: [{
                xtype: 'customgrid',
                dock: 'top',
                title: '',
                forcefit: true,
                itemId: 'cartable-notifications-grid',
				hidesearchbar: true,
				hidePaging: true,
                hideRefreshBtn: false,
                editable: false,
				hideNewBtn: true,
                showRowonNew: false,
				disableEditRow: true,
                store: NotificationsStore,
                model: 'MyDesktop.Modules.Cartable.Client.subModule.Notifications.model.NotificationsModel',
                columns: [{
                    xtype: 'gridcolumn',
//                    dataIndex: 'id',
                    text: 'کد',
                    flex: 1,
					renderer: function(value, metaData, record, row, col, store, gridView) {
						if (record.raw.finalDocument.code)
							return record.raw.finalDocument.code;
					}

                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'documentName',
                    text: 'نام',
                    flex: 1,
					renderer: function(value, metaData, record, row, col, store, gridView) {
						if (record.raw.finalDocument.documentName)
							return record.raw.finalDocument.documentName;
					}

                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'documentType',
                    text: 'نوع مستند',
                    flex: 1,
					renderer: function(value, metaData, record, row, col, store, gridView) {
						if (record.raw.finalDocument.documentType.name)
							return record.raw.finalDocument.documentType.name;
					}
				}, {
					xtype: 'customactioncol',
					flex: 1,
					itemId: 'cartable-notifications-grid-customactioncolumn',
					text: 'عملیات',
					deletable: false,
					editable: true,
					defaultCustomEdit: false,
					items: [{
						handler: function(view, rowIndex, colIndex, item, e, record, row) {
							document.location.replace("downloadReport?output=pdf&finalDocumentId="+record.raw.finalDocument.id);
						},
						icon: 'resources/images/default/component/user_download.png',
						tooltip: 'دانلود'
					}]

                }]
            }]
        });
        me.callParent(arguments);
    }

});
