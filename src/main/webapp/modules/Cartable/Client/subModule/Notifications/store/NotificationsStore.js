Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Notifications.store.NotificationsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Cartable.Client.subModule.Notifications.model.NotificationsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				model: 'MyDesktop.Modules.Cartable.Client.subModule.Notifications.model.NotificationsModel', 
            storeId: 'RegionStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/docNotif/getAll',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
