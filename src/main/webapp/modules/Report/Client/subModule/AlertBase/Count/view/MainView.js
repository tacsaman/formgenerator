Ext.define('MyDesktop.Modules.Report.Client.subModule.AlertBase.Count.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Frequent Alerts',
    itemId: 'alert-count-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
    	var me = this;
        AlertBaseForm = this;
        AlertBaseForm.BarChart = '';
        AlertBaseForm.data = '';
        AlertBaseForm.popup = '';
        AlertBaseForm.BasePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            id:'alertbasepanel',
            style: {
                direction:'ltr !important'
            },
            items: [/*pieExample*/]
        });
        var GridExamPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            title: 'Required values',
            width: 200,
            layout: 'auto',
            region: 'east',
            collapsible: true,
            //collapsed: true,
            items: [{
                xtype: 'textfield',
                name: 'name',
                fieldLabel: 'Signature ID',
                allowBlank: false,
                itemId: 'alert-sid-txt'
            },{
                xtype: 'button',
                text : 'Apply',
                itemId: 'alert-report-btn'
            }, {
                xtype: 'button',
                text : 'Return',
                itemId: 'alert-back-btn'
            }],
        });


        Ext.applyIf(me, {
            items: [AlertBaseForm.BasePanel,GridExamPanel]
        });
        me.callParent(arguments);
    }

});