Ext.define('MyDesktop.Modules.Report.Client.subModule.AlertBase.Count.store.ChartsStoreDetails', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
 		var me = this;
		cfg = cfg || {};
		var jsondata = Ext.encode(["a","b"]) ;
		me.callParent([Ext.apply({
		    autoLoad: true,
		    fields: [
				'timestamp'
			, 'srcip'
			,'srccountry'
			, 'dstip'
			,'dstcountry'
			, 'protocol'
			,'priority'
			,'message'
			,'sid'
			,'defaultMessage'
			,'defaultAction'
			],
		    proxy: {
		    	type:'ajax',
				headers: { 'Content-Type': 'application/json' },
		         paramsAsJson:true,
		        actionMethods: {create: 'POST', read: 'POST', update:'POST', destroy: 'POST'},
		        method:'POST',
		        url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
		extraParams:{ "reportName": "AlertMashkook","reportParams":[cfg.sid],"ipss":true},
		        reader: {
		            type: 'json',root: 'data',totalProperty: 'totalCount'
		        }
		    },
		listeners: {
		'beforeload' :  function(store,records,options) {
		
		}
		}
		}, cfg)]);
}
});
