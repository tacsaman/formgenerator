Ext.define('MyDesktop.Modules.Report.Client.subModule.AlertBase.Count.controller.MainController', {
	extend: 'Ext.app.Controller',

	
	showBarChart: function(){
		var sid = Ext.ComponentQuery.query('#alert-sid-txt')[0].getValue();
		merge = {
				"reportName": "AlertMashkook",
				"reportParams": []
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
	            'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			//async: false,
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				 decoded = Ext.decode(response.responseText);
				 AlertBaseForm.data = decoded.data;
				 AlertBaseForm.BarChart = Ext.create('Chart.ux.Highcharts', {
				    	itemId: "baseDestPort",
				        xAxis: {
				            type: 'category',
				            labels: {
				                rotation: -45,
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            }
				        },

				        store: AlertBaseForm.data,
				        series: [{
				            name: 'Alert number',
				            dataLabels: {
				                enabled: true,
				                color: '#FFFFFF',
				                align: 'center',
				                format: '{point.y}', // one decimal
				                y: 10, // 10 pixels down from the top
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            },
				            
					        point: {
				                    events: {
				                        click: function () {
				                        	Ext.ComponentQuery.query('#alert-sid-txt')[0].setValue(this.options.name)
				                    		me.showBarChartSpec()
//				            				  	Ext.ComponentQuery.query('#port-dest-ip-txt')[0].setValue(this.options.name)
//				            				  	if (state == "tcp")
//						                        	Ext.ComponentQuery.query('#tcp-port-dest-radio')[0].setValue(true);
//						                    	else
//						                        	Ext.ComponentQuery.query('#udp-port-dest-radio')[0].setValue(true);
//				            				  	
//				            				  	me.showSpecificPortChart(state, this.options.name)	
				                        }
				                    }
				                }
				        }],
				        chartConfig: {
				            chart: {
				                type: 'column'
				            },
				            
				            xAxis: {
								 type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							    },

				            title: {
				                text:  'Top 20 Frequent Alerts'
				            },
				            tooltip: {
				                pointFormat: 'Number of alerts: <b>{point.y}</b>'
				            },
				            dataLabels: {
				                enabled: true,
				                color: '#FFFFFF',
				                align: 'center',
				                format: '{point.y}', // one decimal
				                y: 10, // 10 pixels down from the top
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            }
				        }
				 });
				 AlertBaseForm.BarChart.store.reload();
				 AlertBaseForm.BarChart.series[0].data = AlertBaseForm.data;
				 AlertBaseForm.BarChart.refresh();
				 if(AlertBaseForm.BasePanel.items.length == 1){
					 AlertBaseForm.BasePanel.items.items[0].hide();
					 AlertBaseForm.BasePanel.items.removeAt(0);
				 }
				 
				 AlertBaseForm.BasePanel.add(AlertBaseForm.BarChart);
				 myMask.hide();
		}
		});
	},
	showBarChartSpec: function() {
		var sid = Ext.ComponentQuery.query('#alert-sid-txt')[0].getValue();
		
		var MainViewDetails = Ext.create('MyDesktop.Modules.Report.Client.subModule.AlertBase.Count.view.MainViewDetails',{sid:sid});
		MainViewDetails.show();
	},
    
	onafterrenderForm: function(){
		this.showBarChart()
	},
	
	onShowBarchartForSpecificSID:function(){
		this.showBarChartSpec()
	},
	
	
	init: function(application) {
		this.control({
			"#alert-count-report-charts-panel": {
				afterrender: this.onafterrenderForm
			},"#alert-report-btn": {
				click: this.onShowBarchartForSpecificSID
			},"#alert-back-btn": {
				click: this.onafterrenderForm
			}
		});
	}
});
