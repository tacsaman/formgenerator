Ext.define('MyDesktop.Modules.Report.Client.subModule.PriorityBase.TimeSeries.controller.MainController', {
	extend: 'Ext.app.Controller',
	
	showTimeSeries: function(fromDate,toDate,title){
		merge = {
				"reportName": "TimeSeriesPriority",
				"reportParams": [fromDate,toDate]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				decoded = Ext.decode(response.responseText);

				 
				PriorityTimeSeries.data =decoded.data;
				PriorityTimeSeries.pieExample = Ext.create('Chart.ux.Highcharts', {
						itemId: 'prioritytimeseries',
						xField: 'name',
						series:  PriorityTimeSeries.data,
						chartConfig: {
							chart: {     
								type: 'area'
								
							}
							,xAxis: {
								   type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							},
							yAxis: {
								 min: 0,
								title: {
									text: 'Number of alert'
								}
							},
							//store: zonesForm.data,
							title: {
								text: title
							},
							tooltip: {
								pointFormat: 'Number of alerts: <b>{point.y} </b>'
							},
							plotOptions: {
								area: {
					                stacking: 'normal',
					                lineColor: '#666666',
					                lineWidth: 1,
					                marker: {
					                    lineWidth: 1,
					                    lineColor: '#666666'
					                }
					            }
							}
						}
					});
				if(PriorityTimeSeries.piePanel.items.length == 1){
					PriorityTimeSeries.piePanel.items.items[0].hide();
					PriorityTimeSeries.piePanel.items.removeAt(0)
				}
				PriorityTimeSeries.piePanel.add(PriorityTimeSeries.pieExample);
				myMask.hide();
			}
		});
		
	},
   onafterrenderForm: function() {
		
		this.showTimeSeries(null,null,'Severity distribution for the last 24 hours generated alerts');

	},
	
	onClicktimeseriesIpShow: function(){
		var fromdate = Ext.ComponentQuery.query('#priority-report-fromdate-timeseries-date')[0].getValue();
		var todate = Ext.ComponentQuery.query('#priority-report-todate-timeseries-date')[0].getValue();
		
		var todateTitle = todate
		var fromTitle = fromdate
		
		
		if(!todateTitle){
			todateTitle = 'now'	
		}else{
			todateTitle = todate.getFullYear()+ '/' + (todate.getMonth() + 1)  + '/' +  todate.getDate()
		}
		if(!fromTitle){
			fromTitle = 'begining'	
		}else{
			fromTitle = fromTitle.getFullYear()+ '/' + (fromTitle.getMonth() + 1)  + '/' +  fromTitle.getDate()
		}
	
		this.showTimeSeries(fromdate,todate, 'Severity distribution for alerts generated from '+fromTitle+' till '+todateTitle)
	},
	
	onClicktimeseriesBackShow: function(){
		this.showTimeSeries()
	},
	
	init: function(application) {
		this.control({
			"#priority-timeseries-report-charts-panel": {
				afterrender: this.onafterrenderForm
			},"#priority-report-timeseries-charts-btn": {
				click: this.onClicktimeseriesIpShow
			},"#priority-report-timeseriesBacktomain-charts-btn": {
				click: this.onClicktimeseriesBackShow
			}
		});
	}
});