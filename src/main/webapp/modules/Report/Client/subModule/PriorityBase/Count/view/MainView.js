Ext.define('MyDesktop.Modules.Report.Client.subModule.PriorityBase.Count.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Severity Distribution',
    itemId: 'priority-count-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        PriorityBaseForm = this;
        PriorityBaseForm.BarChart = '';
        PriorityBaseForm.pieChartSourceSus = '';
        PriorityBaseForm.data = '';
        PriorityBaseForm.dataPie = '';
        PriorityBaseForm.popup = '';

        PriorityBaseForm.pieSourcePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            id:'reza11',
            style: {
                direction:'ltr !important'
            },
            items: [/*pieExample*/],
        });

        Ext.applyIf(me, {
            items: [PriorityBaseForm.pieSourcePanel]
        });
        me.callParent(arguments);
    }

});