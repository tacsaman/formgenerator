Ext.define('MyDesktop.Modules.Report.Client.subModule.PriorityBase.Count.controller.MainController', {
	extend: 'Ext.app.Controller',

	
	showPieChartSourceSus: function(){
		merge = {
				"reportName": "PriorityMashkook",
				"reportParams": []
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				 decoded = Ext.decode(response.responseText);
				 PriorityBaseForm.dataPie =decoded.data;
				 PriorityBaseForm.pieChartSourceSus = Ext.create('Chart.ux.Highcharts', {
						itemId:'prioritypiechart2',
			            
				        series: [{
				            name: 'Severity Distribution',
				        	colorByPoint: true,
				        	data:PriorityBaseForm.dataPie
				        }]
			           ,
				        chartConfig: {
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false,
			                    type: 'pie'
			                },

			                title: {
			                    text: 'Severity Distribution'
			                },
			                tooltip: {
			                    pointFormat: 'Number of alerts: <b>{point.y}</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        dataLabels: {
			                            enabled: true,
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                            style: {
			                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                            }
			                        }
			                    }
			                }
			            }
				    });
					if(PriorityBaseForm.pieSourcePanel.items.length == 1){
						PriorityBaseForm.pieSourcePanel.items.items[0].hide();
						PriorityBaseForm.pieSourcePanel.items.removeAt(0)
					}
						
					PriorityBaseForm.pieSourcePanel.add(PriorityBaseForm.pieChartSourceSus)
					myMask.hide();
			}
		});
	},
	
	onafterrenderForm: function(){
		this.showPieChartSourceSus()
	},
	
	onShowPiechart:function(){
		this.showPieChartSourceSus()
	},
	
	
	init: function(application) {
		this.control({
			"#priority-count-report-charts-panel": {
				afterrender: this.onafterrenderForm
			}
		});
	}
});
