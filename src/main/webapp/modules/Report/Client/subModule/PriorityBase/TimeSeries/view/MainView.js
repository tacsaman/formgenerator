Ext.define('MyDesktop.Modules.Report.Client.subModule.PriorityBase.TimeSeries.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Severity Time Series',
    itemId: 'priority-timeseries-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        PriorityTimeSeries = this;
        PriorityTimeSeries.data = '';
        PriorityTimeSeries.dataPie = '';
        PriorityTimeSeries.piePanel = '';
        PriorityTimeSeries.GridExamPanel = '';
        PriorityTimeSeries.pieChartDesSus = '';
        PriorityTimeSeries.pieChartDesSusdata = '';
        PriorityTimeSeries.pieChartSourceSus = '';
        PriorityTimeSeries.pieChartSourceSusdata = '';
        PriorityTimeSeries.barChartSourceSus = '';
        PriorityTimeSeries.barChartSourceSusdata = '';
        
        PriorityTimeSeries.piePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            itemId: 'PanelHighechart',
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            items: [/*zonesForm.pieExample*/],
        });

        PriorityTimeSeries.GridExamPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            title: 'Required values',
            width: 200,
            layout: 'auto',
            region: 'east',
            collapsible: true,
            //collapsed: true,
            items: [{
                xtype: 'datefield',
                fieldLabel: 'From',
                name: 'from_date',
                width: 185,
                maxValue: new Date(),
                itemId: 'priority-report-fromdate-timeseries-date'
            }, {
                xtype: 'datefield',
                fieldLabel: 'To',
                name: 'to_date',
                width: 185,
                maxValue: new Date(),
                itemId: 'priority-report-todate-timeseries-date'
            }, {
                xtype: 'button',
                text : 'Apply',
                itemId: 'priority-report-timeseries-charts-btn'
            }, {
                xtype: 'button',
                text : 'Return',
                itemId: 'priority-report-timeseriesBacktomain-charts-btn'
            }],
        });

           Ext.applyIf(me,{
               items: [PriorityTimeSeries.piePanel, PriorityTimeSeries.GridExamPanel]
           });
           me.callParent(arguments);

    },


});