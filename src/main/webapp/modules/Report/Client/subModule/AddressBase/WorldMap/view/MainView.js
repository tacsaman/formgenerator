Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.WorldMap.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Attack World Map',
    itemId: 'address-worldmap-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        WorldBaseForm = this;
        WorldBaseForm.BarChart = '';
        WorldBaseForm.WorldBarPanel = '';
        WorldBaseForm.data = '';
        WorldBaseForm.dataPie = '';

        WorldBaseForm.WorldBarPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            id:'worlmappanel',
            style: {
                direction:'ltr !important'
            },
            items: [],
        });

        var GridExamPanel = Ext.create('Ext.Button', {
            //width: 300,
            text : 'Show on world map',
            height: 50,
            layout: 'fit',
            id: 'show-world-map-btn',
            region: 'south',
            renderTo: Ext.getBody()
        });

        Ext.applyIf(me, {
            items: [WorldBaseForm.WorldBarPanel,GridExamPanel]
        });
        me.callParent(arguments);
    }
});
