Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.SourceTimeSeries.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Time Series',
    itemId: 'address-timeseries-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        SourceTimeSeries = this;
        SourceTimeSeries.data = '';
        SourceTimeSeries.dataPie = '';
        SourceTimeSeries.piePanel = '';
        SourceTimeSeries.GridExamPanel = '';
        SourceTimeSeries.pieChartDesSus = '';
        SourceTimeSeries.pieChartDesSusdata = '';
        SourceTimeSeries.pieChartSourceSus = '';
        SourceTimeSeries.pieChartSourceSusdata = '';
        SourceTimeSeries.barChartSourceSus = '';
        SourceTimeSeries.barChartSourceSusdata = '';
        
         SourceTimeSeries.piePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            itemId: 'PanelHighechart',
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            items: [/*zonesForm.pieExample*/],
        });

        SourceTimeSeries.GridExamPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            title: 'Required values',
            width: 200,
            layout: 'auto',
            region: 'east',
            collapsible: true,
            //collapsed: true,
            items: [{
                xtype: 'textfield',
                name: 'name',
                fieldLabel: 'Source Address',
                allowBlank: false,
                itemId: 'address-source-report-timeseriesipshow-charts-txt'
            },{
                xtype: 'datefield',
                fieldLabel: 'From',
                name: 'from_date',
                width: 185,
                maxValue: new Date(),
                itemId: 'address-source-report-fromdate-timeseries-date'
            }, {
                xtype: 'datefield',
                fieldLabel: 'To',
                name: 'to_date',
                width: 185,
                maxValue: new Date(),
                itemId: 'address-source-report-todate-timeseries-date'
            }, {
                xtype: 'button',
                text : 'Apply',
                itemId: 'address-source-report-timeseries-charts-btn'
            }, {
                xtype: 'button',
                text : 'Return',
                itemId: 'address-source-report-timeseriesBacktomain-charts-btn'
            }],
        });

           Ext.applyIf(me,{
               items: [SourceTimeSeries.piePanel, SourceTimeSeries.GridExamPanel]
           });
           me.callParent(arguments);

    },


});