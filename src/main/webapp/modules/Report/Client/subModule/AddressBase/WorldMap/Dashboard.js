/*!
 * Ext JS Library 4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
/**
 *
 * System Module Created Here
 *
 * Helow Helow
 *
 *
 */
Ext.define('MyDesktop.Modules.Dashboard.Client.Dashboard', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    id: 'Dashboard-win',
    itemId: 'Dashboard-window',
    init: function() {
        this.launcher = {
            text: 'Dashboard',
            iconCls: 'icon-Dashboard',
            handler: this.createWindow,
            scope: this
        };

    },

    getTools: function() {
        return [{
            xtype: 'tool',
            type: 'close',
            handler: function(e, target, header, tool) {
                var cs = Ext.state.Manager.get('PortalWidgets');
                cs['#' + panel.initialConfig.ownerCt.itemId] = false;
                Ext.ComponentQuery.query('#' + panel.initialConfig.ownerCt.itemId)[0].setVisible(false);
                Ext.state.Manager.set('PortalWidgets', cs);
            }
        }];
    },


    createWindow: function() {
        cpuValue = 0;
        var cpuGaugeStore = Ext.create('Ext.data.Store', {
            fields: ['rpm','speed'],
            data: [{"rpm":12,"speed":23}]
        });
        var requestCpuData = function() {
            var params;
            var thisthis = this;
            params = {
                id: 'viewData',
                type: 'cpu',
                token: Ext.ux.TOKEN
            }

            Ext.Ajax.request({
                url: '/cgi-bin/dashboardcgi.cgi',
                method: 'POST',
                params: params,
                success: function(conn, response, options, eOpts) {
                    cpuGaugeStore.getAt(0).set('value', Ext.decode(conn.responseText).cpu);
                },
                failure: function(conn, response, options, eOpts) {}
            });
        }
        cpuInterval = setInterval(function() {
            requestCpuData()
        }, 3000);

        var memoryGaugeStore = Ext.create('Ext.data.JsonStore', {
            fields: ['value'],
            data: [{
                'value': 0
            }]
        });
        var requestMemoryData = function() {
            var params;
            var thisthis = this;
            params = {
                id: 'viewData',
                type: 'memory',
                token: Ext.ux.TOKEN
            }

            Ext.Ajax.request({
                url: '/cgi-bin/dashboardcgi.cgi',
                method: 'POST',
                params: params,
                success: function(conn, response, options, eOpts) {
                    memoryGaugeStore.getAt(0).set('value', Ext.decode(conn.responseText).memory);
                },
                failure: function(conn, response, options, eOpts) {}
            });
        }
        memoryInterval = setInterval(function() {
            requestMemoryData()
        }, 3000);

        var diskGaugeStore = Ext.create('Ext.data.JsonStore', {
            fields: ['value'],
            data: [{
                'value': 0
            }]
        });
        var requestDiskData = function() {
            var params;
            var thisthis = this;
            params = {
                id: 'viewData',
                type: 'disk',
                token: Ext.ux.TOKEN
            }

            Ext.Ajax.request({
                url: '/cgi-bin/dashboardcgi.cgi',
                method: 'POST',
                params: params,
                success: function(conn, response, options, eOpts) {
                    diskGaugeStore.getAt(0).set('value', Ext.decode(conn.responseText).disk);
                },
                failure: function(conn, response, options, eOpts) {}
            });
        }
        diskInterval = setInterval(function() {
            requestDiskData()
        }, 3000);

        var dialColor = '#FA3421';
        var speedColor = '#474337';
	var rpmColor = '#BBB59C';
	var hiRef = '#A41E09';

        var cpuGauge = Ext.create('Ext.chart.Chart', {
            title: 'CPU',
            store: cpuGaugeStore,
            width: 200,
            height: 250,
            animate: true,
            insetPadding: 30,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                minimum: 0,
                maximum: 100,
                steps: 10,
                margin: 10
            }],
            series: [{
                type: 'gauge',
                color: dialColor,
                dataIndex: 'speed',
                yAxis: 0,
                dataLabels: {
                    color: '#E58964',
                    borderWidth: 0,
                    y: -15,
                    style: {
                        fontSize: '40px',
                        fontFamily: 'digital',
                        fontStyle: 'italic'
                    },
                    formatter: function() {
                        return this.y.toFixed(1);
                    }
                },
                dial: {
                    backgroundColor: dialColor,
                    baseLength: '90%',
                    baseWidth: 7,
                    radius: '100%',
                    topWidth: 3,
                    rearLength: '-74%',
                    borderColor: '#B17964',
                    borderWidth: 1
                },
                zIndex: 1,
                pivot: {
                    radius: '0'
                }
            }, {
                type: 'gauge',
                color: dialColor,
                dataIndex: 'rpm',
                yAxis: 2,
                dataLabels: {
                    color: '#E58964',
                    borderWidth: 0,
                    y: -40,
                    x: 5,
                    style: {
                        fontSize: '14px',
                        fontFamily: 'digital',
                        fontStyle: 'italic'
                    },
                    formatter: function() {
                        return (this.y * 1000).toFixed(0) + " rpm"
                    }
                },
                dial: {
                    backgroundColor: dialColor,
                    baseLength: '90%',
                    baseWidth: 7,
                    radius: '100%',
                    topWidth: 3,
                    rearLength: '-74%',
                    borderColor: '#631210',
                    borderWidth: 1
                },
                pivot: {
                    radius: '0'
                }
            }],
            /*listeners: {
                afterChartRendered: function(chart) {
                    var hcExt = this;
                    Demo.gaugeRunnerTask && Demo.gaugeRunnerTask.destroy();
                    Demo.gaugeRunnerTask = new Ext.util.TaskRunner();
                    Demo.gaugeRunnerTask.start({
                        run: function() {
                            hcExt.store.load({
                                scope: this,  // runnerTask
                                callback: function(records) {
                                    if (records[0].data.rpm == null && records[0].data.speed == null) {
                                        this.stop();
                                    }
                                },
                                params: { counter: this.taskRunCount }
                            });
                        },
                        interval: 500,
                        repeat: 0
                    });
                }
            },*/
            chartConfig: {
                chart: {
                    alignTicks: false,
                    events: {
                        redraw: function() {
                            this.ledBox && this.ledBox.destroy();

                            // 2nd pane center
                            var pane = this.panes[1].center;
                            this.ledBox = this.renderer.rect(pane[0] - 50, pane[1], 120, 80, 3).attr({
                                fill: 'rgba(229,137,100, 0.1)',
                                zIndex: 0
                            }).add();
                        }
                    }
                },
                title: {
                    text: 'Fiat 500 Speedometer'
                },
                pane: [{
                    startAngle: -120,
                    endAngle: 120,
                    size: 300,
                    background: [{
                        // BG color for rpm
                        backgroundColor: '#1A1815',
                        outerRadius: '68%',
                        innerRadius: '48%'
                    }, {
                        // BG color in between speed and rpm
                        backgroundColor: '#38392F',
                        outerRadius: '72%',
                        innerRadius: '67%'
                    }, {
                        // BG color for speed
                        //  backgroundColor: '#E4E3DF',
                        backgroundColor: {
                            radialGradient: {
                                cx: 0.5,
                                cy: 0.6,
                                r: 1.0
                            },
                            stops: [
                                [0.3, '#A7A9A4'],
                                //[0.6, '#FF04FF'],
                                [0.45, '#DDD'],
                                [0.7, '#EBEDEA']
                                //[0.7, '#FFFF04'],
                            ]
                        },
                        innerRadius: '72%',
                        outerRadius: '105%'
                    }, {
                        // Below rpm bg color
                        // backgroundColor: '#909090',
                        zIndex: 1,
                        backgroundColor: {
                            radialGradient: {
                                cx: 0.5,
                                cy: 0.55,
                                r: 0.5
                            },
                            stops: [
                                [0.6, '#48443B'],
                                [0.8, '#909090'],
                                [1, '#FFFFF6']
                            ]
                        },
                        outerRadius: '48%',
                        innerRadius: '40%'
                    }, {
                        backgroundColor: '#35382E',
                        zIndex: 1,
                        outerRadius: '40%',
                        innerRadius: '39%'
                    }, {
                        backgroundColor: '#16160E',
                        outerRadius: '39%'

                    }]
                }, {
                    startAngle: -120,
                    endAngle: 120,
                    size: 200
                }],
                yAxis: [{
                    title: {
                        text: 'mph km/h',
                        y: 178,
                        x: -86,
                        style: {
                            color: speedColor,
                            fontFamily: 'Squada One',
                            fontStyle: 'italic'
                        }
                    },
                    min: 0,
                    max: 140,
                    tickInterval: 10,
                    tickLength: 6,
                    lineWidth: 2,
                    lineColor: speedColor,
                    tickColor: speedColor,
                    minorTickInterval: 5,
                    minorTickLength: 3,
                    minorTickWidth: 2,
                    minorTickColor: speedColor,
                    endOnTick: false,
                    labels: {
                        rotation: 'auto',
                        step: 2,
                        style: {
                            fontFamily: 'Squada One',
                            fontSize: 28,
                            color: speedColor
                        }
                    },
                    pane: 0
                }, {
                    min: 0,
                    max: 220,
                    tickLength: 2,
                    minorTickLength: 2,
                    minorTickInterval: 10,
                    tickInterval: 10,
                    tickPosition: 'outside',
                    lineColor: speedColor,
                    tickColor: speedColor,
                    minorTickPosition: 'outside',
                    minorTickColor: speedColor,
                    labels: {
                        distance: 5,
                        rotation: 'auto',
                        style: {
                            color: speedColor ,
                            zIndex: 0
                        },
                        formatter: function() {
                            var val = null;
                            var label = this.value;
                            $.each(kmhArr, function(idx, kmh) {
                                if (label == kmh) {
                                    val = label;
                                    return false;
                                } 
                            }); 
                            return val;
                        }
                    },
                    endOnTick: false,
                    offset: -40,
                    pane: 0
                }, {
                    title: {
                        text: 'rpm x1000',
                        y: 128,
                        x: -38,
                        style: {
                            color: rpmColor,
                            fontFamily: 'Squada One',
                            fontSize: '9px',
                            fontStyle: 'italic'
                        }
                    },
                    min: 0,
                    max: 8,
                    //offset: -50,
                    minorTickInterval: 0.5,
                    tickInterval: 1,
                    tickLength: 3,
                    minorTickLength: 6,
                    lineColor: rpmColor,
                    tickWidth: 2,
                    minorTickWidth: 2,
                    lineWidth: 2,
                    labels: {
                        rotation: 'auto',
                        style: {
                            fontFamily: 'Squada One',
                            fontSize: 22,
                            color: rpmColor 
                        },
                        formatter: function() {
                            if (this.value >=6) {
                                return '<span style="color:' + hiRef +'">' + this.value + "</span>";
                            }
                            return this.value;
                        }
                    },
                    pane: 1,
                    plotBands: [{
                        from: 6,
	        	            to: 8,
	        	            color: hiRef,
                        innerRadius: '94%'
                    }]
                }]



            }
        });

        var memoryGauge = Ext.create('Ext.chart.Chart', {
            store: memoryGaugeStore,
            width: 200,
            height: 250,
            animate: true,
            insetPadding: 30,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                minimum: 0,
                maximum: 100,
                steps: 10,
                margin: 10
            }],
            series: [{
                type: 'gauge',
                field: 'value',
                donut: 30,
                colorSet: ['#F49D10', '#ddd']
            }]
        });

        var diskGauge = Ext.create('Ext.chart.Chart', {
            store: diskGaugeStore,
            width: 200,
            height: 250,
            animate: true,
            insetPadding: 30,
            axes: [{
                type: 'gauge',
                position: 'gauge',
                minimum: 0,
                maximum: 100,
                steps: 10,
                margin: 10
            }],
            series: [{
                type: 'gauge',
                field: 'value',
                donut: 30,
                colorSet: ['#F49D10', '#ddd']
            }]
        });




       /* var requestData = function() {
            var params;
            params = {
                id: 'viewData',
                type: 'cpu',
                token: Ext.ux.TOKEN
            }

            Ext.Ajax.request({
                url: '/cgi-bin/dashboardcgi.cgi',
                method: 'POST',
                params: params,
                success: function(conn, response, options, eOpts) {
                    cpuValue = Ext.decode(conn.responseText).cpu;
                    //monitoringInterfaces.bytesIn = Ext.decode(conn.responseText).data["bytes-in"]
                    //monitoringInterfaces.bytesOut = Ext.decode(conn.responseText).data["bytes-out"]
                },
                failure: function(conn, response, options, eOpts) {}
            });
        }

        var InterfaceChart = Ext.create('Chart.ux.Highcharts', {
            region: 'center',
            itemId: 'Monitoring-Interface-InterfaceChart',
            //store: store,
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },

            yAxis: {
                min: 0,
                max: 200,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'km/h'
                },
                plotBands: [{
                    from: 0,
                    to: 120,
                    color: '#55BF3B' // green
                }, {
                    from: 120,
                    to: 160,
                    color: '#DDDF0D' // yellow
                }, {
                    from: 160,
                    to: 200,
                    color: '#DF5353' // red
                }]
            },

            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },
            series: [{
                //name: 'Pckets Per Second',
                data: (function() {
                    // generate an array of random data

                    var dt = Ext.Date.parse(parseInt(new Date().getTime()) / 1000, "U");
                    var data = [],
                        time = (new Date()).getTime();
                    for (i = -60; i <= 0; i += 1) {
                        data.push([
                            time + i * 1000,
                            0
                            //Math.round(Math.random() * 100)
                        ]);
                    }
                    return data;
                }())
            }],

            chartConfig: {
                chart: {
                    type: 'spline',
                    events: {
                        load: function() {
                            // set up the updating of the chart each second			
                            var series1 = this.series[0];
                            a = self.setInterval(function() {
                                requestData();
                                var newVal = 0;
                                inc = Math.round((Math.random() - 0.5) * 20);
                                newVal = parseInt(cpuValue) + inc;
                                if (newVal < 0 || newVal > 200) {
                                    newVal = parseInt(cpuValue) - inc;
                                }
                                var x = (new Date()).getTime(); // current time
                                //y = Math.round(Math.random() * 100);
                                series1.addPoint([x, parseInt(newVal)], true, true);
                            }, 1000);
                        },
                        destroy: function() {
                            window.clearInterval(a);
                        },
                        hide: function() {
                            window.clearInterval(a);
                        }
                    }
                },
                title: {
                    text: ''
                },
            }
        });*/




        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('Dashboard-win');

        var Obj = Ext.create('MyDesktop.Modules.Dashboard.Client.Controller');
        Obj.init();

        var Obj2 = Ext.create('MyDesktop.Modules.Dashboard.Client.EditWidgetsController');
        Obj2.init();

        var storeEvent = Ext.create('MyDesktop.Modules.Dashboard.Client.store.storeEvent');
        var storeAlert = Ext.create('MyDesktop.Modules.Dashboard.Client.store.storeAlert');
        var storeGauge= Ext.create('MyDesktop.Modules.Dashboard.Client.store.Speedometer');
        Dashboard.visibilityVector = [];


        var cs = Ext.state.Manager.get('PortalWidgets');

        // a vector of checkbox ID's
        var idVector = ['#Dashboard-EditWidgets-sysStatusX', '#Dashboard-EditWidgets-devInfoX', '#Dashboard-EditWidgets-sysInfoX',
            '#Dashboard-EditWidgets-intLnkStatusX', '#Dashboard-EditWidgets-rEventsX', '#Dashboard-EditWidgets-rAlertsX',
            '#Dashboard-EditWidgets-dbInfoX', '#Dashboard-EditWidgets-currMgrX', '#Dashboard-EditWidgets-ipsInfoX'
        ];
        // a vector of visibility in the same order as ID's above; this is gonna be used in the definition of widgets to make them visible/hidden
        Dashboard.visibilityVector = cs ? [1, 1, 1, 1, 1, 1, 1, 1, 1] : [0, 0, 0, 0, 0, 0, 1, 0, 0];

        // applying the saved state to the visbility vector
        if (cs) {
            for (var j = 0; j < idVector.length; j++)
                if (cs[idVector[j] + 'Pan'])
                    Dashboard.visibilityVector[j] = 0;
        }



        var tools = [{
            type: 'close',
            handler: function(e, target, panel) {
                // if there's no checkboxgroup already save, do it by creating the window in the background
                if (!Ext.state.Manager.get('PortalWidgets'))
                    Ext.create('MyDesktop.Modules.Dashboard.Client.EditWidgets', {}).destroy();


                var cs = Ext.state.Manager.get('PortalWidgets');
                cs['#' + panel.initialConfig.ownerCt.itemId] = false;
                Ext.ComponentQuery.query('#' + panel.initialConfig.ownerCt.itemId)[0].setVisible(false);
                Ext.state.Manager.set('PortalWidgets', cs);

            }
        }];




        if (!win) {
            win = desktop.createWindow({
                //id: 'Dashboard-win',
                title: 'Dashboard Administration',
                width: 1300,
                height: 450,
                iconCls: 'icon-Dashboard',
                animCollapse: false,
                constrainHeader: true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                itemId: 'Dashboard-dashboard-win',
                items: [{
                        xtype: 'customform',
                        autoScroll: true,
                        //padding: '5 5 0 5',
                        border: false,
                        frame: true,
                        layout: 'auto',
                        url: '/cgi-bin/dashboardcgi.cgi',
                        hideSaveBtn: true,
                        hideCancelBtn: true,
                        hideResetBtn: true,
                        defaultSave: false,
                        defaultReset: false,
                        defaultafterrender: true,
                        itemId: 'Dashboard-dashboard-form',
                        loadId: 'viewData',
                        loadType: 'form',
                        items: [{
                            xtype: 'portalpanel',
                            margins: '-2 0 0 0',
                            itemId: 'Dashboard-dashboard-portalViewport',
                            items: [{
                                itemId: 'col-1',
                                style: 'padding:10px 0 10px 10px',
                                items: [{
                                    //id: 'Dashboard-EditWidgets-intLnkStatusXPan',
                                    itemId: 'Dashboard-EditWidgets-intLnkStatusXPan',
                                    title: 'Interface Link Status',
                                    hidden: Dashboard.visibilityVector[3],
                                    tools: tools,
                                    closable: false,
                                    padding: '0 0 5 0',
                                    items: [{
                                        xtype: 'form',
                                        itemId: 'Dashboard-EditWidgets-Interface-Link-Status-form',
                                        bodyStyle: 'padding:5px 5px 0',
                                        width: 550,
                                        height: 200,
                                        //anchor: "100%",
                                        items: [{
                                            layout: 'table',
                                            bodyStyle: 'padding:2px 2px 2px 2px',
                                            //anchor: "100%",
                                            items: [{
                                                border: false,
                                                items: [{
                                                    xtype: 'box',
                                                    width: 520,
                                                    height: 170,
                                                    autoEl: {
                                                        tag: 'div',
                                                        html: '<div id="pic" style="height:55px">' +
                                                            '<table valign=top heigth="100%" cellspacing=0 cellpadding=2 width="100%">' +
                                                            '<tr>' +
                                                            '<td width="80%" align="right" style="font-size: 9px; color: #ffffff; font-family:Verdana,Arial,Helvetica" valign="top" height="20" id="ProductName1" name="ProductName1"></td>' +
                                                            '<td align="left" style="font-size: 9px; color: #ffffff; font-family:Verdana,Arial,Helvetica; padding-left:5px" valign="top" height="20" id="ProductName" name="ProductName"></td>' +
                                                            '</tr>' +
                                                            '<tr>' +
                                                            '<td rowspan="2" colspan="2">' +
                                                            '<div id="portNum" style="padding-left: 115px; text-align: center; width: 280px; letter-spacing: 12px;"></div>' +
                                                            '<div id="ports" style="width:280px;padding-left:115px;text-align:center"></div>' +
                                                            '</td>' +
                                                            '</tr>' +
                                                            '</table>' +
                                                            '</div>'
                                                    }
                                                }]
                                            }]
                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-sysStatusXPan',
                                    title: 'System Status',
                                    hidden: Dashboard.visibilityVector[0],
                                    tools: tools,
                                    closable: false,
                                    items: [{
                                        xtype: 'form',
                                        //frame: true,
                                        bodyStyle: 'padding:5px 5px 0',
                                        layout: 'form',
                                        columnWidth: .5,
                                        anchor: '100%',
                                        items: [{
                                            xtype: 'displayfield',
                                            fieldLabel: 'Uptime',
                                            name: 'Uptime',
                                            itemId: 'DashboardMod-Uptime-label'
                                        }, {
                                            xtype: 'displayfield',
                                            fieldLabel: 'System Time',
                                            name: 'SystemTime',
                                            itemId: 'DashboardMod-SystemTime-label'
                                        }, {
                                            xtype: 'displayfield',
                                            fieldLabel: 'Host Name',
                                            name: 'HostName',
                                            itemId: 'DashboardMod-HostName-label'
                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-devInfoXPan',
                                    title: 'Device Information',
                                    hidden: Dashboard.visibilityVector[1],
                                    tools: tools,
                                    closable: false,
                                    items: [{
                                        xtype: 'form',
                                        //frame: true,
                                        bodyStyle: 'padding:5px 5px 0',
                                        anchor: '100%',
                                        columnWidth: .5,
                                        layout: 'form',
                                        items: [{
                                            xtype: 'displayfield',
                                            fieldLabel: 'Product',
                                            name: 'Product',
                                            itemId: 'DashboardMod-Product-label'
                                        }, {
                                            xtype: 'displayfield',
                                            fieldLabel: 'Firmware Version',
                                            name: 'FirmwareVersion',
                                            itemId: 'DashboardMod-FirmwareVersion-label'
                                        }, {
                                            xtype: 'displayfield',
                                            fieldLabel: 'Serial Number',
                                            name: 'SerialNumber',
                                            itemId: 'DashboardMod-SerialNumber-label'
                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-dbInfoXPan',
                                    title: 'URL Database Information',
                                    hidden: Dashboard.visibilityVector[6],
                                    tools: tools,
                                    closable: false,
                                    items: [{
                                        xtype: 'form',
                                        //frame: true,
                                        bodyStyle: 'padding:5px 5px 0',
                                        //anchor: '100%',
                                        defaultType: 'displayfield',
                                        columnWidth: .5,
                                        labelWidth: 150,
                                        layout: 'form',
                                        items: [{
                                            fieldLabel: 'URL Database Updating',
                                            labelWidth: 200,
                                            name: 'URLDatabaseUpdating',
                                            itemId: 'DashboardMod-URLDatabaseUpdating-label'
                                        }, {
                                            fieldLabel: 'Registration Code',
                                            name: 'URLRegistrationCode',
                                            itemId: 'DashboardMod-URLRegistrationCode-label',
                                            labelWidth: 200,
                                            //width: 200
                                        }, {
                                            fieldLabel: 'Last Updating',
                                            name: 'URLLastUpdating',
                                            itemId: 'DashboardMod-URLLastUpdating-label'
                                        }, {
                                            fieldLabel: 'Expiration Date',
                                            name: 'URLExpirationDate',
                                            itemId: 'DashboardMod-URLExpirationDate-label'
                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-ipsInfoXPan',
                                    title: 'IPS Database Information',
                                    hidden: Dashboard.visibilityVector[8],
                                    tools: tools,
                                    closable: false,
                                    items: [{
                                        xtype: 'form',
                                        labelWidth: 150,
                                        bodyStyle: 'padding-top:1px',
                                        defaultType: 'displayfield',
                                        columnWidth: .5,
                                        labelWidth: 150,
                                        layout: 'form',
                                        items: [{
                                            fieldLabel: 'IPS Database Updating',
                                            labelWidth: 200,
                                            name: 'IPSDatabaseUpdating',
                                            itemId: 'DashboardMod-IPSDatabaseUpdating-label'
                                        }, {
                                            fieldLabel: 'Registration Code',
                                            name: 'IPSRegistrationCode',
                                            itemId: 'DashboardMod-IPSRegistrationCode-label',
                                            labelWidth: 200,
                                        }, {
                                            fieldLabel: 'Last Updating',
                                            name: 'IPSLastUpdating',
                                            itemId: 'DashboardMod-IPSLastUpdating-label'
                                        }, {
                                            fieldLabel: 'Expiration Date',
                                            name: 'IPSExpirationDate',
                                            itemId: 'DashboardMod-IPSExpirationDate-label'
                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-cliPortlet',
                                    title: 'Command-line Interface',
                                    items: [{
                                        xtype: 'form',
                                        bodyStyle: 'padding-top:1px',
                                        items: []
                                    }]
                                }]
                            }, {
                                id: 'col-2',
                                style: 'padding:10px 0 10px 10px',
                                height: 400,
                                items: [{
                                    itemId: 'Dashboard-EditWidgets-sysInfoXPan',
                                    title: 'System Information',
                                    hidden: Dashboard.visibilityVector[2],
                                    height: 200,
                                    tools: tools,
                                    closable: false,
                                    items: [{
                                        xtype: 'panel',
                                        //anchor: '100%',
                                        padding: '5px',
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [cpuGauge/*, memoryGauge, diskGauge*/]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-currMgrXPan',
                                    title: 'Current Manager',
                                    hidden: Dashboard.visibilityVector[7],
                                    tools: tools,
                                    closable: false,
                                    items: [{
                                        xtype: 'form',
                                        //frame: true,
                                        bodyStyle: 'padding:5px 5px 0',
                                        anchor: '100%',
                                        defaultType: 'displayfield',
                                        columnWidth: .5,
                                        layout: 'form',
                                        items: [{
                                            fieldLabel: 'User',
                                            name: 'Administrator',
                                            itemId: 'DashboardMod-Administrator-label'
                                        }, {
                                            fieldLabel: 'Start Date/Time',
                                            name: 'StartDateTime',
                                            itemId: 'DashboardMod-StartDateTime-label'
                                        }, {
                                            fieldLabel: 'IP Address',
                                            name: 'IPAddress2',
                                            itemId: 'DashboardMod-IPAddress2-label'
                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-rEventsXPan',
                                    title: 'Recent Events',
                                    hidden: Dashboard.visibilityVector[4],
                                    tools: tools,
                                    closable: false,
                                    layout: 'fit',
                                    region: 'west',
                                    height: 250,
                                    border: false,
                                    items: [{
                                        xtype: 'customgrid',
                                        //autoScroll: true,
                                        margins: '0 5 5 5',
                                        itemId: 'Dashboard-EditWidgets-rEventsXPan-grid',
                                        //editURL: SubnetEdit.connection,
                                        hideNewBtn: false,
                                        newBtnText: 'More ...',
                                        newBtnItemId: 'Dashboard-EditWidgets-rEventsXPan-grid-add-btn',
                                        forceFit: true,
                                        hidesearchbar: false,
                                        editable: false,
                                        hidePaging: true,
                                        hideRefreshBtn: false,
                                        showRowonNew: false,
                                        store: storeEvent,
                                        model: 'MyDesktop.Modules.Dashboard.Client.model.modelEvent',
                                        /*editObject: {
                                            id: 'editSubnet',
                                            type: 'range',
                                            interface: SubnetEdit.Interface,
                                            subnet: SubnetEdit.formData.data.SubnetID,
                                            token: Ext.ux.TOKEN
                                        },
                                        editkeyArray: [
                                            ['reqID', 'ID'],
                                            ['from', 'From'],
                                            ['to', 'To'],
                                            ['status', 'Status']
                                        ],
                                        cancelObject: 'From',*/
                                        columns: [{
                                            xtype: 'gridcolumn',
                                            dataIndex: 'DateTime',
                                            text: 'Date/Time',
                                            type: 'date',
                                            itemId: 'DashboardMod-DateTime',
                                            sortable: true
                                        }, {
                                            xtype: 'gridcolumn',
                                            dataIndex: 'Message',
                                            text: 'Message',
                                            width: 400,
                                            sortable: true,

                                        }]
                                    }]
                                }, {
                                    itemId: 'Dashboard-EditWidgets-rAlertsXPan',
                                    title: 'Recent Alerts',
                                    hidden: Dashboard.visibilityVector[5],
                                    tools: tools,
                                    closable: false,
                                    layout: 'fit',
                                    anchor: '100%',
                                    height: 250,
                                    border: false,
                                    items: [{
                                        xtype: 'customgrid',
                                        //autoScroll: true,
                                        margins: '0 5 5 5',
                                        itemId: 'Dashboard-EditWidgets-rAlertsXPan-grid',
                                        //editURL: SubnetEdit.connection,
                                        hideNewBtn: false,
                                        newBtnText: 'More ...',
                                        newBtnItemId: 'Dashboard-EditWidgets-rAlertsXPan-grid-add-btn',
                                        forceFit: true,
                                        hidesearchbar: false,
                                        editable: false,
                                        hidePaging: true,
                                        hideRefreshBtn: false,
                                        showRowonNew: false,
                                        store: storeAlert,
                                        model: 'MyDesktop.Modules.Dashboard.Client.model.modelAlert',
                                        /*editObject: {
                                            id: 'editSubnet',
                                            type: 'range',
                                            interface: SubnetEdit.Interface,
                                            subnet: SubnetEdit.formData.data.SubnetID,
                                            token: Ext.ux.TOKEN
                                        },
                                        editkeyArray: [
                                            ['reqID', 'ID'],
                                            ['from', 'From'],
                                            ['to', 'To'],
                                            ['status', 'Status']
                                        ],
                                        cancelObject: 'From',*/
                                        columns: [{
                                            text: "Date/Time",
                                            itemId: 'DashboardMod-DateTime',
                                            dataIndex: 'DateTime',
                                            type: 'date',
                                            sortable: true
                                        }, {
                                            text: 'Action',
                                            dataIndex: 'Action',
                                            sortable: true
                                        }, {
                                            text: 'Severity',
                                            dataIndex: 'Severity',
                                            sortable: true
                                        }, {
                                            text: 'Message',
                                            dataIndex: 'Message',
                                            sortable: true
                                        }]
                                    }]
                                }]
                            }]
                        }],
                        tbar: [{ /*handler: this.onRefresh , */
                            iconCls: 'wui-refresh',
                            scope: this,
                            tooltip: 'Refresh'
                        }, {
                            xtype: 'splitbutton',
                            scope: this,
                            text: 'Manually',
                            /*qtip:'Refreshing Policy',id:refbtnId,*/
                            menu: [{
                                text: 'Manually',
                                type: 0 /*,handler:this.setRefreshing*/ ,
                                scope: this,
                                itemId: 'Dashboard-EditWidgets-Refresh-Manually'
                            }, {
                                text: 'Every 5 seconds',
                                type: 5 /*,handler:this.setRefreshing*/ ,
                                scope: this,
                                itemId: 'Dashboard-EditWidgets-Refresh-5seconds'
                            }, {
                                text: 'Every 15 seconds',
                                type: 15 /*,handler:this.setRefreshing*/ ,
                                scope: this
                            }, {
                                text: 'Every 30 seconds',
                                type: 30 /*,handler:this.setRefreshing*/ ,
                                scope: this
                            }, {
                                text: 'Every 60 seconds',
                                type: 60 /*,handler:this.setRefreshing*/ ,
                                scope: this
                            }]
                        }, '-', {
                            text: 'Widgets',
                            //handler: thisthis.onEditWidgets.createDelegate(thisthis),
                            iconCls: 'icon-custom',
                            itemId: 'Dashboard-EditWidgets-ViewWidgets-btn'
                        }, {
                            text: 'Reset Dashboard',
                            iconCls: 'wui-arrow-rotate-icon',
                            itemId: 'Dashboard-EditWidgets-ResetDashboard-btn'
                        }]
                    }
                    /*{xtype:'module' , url:'modules/Dashboard/Client/tree-data.json', prefix: 'Dashboard'}*/
                ]
            });
        }
        win.show();
        return win;
    }
});
