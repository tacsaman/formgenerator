Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.SourceBase.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Suspected Source Addresses',
    itemId: 'address-source-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        SourceBaseForm = this;
        SourceBaseForm.BarChart = '';
        SourceBaseForm.pieChartSourceSus = '';
        SourceBaseForm.data = '';
        SourceBaseForm.dataPie = '';
        SourceBaseForm.popup = '';

        SourceBaseForm.pieSourcePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            id:'reza',
            style: {
                direction:'ltr !important'
            },
            items: [/*pieExample*/],
        });

        var GridExamPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            title: 'Required values',
            width: 200,
            layout: 'auto',
            region: 'east',
            collapsible: true,
            //collapsed: true,
            items: [{
                xtype: 'textfield',
                name: 'name',
                fieldLabel: 'Source address',
                allowBlank: false,
                itemId: 'address-source-ip-txt'
            }, {
                xtype: 'button',
                text : 'Apply',
                itemId: 'address-source-report-charts-btn'
            }, {
                xtype: 'button',
                text : 'Return',
                itemId: 'address-source-report-charts-returnSourceSus-btn'
            }],
        });

        Ext.applyIf(me, {
            items: [SourceBaseForm.pieSourcePanel, GridExamPanel]
        });
        me.callParent(arguments);
    }

});