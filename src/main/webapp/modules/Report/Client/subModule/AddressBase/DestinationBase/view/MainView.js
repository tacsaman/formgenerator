Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.DestinationBase.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Suspected Destination Addresses',
    itemId: 'address-destination-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        DestinationBaseForm = this;
        DestinationBaseForm.DespieExample = '';
        DestinationBaseForm.pieChartDesSus = '';
        DestinationBaseForm.data = '';
        DestinationBaseForm.dataPie = '';

        DestinationBaseForm.DespiePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            id:'reza1',
            style: {
                direction:'ltr !important'
            },
            items: [/*pieExample*/],
        });

        var GridExamPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            title: 'Required values',
            width: 200,
            layout: 'auto',
            region: 'east',
            collapsible: true,
            //collapsed: true,
            items: [{
                xtype: 'textfield',
                name: 'name',
                fieldLabel: 'Destination address',
                allowBlank: false,
                itemId: 'address-dest-ip-txt'
            }, {
                xtype: 'button',
                text : 'Apply',
                itemId: 'address-dest-report-charts-btn'
            }, {
                xtype: 'button',
                text : 'Return',
                itemId: 'address-dest-report-charts-returnSourceSus-btn'
            }],
        });

        Ext.applyIf(me, {
            items: [DestinationBaseForm.DespiePanel , GridExamPanel]
        });
        me.callParent(arguments);
    }
});
