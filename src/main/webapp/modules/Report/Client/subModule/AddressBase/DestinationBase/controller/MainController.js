Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.DestinationBase.controller.MainController', {
	extend: 'Ext.app.Controller',

	showPieChartDesSus: function(){
		merge = {
				"reportName": "MaghsadMashkook",
				"reportParams": []
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				
				 decoded = Ext.decode(response.responseText);
				 DestinationBaseForm.dataPie =decoded.data;
				 DestinationBaseForm.pieChartDesSus = Ext.create('Chart.ux.Highcharts', {
						itemId:'Ourchart',
			            
				        //store: zonesForm.dataPie,
				        series: [{
				            name: 'Alert number',
				        	colorByPoint: true,
				        	data:DestinationBaseForm.dataPie,
				        	point: {
			                    events: {
			                        click: function () {
			                        	Ext.ComponentQuery.query('#address-dest-ip-txt')[0].setValue(this.options.name)
			                    		me.onClickDesReportChart()
			                        }
			                    }
			                }
				        }]
			           , 
				        chartConfig: {
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false,
			                    type: 'pie'
			                },

			                title: {
			                    text: 'Suspected Destination Addresses Distribution'
			                },
			                tooltip: {
			                    pointFormat: 'Number of alerts: <b>{point.y}</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        grid: { clickable: true },
			                        dataLabels: {
			                            enabled: true,
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                            style: {
			                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                            }
			                        }
			                    }
			                }
			            }
				    });
					//zonesForm.pieChartSourceSus.store.reload();
					//zonesForm.pieChartSourceSus.series[0].data = zonesForm.dataPie;	
					
					//zonesForm.pieChartSourceSus.refresh();
					if(DestinationBaseForm.DespiePanel.items.length == 1){
						DestinationBaseForm.DespiePanel.items.items[0].hide();
						DestinationBaseForm.DespiePanel.items.removeAt(0)
					}
						
					//zonesForm.piePanel.items.items[0].hide();
					DestinationBaseForm.DespiePanel.add(DestinationBaseForm.pieChartDesSus)
					myMask.hide();
			}
			
		});
	},
	
	
	onafterrenderDesForm: function(){
		this.showPieChartDesSus()
	},
	
	onClickDesReportChart: function(){
		var IPSource = Ext.ComponentQuery.query('#address-dest-ip-txt')[0].getValue();
		me = this;
	    //chart = Ext.ComponentQuery.query('#morteza')[0];
		merge = {
				"reportName": "MaghsadBaHoshadarFaravan",
				"reportParams": [IPSource]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			
			//async: false,
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				decoded = Ext.decode(response.responseText);
				DestinationBaseForm.data = decoded.data;
				DestinationBaseForm.DespieExample = Ext.create('Chart.ux.Highcharts', {
				    	itemId: "BarChartMaghsad",
				    	xAxis:{
				    		type: 'numeric'
				    		      },
				        yAxis: {
				            min: 0
				            
				        },
				        store: DestinationBaseForm.data,
				        series: [{
				        	name: 'Alert number',
				            dataLabels: {
				                enabled: true,
				                color: '#closableFFFFFF',
				                align: 'center',
				                format: '{point.y}', // one decimal
				                y: 10, // 10 pixels down from the top
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            },
				            point: {
			                    events: {
			                        click: function () {
				            			Ext.create('MyDesktop.Util.util').popUpWindow(this.options.name);
				            			}
			                    }
			                }
				        }],
				        chartConfig: {
				        	chart: {
				                type: 'column'
				            },xAxis: {
								   type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							},

				           title: {
				                text: 'Distribution Time Series of ' + IPSource
				            },
				            tooltip: {
				                
				            }
				        }
				    });
				 	DestinationBaseForm.DespieExample.store.reload()
					DestinationBaseForm.DespieExample.series[0].data = DestinationBaseForm.data
					DestinationBaseForm.DespieExample.refresh();
					if(DestinationBaseForm.DespiePanel.items.length == 1){
						DestinationBaseForm.DespiePanel.items.items[0].hide();
						DestinationBaseForm.DespiePanel.items.removeAt(0)
					}
						
					//zonesForm.piePanel.items.items[0].hide();
					DestinationBaseForm.DespiePanel.add(DestinationBaseForm.DespieExample)
					myMask.hide();
			}
		});
	},
	
	onShowDesPiechart:function(){
		this.showPieChartDesSus()
	},
	
	
	init: function(application) {
		this.control({
			"#address-destination-report-charts-panel": {
				afterrender: this.onafterrenderDesForm
			},"#address-dest-report-charts-btn": {
				click: this.onClickDesReportChart
			},"#address-dest-report-charts-returnSourceSus-btn": {
				click: this.onShowDesPiechart
			}
		});
	}
});
