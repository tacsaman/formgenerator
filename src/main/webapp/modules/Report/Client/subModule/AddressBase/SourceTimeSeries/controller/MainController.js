Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.SourceTimeSeries.controller.MainController', {
	extend: 'Ext.app.Controller',
	
	showPieChartDesSusInHour: function(hour){
		merge = {
				"reportName": "MaghsadMashkook",
				"reportParams": [hour]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				 decoded = Ext.decode(response.responseText);
				 SourceTimeSeries.pieChartDesSusdata =decoded.data;
				 SourceTimeSeries.pieChartDesSus = Ext.create('Chart.ux.Highcharts', {
						itemId:'Ourchart',
			            
				        //store: zonesForm.dataPie,
				        series: [{
				            name: 'Alert number',
				        	colorByPoint: true,
				        	data: SourceTimeSeries.pieChartDesSusdata
				        }]
			           , 
				        chartConfig: {
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false,
			                    type: 'pie'
			                },

			                title: {
			                    text: 'Suspected destination at '+ hour
			                },
			                tooltip: {
			                    pointFormat: 'Number of alerts: <b>{point.y}</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        grid: { clickable: true },
			                        dataLabels: {
			                            enabled: true,
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                            style: {
			                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                            }
			                        }
			                    }
			                }
			            }
				    });
				 	if(SourceTimeSeries.piePanel.items.length == 1){
						SourceTimeSeries.piePanel.items.items[0].hide();
						SourceTimeSeries.piePanel.items.removeAt(0)
					}
				 	if(SourceTimeSeries.piePanel.items.length == 2){
						SourceTimeSeries.piePanel.items.items[0].hide();
						SourceTimeSeries.piePanel.items.removeAt(0)
					}
				 	if(SourceTimeSeries.piePanel.items.length == 3){
						SourceTimeSeries.piePanel.items.items[0].hide();
						SourceTimeSeries.piePanel.items.removeAt(0)
					}
					SourceTimeSeries.piePanel.add(0,SourceTimeSeries.pieChartDesSus)
					myMask.hide();
			}
		});
		
	},
	
	showPieChartSourceSusInHour: function(hour){
		merge = {
				"reportName": "MabdaMashkook",
				"reportParams": [hour]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				 decoded = Ext.decode(response.responseText);
				 SourceTimeSeries.pieChartSourceSusdata =decoded.data;
				 SourceTimeSeries.pieChartSourceSus = Ext.create('Chart.ux.Highcharts', {
						itemId:'Ourchart2',
			            
				        //store: zonesForm.dataPie,
				        series: [{
				            name: 'Alert number',
				        	colorByPoint: true,
				        	data: SourceTimeSeries.pieChartSourceSusdata
//				        	point: {
//			                    events: {
//			                        click: function () {
//			                        	Ext.ComponentQuery.query('#address-dest-ip-txt')[0].setValue(this.options.name)
//			                    		me.onClickDesReportChart()
//			                        }
//			                    }
//			                }
				        }]
			           , 
				        chartConfig: {
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false,
			                    type: 'pie'
			                },

			                title: {
			                    text: 'Suspected source at '+ hour
			                },
			                tooltip: {
			                    pointFormat: 'Number of alerts: <b>{point.y}</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        grid: { clickable: true },
			                        dataLabels: {
			                            enabled: true,
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                            style: {
			                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                            }
			                        }
			                    }
			                }
			            }
				    });
					SourceTimeSeries.piePanel.add(1,SourceTimeSeries.pieChartSourceSus)
					myMask.hide();
			}
		});
		
	},
	
	showBarChartSignatureSusInHour: function(hour){
		me = this;
	merge = {
			"reportName": "MabdaBaHoshadarFaravan",
			"reportParams": ['',hour]
	}
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
	myMask.show();
	
	var ajax = Ext.Ajax.request({
		headers: {
            'Content-Type': 'application/json'
		},
		url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
		method: 'POST',
		
		//async: false,
		params: Ext.encode(merge),
		success: function(response) {
			Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
			 decoded = Ext.decode(response.responseText);
			 SourceTimeSeries.barChartSourceSusdata = decoded.data;
			 SourceTimeSeries.barChartSourceSus = Ext.create('Chart.ux.Highcharts', {
			    	itemId: "BarchartSignatureTotal",
			        store: SourceTimeSeries.barChartSourceSusdata,
			        series: [{
			            name: 'Alert number',
			            dataLabels: {
			                enabled: true,
			                color: '#FFFFFF',
			                align: 'center',
			                format: '{point.y}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            },
			            point: {
		                    events: {
		                        click: function () {
			            			Ext.create('MyDesktop.Util.util').popUpWindow(this.options.name);
		                        }
		                    }
		                }
			        }],
			        chartConfig: {
			            chart: {
			                type: 'column'
			            },xAxis: {
							   type: 'category',
						        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
						        minorGridLineWidth: 0, //
						        tickInterval : 1,
						        showFirstLabel: true
						},
			            title: {
			                text: 'Alert distribution at '+ hour
			            },
			            tooltip: {
			                pointFormat: 'Number of alerts: <b>{point.y:.1f}</b>'
			            },
			            dataLabels: {
			                enabled: true,
			                color: '#FFFFFF',
			                align: 'center',
			                format: '{point.y}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        }
			    });
			SourceTimeSeries.barChartSourceSus.store.reload()
			SourceTimeSeries.barChartSourceSus.series[0].data = SourceTimeSeries.barChartSourceSusdata
			SourceTimeSeries.barChartSourceSus.refresh();
			
			SourceTimeSeries.piePanel.add(2,SourceTimeSeries.barChartSourceSus)
			myMask.hide();
		}
	});

	},
	
	showTimeSeries: function(ip,fromDate,toDate,title){
		merge = {
				"reportName": "TimeSeriesHoshdar",
				"reportParams": [ip,fromDate,toDate]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				decoded = Ext.decode(response.responseText);
				 SourceTimeSeries.data =decoded.data;
				 SourceTimeSeries.pieExample = Ext.create('Chart.ux.Highcharts', {
						itemId: 'Ourchart',
						xAxis: {
							type: "category",
							tickWidth: 4,
							tickLength: 10
						},

						yAxis: {
							 min: 0,
							title: {
								text: 'Exchange rate'
							}
						},
						store: SourceTimeSeries.data,

						series: [{
							type: 'area',
							name: 'Alert number',
							point: {
			                    events: {
			                        click: function () {
			                        	if (this.options.name.indexOf(":") !=-1) {
			            					me.showPieChartDesSusInHour(this.options.name);
			                        		me.showPieChartSourceSusInHour(this.options.name);
			                        		me.showBarChartSignatureSusInHour(this.options.name);
			                        	}
			                        	else{
			                        		var todateTitle = this.options.name+" 00:00"
			                        		var fromTitle = this.options.name+" 23:59"
			                        		var ipsourceTitle = ip
			                        		
			                        		if(!todateTitle){
			                        			todateTitle = 'now'	
			                        		}if(!fromTitle){
			                        			fromTitle = 'begining'	
			                        		}if(!ipsourceTitle){
			                        			ipsourceTitle = 'all of the alerts'	
			                        		}
			                        		me.showTimeSeries(ip,this.options.name+"T00:00",this.options.name+"T23:59", 'Alerts generated from '+fromTitle+' to '+todateTitle+' for '+ipsourceTitle)
			                        	}
			                        }
			                    }
			                }
						}],
						chartConfig: {
							chart: {
								plotBackgroundColor: null,
								plotBorderWidth: null,
								plotShadow: true,
								zoomType: 'x',
							},
							  xAxis: {
								 type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							    },

							yAxis: {
								 min: 0,
								title: {
									text: 'Number of alert'
								}
							},
							//store: zonesForm.data,
							title: {
								text: title
							},
							tooltip: {
								pointFormat: 'Number of alerts: <b>{point.y} </b>'
							},
							plotOptions: {
								area: {
									fillColor: {
										linearGradient: {
											x1: 0,
											y1: 0,
											x2: 0,
											y2: 1
										},
										stops: [
											[0, Highcharts.getOptions().colors[0]],
											[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
										]
									},
									marker: {
										radius: 2
									},
									lineWidth: 1,
									states: {
										hover: {
											lineWidth: 1
										}
									},
									threshold: null
								}
							}
						}
					});
				 SourceTimeSeries.pieExample.store.reload();
				 SourceTimeSeries.pieExample.series[0].data = SourceTimeSeries.data;	
					
				 SourceTimeSeries.pieExample.refresh();
				if(SourceTimeSeries.piePanel.items.length == 1){
					SourceTimeSeries.piePanel.items.items[0].hide();
					SourceTimeSeries.piePanel.items.removeAt(0)
				}
			
				if(SourceTimeSeries.piePanel.items.length == 3){
					SourceTimeSeries.piePanel.items.items[2].hide();
					SourceTimeSeries.piePanel.items.removeAt(2)
					SourceTimeSeries.piePanel.items.items[1].hide();
					SourceTimeSeries.piePanel.items.removeAt(1)
					SourceTimeSeries.piePanel.items.items[0].hide();
					SourceTimeSeries.piePanel.items.removeAt(0)
				}
				SourceTimeSeries.piePanel.add(SourceTimeSeries.pieExample)
				myMask.hide();
			}
		});
		
	},
	
	onafterrenderForm: function() {
		this.showTimeSeries(null,null,null,'Alerts generated in the last 24 hours');
	},
	
	onClicktimeseriesIpShow: function(){
		var IPSource = Ext.ComponentQuery.query('#address-source-report-timeseriesipshow-charts-txt')[0].getValue();
		var fromdate = Ext.ComponentQuery.query('#address-source-report-fromdate-timeseries-date')[0].getValue();
		var todate = Ext.ComponentQuery.query('#address-source-report-todate-timeseries-date')[0].getValue();
		
		var todateTitle = todate
		var fromTitle = fromdate
		var ipsourceTitle = IPSource
		
		
		if(!todateTitle){
			todateTitle = 'now'	
		}else{
			todateTitle = todate.getFullYear()+ '/' + (todate.getMonth() + 1)  + '/' +  todate.getDate()
		}
		if(!fromTitle){
			fromTitle = 'begining'	
		}else{
			fromTitle = fromTitle.getFullYear()+ '/' + (fromTitle.getMonth() + 1)  + '/' +  fromTitle.getDate()
		}
		if(!ipsourceTitle){
			ipsourceTitle = 'all of the alerts'	
		}
		
		this.showTimeSeries(IPSource,fromdate,todate,  'Alerts generated from '+fromTitle+' to '+todateTitle+' for '+ipsourceTitle)
	},
	
	onClicktimeseriesBackShow: function(){
		this.showTimeSeries()
	},
	
	init: function(application) {
		this.control({
			"#address-timeseries-report-charts-panel": {
				afterrender: this.onafterrenderForm
			},"#address-source-report-timeseries-charts-btn": {
				click: this.onClicktimeseriesIpShow
			},"#address-source-report-timeseriesBacktomain-charts-btn": {
				click: this.onClicktimeseriesBackShow
			}
		});
	}
});