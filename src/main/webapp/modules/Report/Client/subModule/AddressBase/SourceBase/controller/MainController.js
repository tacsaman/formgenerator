Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.SourceBase.controller.MainController', {
	extend: 'Ext.app.Controller',

	
	showPieChartSourceSus: function(){
		merge = {
				"reportName": "MabdaMashkook",
				"reportParams": []
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				
				 decoded = Ext.decode(response.responseText);
				 SourceBaseForm.dataPie =decoded.data;
				 SourceBaseForm.pieChartSourceSus = Ext.create('Chart.ux.Highcharts', {
						itemId:'Ourchart',
			            
				        series: [{
				            name: 'Alert number',
				        	colorByPoint: true,
				        	data:SourceBaseForm.dataPie,
				        	point: {
			                    events: {
			                        click: function () {
			                        	Ext.ComponentQuery.query('#address-source-ip-txt')[0].setValue(this.options.name)
			                    		me.onClickReportChart()
			                        }
			                    }
			                }
				        }]
			           ,
				        chartConfig: {
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false,
			                    type: 'pie'
			                },

			                title: {
			                    text: 'Suspected Source Addresses Distribution'
			                },
			                tooltip: {
			                    pointFormat: 'Number of alerts: <b>{point.y}</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        dataLabels: {
			                            enabled: true,
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                            style: {
			                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                            }
			                        }
			                    }
			                }
			            }
				    });
					if(SourceBaseForm.pieSourcePanel.items.length == 1){
						SourceBaseForm.pieSourcePanel.items.items[0].hide();
						SourceBaseForm.pieSourcePanel.items.removeAt(0)
					}
						
					SourceBaseForm.pieSourcePanel.add(SourceBaseForm.pieChartSourceSus)
					myMask.hide();
			}
		});
		
	},
	
	onafterrenderForm: function(){
		this.showPieChartSourceSus()
	},
	
	onClickReportChart: function(){
		var IPSource = Ext.ComponentQuery.query('#address-source-ip-txt')[0].getValue();
		me = this;
	    //chart = Ext.ComponentQuery.query('#morteza')[0];
		merge = {
				"reportName": "MabdaBaHoshadarFaravan",
				"reportParams": [IPSource]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			
			//async: false,
			params: Ext.encode(merge),
			success: function(response) {
				Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
				 decoded = Ext.decode(response.responseText);
				 SourceBaseForm.data = decoded.data;
				 SourceBaseForm.BarChart = Ext.create('Chart.ux.Highcharts', {
				    	itemId: "BarchartMabdaSignature",
				        xAxis: {
				            type: 'category',
				            labels: {
				                rotation: -45,
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            }
				        },

				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Population (millions)'
				            }
				        },
				        store: SourceBaseForm.data,
				        series: [{
				            name: 'Alert number',
				            dataLabels: {
				                enabled: true,
				                color: '#FFFFFF',
				                align: 'center',
				                format: '{point.y}', // one decimal
				                y: 10, // 10 pixels down from the top
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            },
				            point: {
			                    events: {
			                        click: function () {
				            			Ext.create('MyDesktop.Util.util').popUpWindow(this.options.name);
			                        }
			                    }
			                }
				        }],
				        chartConfig: {
				            chart: {
				                type: 'column'
				            },
				            xAxis: {
								   type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							},

				            title: {
				                text: 'Distribution Time Series of ' + IPSource
				            },
				            tooltip: {
				                pointFormat: 'Number of alerts: <b>{point.y:.1f}</b>'
				            },
				            xAxis: {
								 type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							    },
				            dataLabels: {
				                enabled: true,
				                color: '#FFFFFF',
				                align: 'center',
				                format: '{point.y}', // one decimal
				                y: 10, // 10 pixels down from the top
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                },
				            }
				        }
				    });
				 	SourceBaseForm.BarChart.store.reload()
					SourceBaseForm.BarChart.series[0].data = SourceBaseForm.data
					SourceBaseForm.BarChart.refresh();
					if(SourceBaseForm.pieSourcePanel.items.length == 1){
						SourceBaseForm.pieSourcePanel.items.items[0].hide();
						SourceBaseForm.pieSourcePanel.items.removeAt(0)
					}
						
					SourceBaseForm.pieSourcePanel.add(SourceBaseForm.BarChart)
					myMask.hide();
			}
		});
	
	},
	
	onShowPiechart:function(){
		this.showPieChartSourceSus()
	},
	
	
	init: function(application) {
		this.control({
			"#address-source-report-charts-panel": {
				afterrender: this.onafterrenderForm
			},"#address-source-report-charts-btn": {
				click: this.onClickReportChart
			},"#address-source-report-charts-returnSourceSus-btn": {
				click: this.onShowPiechart
			}
		});
	}
	
});
