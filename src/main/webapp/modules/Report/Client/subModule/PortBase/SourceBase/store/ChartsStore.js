Ext.define('MyDesktop.Modules.Report.Client.subModule.AddressBase.Charts.store.ChartsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Report.Client.subModule.AddressBase.Charts.model.ChartsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				model: 'MyDesktop.Modules.Report.Client.subModule.AddressBase.Charts.model.ChartsModel',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/Charts/getAll',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
