Ext.define('MyDesktop.Modules.Report.Client.subModule.PortBase.SourceBase.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Suspected Source Port',
    itemId: 'port-source-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
    var me = this;
    SourceBasePortForm = this;
    SourceBasePortForm.BarChart = '';
    SourceBasePortForm.pieChartSourceSus = '';
    SourceBasePortForm.data = '';
    SourceBasePortForm.dataPie = '';
    SourceBasePortForm.pieExample = '';
    SourceBasePortForm.popup = '';
    SourceBasePortForm.pieSourcePanel = Ext.create('Ext.panel.Panel', {
        bodyPadding: 5, // Don't want content to crunch against the borders
        //width: 300,
        //height: 500,
        layout: 'fit',
        region: 'center',
        autoScroll: true,
        id:'portpanel',
        style: {
            direction:'ltr !important'
        },
        items: [/*pieExample*/],
    });

    var GridExamPanel = Ext.create('Ext.panel.Panel', {
        bodyPadding: 5, // Don't want content to crunch against the borders
        //width: 300,
        title: 'Required values',
        width: 200,
        layout: 'auto',
        region: 'east',
        collapsible: true,
        //collapsed: true,
        items: [{
            xtype: 'textfield',
            name: 'name',
            fieldLabel: 'Source port',
            allowBlank: false,
            itemId: 'port-source-ip-txt'
        }, {
            xtype: 'radio',
            name: 'name',
            fieldLabel: 'TCP',
            allowBlank: false,
            itemId: 'tcp-port-source-radio'
        },{
            xtype: 'radio',
            name: 'name',
            fieldLabel: 'UDP',
            allowBlank: false,
            itemId: 'udp-port-source-radio'
        },{
            xtype: 'button',
            text : 'Apply',
            itemId: 'port-source-report-charts-btn'
        }, {
            xtype: 'button',
            text : 'Return',
            itemId: 'port-source-report-charts-returnSourceSus-btn'
        }],
    });

    Ext.applyIf(me, {
        items: [SourceBasePortForm.pieSourcePanel, GridExamPanel]
    });
    me.callParent(arguments);
}

});
