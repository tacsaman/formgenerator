Ext.define('MyDesktop.Modules.Report.Client.subModule.PortBase.DestinationBase.view.MainView', {
	extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
	title: 'Suspected Destination Ports',
	itemId: 'port-dest-report-charts-panel',
	layout: 'border',
    height: 800,

    initComponent: function() {
	var me = this;
    DestBasePortForm = this;
    DestBasePortForm.BarChart = '';
    DestBasePortForm.pieChartSourceSus = '';
    DestBasePortForm.data = '';
    DestBasePortForm.dataPie = '';
    DestBasePortForm.pieExample = '';
    DestBasePortForm.popup = '';
    DestBasePortForm.pieDestPanel = Ext.create('Ext.panel.Panel', {
        bodyPadding: 5, // Don't want content to crunch against the borders
        //width: 300,
        //height: 500,
        layout: 'fit',
        region: 'center',
        autoScroll: true,
        id:'destPortpanel',
        style: {
            direction:'ltr !important'
        },
        items: [/*pieExample*/],
    });

    var GridExamPanel = Ext.create('Ext.panel.Panel', {
        bodyPadding: 5, // Don't want content to crunch against the borders
        //width: 300,
        title: 'Required values',
        width: 200,
        layout: 'auto',
        region: 'east',
        collapsible: true,
        //collapsed: true,
        items: [{
            xtype: 'textfield',
            name: 'name',
            fieldLabel: 'Destination port',
            allowBlank: false,
            itemId: 'port-dest-ip-txt'
        }, {
            xtype: 'radio',
            name: 'name',
            fieldLabel: 'TCP',
            allowBlank: false,
            itemId: 'tcp-port-dest-radio'
        },{
            xtype: 'radio',
            name: 'name',
            fieldLabel: 'UDP',
            allowBlank: false,
            itemId: 'udp-port-dest-radio'
        },{
            xtype: 'button',
            text : 'Apply',
            itemId: 'port-dest-report-charts-btn'
        }, {
            xtype: 'button',
            text : 'Return',
            itemId: 'port-dest-report-charts-returnSourceSus-btn'
        }],
    });

    Ext.applyIf(me, {
        items: [DestBasePortForm.pieDestPanel, GridExamPanel]
    });
    me.callParent(arguments);
}

});