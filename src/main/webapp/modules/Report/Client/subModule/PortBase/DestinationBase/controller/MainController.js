Ext.define('MyDesktop.Modules.Report.Client.subModule.PortBase.DestinationBase.controller.MainController', {
	extend: 'Ext.app.Controller',

	showBaseDestPortChart: function(state, portnum, show, index){
	merge = {
			"reportName": "PortMaghsadBaHoshdarFaravan",
			"reportParams": [state, portnum]
	}
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
	myMask.show();
	
	me = this;
	var ajax = Ext.Ajax.request({
		headers: {
            'Content-Type': 'application/json'
		},
		url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
		method: 'POST',
		//async: false,
		params: Ext.encode(merge),
		success: function(response) {
			Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
			 decoded = Ext.decode(response.responseText);
			 DestBasePortForm.data = decoded.data;
			 DestBasePortForm.BarChart = Ext.create('Chart.ux.Highcharts', {
			    	itemId: "baseDestPort"+index,
			        xAxis: {
			            type: 'category',
			            labels: {
			                rotation: -45,
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        },

			        store: DestBasePortForm.data,
			        series: [{
			            name: 'Alert number',
			            dataLabels: {
			                enabled: true,
			                color: '#FFFFFF',
			                align: 'center',
			                format: '{point.y}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            },
			            
				        point: {
			                    events: {
			                        click: function () {
			            				  	Ext.ComponentQuery.query('#port-dest-ip-txt')[0].setValue(this.options.name)
			            				  	if (state == "tcp")
					                        	Ext.ComponentQuery.query('#tcp-port-dest-radio')[0].setValue(true);
					                    	else
					                        	Ext.ComponentQuery.query('#udp-port-dest-radio')[0].setValue(true);
			            				  	
			            				  	me.showSpecificPortChart(state, this.options.name)	
			                        }
			                    }
			                }
			        }],
			        chartConfig: {
			            chart: {
			                type: 'column'
			            },
			            
			            xAxis: {
							 type: 'category',
						        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
						        minorGridLineWidth: 0, //
						        tickInterval : 1,
						        showFirstLabel: true
						    },

			            title: {
			                text:  'Alerts distribution for destination '+state+' ports'
			            },
			            tooltip: {
			                pointFormat: 'Number of alerts: <b>{point.y}</b>'
			            },
			            dataLabels: {
			                enabled: true,
			                color: '#FFFFFF',
			                align: 'center',
			                format: '{point.y}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        }
			 });
			 DestBasePortForm.BarChart.store.reload();
			 DestBasePortForm.BarChart.series[0].data = DestBasePortForm.data;
			 DestBasePortForm.BarChart.refresh();
			 if(DestBasePortForm.pieDestPanel.items.length == 2){
				 DestBasePortForm.pieDestPanel.items.items[1].hide();
				 DestBasePortForm.pieDestPanel.items.removeAt(1);
				 DestBasePortForm.pieDestPanel.items.items[0].hide();
				 DestBasePortForm.pieDestPanel.items.removeAt(0);
			 }
			 if(show == true && DestBasePortForm.pieDestPanel.items.length == 1){
				 DestBasePortForm.pieDestPanel.items.items[0].hide();
				 DestBasePortForm.pieDestPanel.items.removeAt(0);
			 }
			 
			 DestBasePortForm.pieDestPanel.add(index, DestBasePortForm.BarChart)
			 myMask.hide();
	}
	});
},

showSpecificPortChart: function(state, portnum){
	merge = {
			"reportName": "PortMaghsadBaHoshdarFaravan",
			"reportParams": [state, portnum]
	}
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
	myMask.show();
	
	me = this;
	var ajax = Ext.Ajax.request({
		headers: {
            'Content-Type': 'application/json'
		},
		url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
		method: 'POST',
		
		//async: false,
		params: Ext.encode(merge),
		success: function(response) {
			Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
			 decoded = Ext.decode(response.responseText);
			 DestBasePortForm.data =decoded.data;
			 DestBasePortForm.pieExample = Ext.create('Chart.ux.Highcharts', {
					itemId: 'specificDestPort',
					xAxis: {
						type: "datetime",
						tickWidth: 4,
						tickLength: 10
					},

					yAxis: {
						 min: 0,
						title: {
							text: 'Exchange rate'
						}
					},
					store: DestBasePortForm.data,

					series: [{
						type: 'area',
						name: 'Alert number',
						point: {
		                    events: {
		                        click: function () {
									  var portNumber = Ext.ComponentQuery.query('#port-dest-ip-txt')[0].getValue();
		                        	  me.showSpecPortChartinHour(state, portNumber, this.options.name)
		                        }
		                    }
		                }
					}],
					chartConfig: {
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: true,
							zoomType: 'x',
						},
						  xAxis: {
							 type: 'category',
						        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
						        minorGridLineWidth: 0, //
						        tickInterval : 1,
						        showFirstLabel: true
						    },

						yAxis: {
							 min: 0,
							title: {
								text: 'Number of alert'
							}
						},
						//store: zonesForm.data,
						title: {
							text:  'Alerts distribution for destination '+state+' port number '+portnum
						},
						tooltip: {
							pointFormat: 'Number of alerts: <b>{point.y} </b>'
						},
						plotOptions: {
							area: {
								fillColor: {
									linearGradient: {
										x1: 0,
										y1: 0,
										x2: 0,
										y2: 1
									},
									stops: [
										[0, Highcharts.getOptions().colors[0]],
										[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
									]
								},
								marker: {
									radius: 2
								},
								lineWidth: 1,
								states: {
									hover: {
										lineWidth: 1
									}
								},
								threshold: null
							}
						}
					}
				});
			 
			 DestBasePortForm.pieExample.store.reload();
			 DestBasePortForm.pieExample.series[0].data = DestBasePortForm.data;	
			 DestBasePortForm.pieExample.refresh();
			 
			if(DestBasePortForm.pieDestPanel.items.length == 2){
				DestBasePortForm.pieDestPanel.items.items[1].hide();
				DestBasePortForm.pieDestPanel.items.removeAt(1)
				DestBasePortForm.pieDestPanel.items.items[0].hide();
				DestBasePortForm.pieDestPanel.items.removeAt(0)
			}
			if(DestBasePortForm.pieDestPanel.items.length == 1){
				DestBasePortForm.pieDestPanel.items.items[0].hide();
				DestBasePortForm.pieDestPanel.items.removeAt(0)
			}
			
			DestBasePortForm.pieDestPanel.add(DestBasePortForm.pieExample)
			myMask.hide();
		}
	});
},

showSpecPortChartinHour: function(state, portnum, date){
	merge = {
			"reportName": "PortMaghsadBaHoshdarFaravan",
			"reportParams": [state, portnum, date]
	}
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
	myMask.show();
	
	me = this;
	var ajax = Ext.Ajax.request({
		headers: {
            'Content-Type': 'application/json'
		},
		url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
		method: 'POST',
		
		//async: false,
		params: Ext.encode(merge),
		success: function(response) {
			 decoded = Ext.decode(response.responseText);
			 DestBasePortForm.data = decoded.data;
			 DestBasePortForm.BarChart = Ext.create('Chart.ux.Highcharts', {
			    	itemId: "specDestPortINHour",
			        xAxis: {
			            type: 'category',
			            labels: {
			                rotation: -45,
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        },

			        store: DestBasePortForm.data,
			        series: [{
			            name: 'Alert number',
			            dataLabels: {
			                enabled: true,
			                color: '#FFFFFF',
			                align: 'center',
			                format: '{point.y}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            },
			            point: {
			            	 events: {
			            			click: function () {
			            				Ext.create('MyDesktop.Util.util').popUpWindow(this.options.name);
			            				}
			            		}
			            	}
			        }],
			        chartConfig: {
			            chart: {
			                type: 'column'
			            },
			            
			            xAxis: {
							 type: 'category',
						        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
						        minorGridLineWidth: 0, //
						        tickInterval : 1,
						        showFirstLabel: true
						    },

			            title: {
			                text:  'Alerts generated for destination '+state+' port number '+portnum+' at '+date
			            },
			            tooltip: {
			                pointFormat: 'Number of alerts: <b>{point.y}</b>'
			            },
			            dataLabels: {
			                enabled: true,
			                color: '#FFFFFF',
			                align: 'center',
			                format: '{point.y}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        }
			 });
			 DestBasePortForm.BarChart.store.reload()
			 DestBasePortForm.BarChart.series[0].data = DestBasePortForm.data
			 DestBasePortForm.BarChart.refresh();
			 
			 if(DestBasePortForm.pieDestPanel.items.length == 1){
				 DestBasePortForm.pieDestPanel.items.items[0].hide();
				 DestBasePortForm.pieDestPanel.items.removeAt(0)
			 }
			 DestBasePortForm.pieDestPanel.add(DestBasePortForm.BarChart)
			 myMask.hide();
	}
	});
},

	onafterrenderForm: function(){
		this.showBaseDestPortChart('tcp', '', false, 1);
		this.showBaseDestPortChart('udp', '',false, 2);
	},
	
	onClickPortReportChart:function(){
		var portNumber = Ext.ComponentQuery.query('#port-dest-ip-txt')[0].getValue();
		
		if( Ext.ComponentQuery.query('#tcp-port-dest-radio')[0].getValue())
			this.showSpecificPortChart('tcp', portNumber);
		if( Ext.ComponentQuery.query('#udp-port-dest-radio')[0].getValue())
			this.showSpecificPortChart('udp', portNumber);
	},
	
	onShowPortReportChart:function(){
		if(Ext.ComponentQuery.query('#tcp-port-dest-radio')[0].getValue()){
			this.showBaseDestPortChart('tcp', '', true, 1);
			this.showBaseDestPortChart('udp', '', false, 2);
		}else if (Ext.ComponentQuery.query('#udp-port-dest-radio')[0].getValue()){
			this.showBaseDestPortChart('udp', '', true, 2);
			this.showBaseDestPortChart('tcp', '', false, 1);
		}else{
			this.showBaseDestPortChart('tcp', '', false, 1);
			this.showBaseDestPortChart('udp', '', false, 2);
		}
	},
	
	init: function(application) {
		this.control({
			"#port-dest-report-charts-panel": {
				afterrender: this.onafterrenderForm
			},"#port-dest-report-charts-btn": {
				click: this.onClickPortReportChart
			},"#port-dest-report-charts-returnSourceSus-btn": {
				click: this.onShowPortReportChart
			}
		});
	}
});

