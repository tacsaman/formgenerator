Ext.define('MyDesktop.Modules.Report.Client.subModule.ProtocolBase.TimeSeries.controller.MainController', {
	extend: 'Ext.app.Controller',
	
	showTimeSeries: function(fromDate,toDate,title){
		merge = {
				"reportName": "TimeSeriesProtocol",
				"reportParams": [null,fromDate,toDate]
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				decoded = Ext.decode(response.responseText);

				 
				ProtocolTimeSeries.data =decoded.data;
				ProtocolTimeSeries.pieExample = Ext.create('Chart.ux.Highcharts', {
						itemId: 'protocoltimeseries',
						xField: 'name',
						series:  ProtocolTimeSeries.data,
						chartConfig: {
							chart: {     
								type: 'area'
								
							}
							,xAxis: {
								   type: 'category',
							        minorTickWidth: 1, // this is by default 0 and is why you don't see ticks
							        minorGridLineWidth: 0, //
							        tickInterval : 1,
							        showFirstLabel: true
							},
							yAxis: {
								 min: 0,
								title: {
									text: 'Number of alert'
								}
							},
							//store: zonesForm.data,
							title: {
								text: title
							},
							tooltip: {
								pointFormat: 'Number of alerts: <b>{point.y} </b>'
							},
							plotOptions: {
								area: {
					                stacking: 'normal',
					                lineColor: '#666666',
					                lineWidth: 1,
					                marker: {
					                    lineWidth: 1,
					                    lineColor: '#666666'

					                }
					            }
							}
						}
					});
				if(ProtocolTimeSeries.piePanel.items.length == 1){
					ProtocolTimeSeries.piePanel.items.items[0].hide();
					ProtocolTimeSeries.piePanel.items.removeAt(0)
				}
			ProtocolTimeSeries.piePanel.add(ProtocolTimeSeries.pieExample);
			myMask.hide();
			}
		});
		
	},
   onafterrenderForm: function() {
		
		this.showTimeSeries(null,null,'Protocol distribution for the last 24 hours generated alerts');

	},
	
	onClicktimeseriesIpShow: function(){
		var fromdate = Ext.ComponentQuery.query('#protocol-report-fromdate-timeseries-date')[0].getValue();
		var todate = Ext.ComponentQuery.query('#protocol-report-todate-timeseries-date')[0].getValue();
		
		var todateTitle = todate
		var fromTitle = fromdate
		
		
		if(!todateTitle){
			todateTitle = 'now'	
		}else{
			todateTitle = todate.getFullYear()+ '/' + (todate.getMonth() + 1)  + '/' +  todate.getDate()
		}
		if(!fromTitle){
			fromTitle = 'begining'	
		}else{
			fromTitle = fromTitle.getFullYear()+ '/' + (fromTitle.getMonth() + 1)  + '/' +  fromTitle.getDate()
		}
	
		this.showTimeSeries(fromdate,todate, 'Protocol distribution for alerts generated from '+fromTitle+' till '+todateTitle)
	},
	
	onClicktimeseriesBackShow: function(){
		this.showTimeSeries()
	},
	
	init: function(application) {
		this.control({
			"#protocol-timeseries-report-charts-panel": {
				afterrender: this.onafterrenderForm
			},"#protocol-report-timeseries-charts-btn": {
				click: this.onClicktimeseriesIpShow
			},"#protocol-report-timeseriesBacktomain-charts-btn": {
				click: this.onClicktimeseriesBackShow
			}
		});
	}
});