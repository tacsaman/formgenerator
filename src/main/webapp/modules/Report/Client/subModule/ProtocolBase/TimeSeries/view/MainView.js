Ext.define('MyDesktop.Modules.Report.Client.subModule.ProtocolBase.TimeSeries.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Protocol Time Series',
    itemId: 'protocol-timeseries-report-charts-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
        var me = this;
        ProtocolTimeSeries = this;
        ProtocolTimeSeries.data = '';
        ProtocolTimeSeries.dataPie = '';
        ProtocolTimeSeries.piePanel = '';
        ProtocolTimeSeries.GridExamPanel = '';
        ProtocolTimeSeries.pieChartDesSus = '';
        ProtocolTimeSeries.pieChartDesSusdata = '';
        ProtocolTimeSeries.pieChartSourceSus = '';
        ProtocolTimeSeries.pieChartSourceSusdata = '';
        ProtocolTimeSeries.barChartSourceSus = '';
        ProtocolTimeSeries.barChartSourceSusdata = '';
        
        ProtocolTimeSeries.piePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            itemId: 'PanelHighechart',
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            items: [/*zonesForm.pieExample*/],
        });

        ProtocolTimeSeries.GridExamPanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            title: 'Required values',
            width: 200,
            layout: 'auto',
            region: 'east',
            collapsible: true,
            //collapsed: true,
            items: [{
                xtype: 'datefield',
                fieldLabel: 'From',
                name: 'from_date',
                width: 185,
                maxValue: new Date(),
                itemId: 'protocol-report-fromdate-timeseries-date'
            }, {
                xtype: 'datefield',
                fieldLabel: 'To',
                name: 'to_date',
                width: 185,
                maxValue: new Date(),
                itemId: 'protocol-report-todate-timeseries-date'
            }, {
                xtype: 'button',
                text : 'Apply',
                itemId: 'protocol-report-timeseries-charts-btn'
            }, {
                xtype: 'button',
                text : 'Return',
                itemId: 'protocol-report-timeseriesBacktomain-charts-btn'
            }],
        });

           Ext.applyIf(me,{
               items: [ProtocolTimeSeries.piePanel, ProtocolTimeSeries.GridExamPanel]
           });
           me.callParent(arguments);

    },


});