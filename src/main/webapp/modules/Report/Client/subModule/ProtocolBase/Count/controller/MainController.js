Ext.define('MyDesktop.Modules.Report.Client.subModule.ProtocolBase.Count.controller.MainController', {
	extend: 'Ext.app.Controller',

	
	showPieChartSourceSus: function(){
		merge = {
				"reportName": "ProtocolMashkook",
				"reportParams": []
		}
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading..."});
		myMask.show();
		
		me = this;
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			params: Ext.encode(merge),
			success: function(response) {
				 decoded = Ext.decode(response.responseText);
				 ProtocolBaseForm.dataPie =decoded.data;
				 ProtocolBaseForm.pieChartSourceSus = Ext.create('Chart.ux.Highcharts', {
						itemId:'protocolpiechart2',
			            
				        series: [{
				            name: 'Protocol Distribution',
				        	colorByPoint: true,
				        	data:ProtocolBaseForm.dataPie
				        }]
			           ,
				        chartConfig: {
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false,
			                    type: 'pie'
			                },

			                title: {
			                    text: 'Protocol Distribution'
			                },
			                tooltip: {
			                    pointFormat: 'Number of alerts: <b>{point.y}</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        dataLabels: {
			                            enabled: true,
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                            style: {
			                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                            }
			                        }
			                    }
			                }
			            }
				    });
					if(ProtocolBaseForm.pieSourcePanel.items.length == 1){
						ProtocolBaseForm.pieSourcePanel.items.items[0].hide();
						ProtocolBaseForm.pieSourcePanel.items.removeAt(0)
					}
						
					ProtocolBaseForm.pieSourcePanel.add(ProtocolBaseForm.pieChartSourceSus)
					myMask.hide();
			}
		});
		
	},
	
	onafterrenderForm: function(){
		this.showPieChartSourceSus()
	},
	
	onShowPiechart:function(){
		this.showPieChartSourceSus()
	},
	
	
	init: function(application) {
		
		this.control({
			"#protocol-count-report-charts1-panel": {
				afterrender: this.onafterrenderForm
			}
		});
	}
});
