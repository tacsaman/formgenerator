Ext.define('MyDesktop.Modules.Report.Client.subModule.ProtocolBase.Count.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window'
    ],
    title: 'Protocol Distribution',
    itemId: 'protocol-count-report-charts1-panel',
    layout: 'border',
    height: 800,

    initComponent: function() {
    	var me = this;
        ProtocolBaseForm = this;
        ProtocolBaseForm.BarChart = '';
        ProtocolBaseForm.pieChartSourceSus = '';
        ProtocolBaseForm.data = '';
        ProtocolBaseForm.dataPie = '';
        ProtocolBaseForm.popup = '';

        ProtocolBaseForm.pieSourcePanel = Ext.create('Ext.panel.Panel', {
            bodyPadding: 5, // Don't want content to crunch against the borders
            //width: 300,
            //height: 500,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            id:'reza10',
            style: {
                direction:'ltr !important'
            },
            items: [/*pieExample*/],
        });

        Ext.applyIf(me, {
            items: [ProtocolBaseForm.pieSourcePanel]
        });
        me.callParent(arguments);
    }

});