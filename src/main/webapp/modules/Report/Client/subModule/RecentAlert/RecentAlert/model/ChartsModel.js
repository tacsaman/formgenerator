Ext.define('MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.model.ChartsModel', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.Field'],
    fields: [{
		name: 'timestamp'
	},{
		name: 'srcip'
	}, {
		name: 'srccountry'
	}, {
		name: 'dstip'
	}, {
		name: 'dstcountry'
	}, {
		name: 'protocol'
	}, {
		name: 'priority'
	}, {
		name: 'message'
	}, {
		name: 'sid'
	}, {
		name: 'defaultMessage'
	}, {
		name: 'defaultAction'
	}]

});