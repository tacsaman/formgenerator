Ext.define('MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.store.ChartsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.model.ChartsModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
 		var me = this;
		cfg = cfg || {};
		merge = {
		"reportName": "RecentAlert",
		"reportParams": []
		}
		var jsondata = Ext.encode(["a","b"]) ;
		me.callParent([Ext.apply({
		    autoLoad: true,
		    model: 'MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.model.ChartsModel',
		    proxy: {
		    	type:'ajax',
				headers: { 'Content-Type': 'application/json' },
		         paramsAsJson:true,
		        actionMethods: {create: 'POST', read: 'POST', update:'POST', destroy: 'POST'},
		        method:'POST',
		        url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
		extraParams:{ "reportName": "RecentAlert","reportParams":[],"ipss":true},
		        reader: {
		            type: 'json',root: 'data',totalProperty: 'totalCount'
		        }
		    },
		listeners: {
		'beforeload' :  function(store,records,options) {
		
		}
		}
		}, cfg)]);
}
});
