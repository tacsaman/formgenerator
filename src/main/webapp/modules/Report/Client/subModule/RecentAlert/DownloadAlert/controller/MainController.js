Ext.define('MyDesktop.Modules.Report.Client.subModule.RecentAlert.DownloadAlert.controller.MainController', {
	extend: 'Ext.app.Controller',

	onafterrenderDesForm: function(){
		
	},
	run: function(){
//		var IPSource = Ext.ComponentQuery.query('#address-dest-ip-txt')[0].getValue();
		me = this;
//		Ext.util.Cookies.set("userId", "mostafa")
	    document.location.replace(Ext.ux.preURL+'form/ParsIPS-Event-Detatils.xls')
	},
	init: function(application) {
		this.control({
			"#address-destination-report-charts-panel": {
				afterrender: this.onafterrenderDesForm
			},"#download-alert-btn": {
				click: this.run
			}
		});
	}
});
