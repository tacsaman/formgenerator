Ext.define('MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Fit',
        'Ext.chart.*',
        'Ext.chart.axis.Gauge',
        'Ext.chart.series.*',
        'Ext.Window',
		'Ext.WindowManager',
        'Ext.data.*',
        'Ext.layout.container.Accordion'
    ],
    title: 'Recent Alerts',
    itemId: 'protocol-count-report-grid-panel',
    layout: 'border',
    height: 800,
    popUpWindowPacketDetail: function(Timestamp,SourceIP,DestinationIP,signatureNum){
		me = this;
		merge = {
				"reportName": "PacketInfo",
				"reportParams": [Timestamp,SourceIP,DestinationIP,signatureNum]
		}
		
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			
			//async: false,
			params: Ext.encode(merge),
			success: function(response) {
				decoded = Ext.decode(response.responseText);
				var error = '0';
				var signature = '';
				if(decoded.data.length == 0)
					 error = '1';
				 if(error == '0')
					 signature = decoded.data[0].signature;
				 if(signature == '')
					 error = '1';
				 
				 if(error == '0'){
					 if(decoded.data[0].tcp_csum >0){
						 var mypanel= Ext.create('Ext.panel.Panel', {
							    title: 'Event Discription',
							    width: 450,
							    height: 400,
						        plain: true,
						        floating: true,
						        closable : true,
						        autoScroll: true,
						        layout: 'fit',
						        rtl: false,
						        items: [
					                    {
					                    	layout: {
					                    	      type: 'accordion',      
					                    	      animate: true,
					                    	      multi: true,
											        
					                    	   },
					                                items: [{
					                                    hidden:true,        
					                                },{
					                                        title: '<b>Event Information </b>',
					                                        html: '<p><i>\nIndex : </i>'+ decoded.data[0].sid+'('+decoded.data[0].cid+')</p>'
					                                        +  '<p><i>\nSignature : </i>'+decoded.data[0].signature+'</p>'
					            						    +  '<p><i>\nTimestamp : </i>'+decoded.data[0].timestamp+'</p>'
					            						    +  '<p><i>\nSource IP : </i>'+decoded.data[0].ip_src+'</p>'
					            						    +  '<p><i>\nDestination IP : </i>'+decoded.data[0].ip_dst+'</p>',
					            						    collapsed: true,
					            						    autoScroll: true
					                                },
					                                {
					                                	 title: '<b>Internet Protocol</b>',
					                                        html: '<p><i>\nIP Version : </i>'+decoded.data[0].ip_ver+'</p>'
					            						    +  '<p><i>\nIP Header Length : </i>'+decoded.data[0].ip_hlen+'</p>'
					            						    +  '<p><i>\nIP TOS : </i>'+decoded.data[0].ip_tos+'</p>'
					            						    +  '<p><i>\nIP Length : </i>'+decoded.data[0].ip_len+'</p>'
					            						    +  '<p><i>\nIP ID : </i>'+decoded.data[0].ip_id+'</p>'
					            						    +  '<p><i>\nIP Flags : </i>'+decoded.data[0].ip_flags+'</p>'
					            						    +  '<p><i>\nIP Offset : </i>'+decoded.data[0].ip_off+'</p>'
					            						    +  '<p><i>\nIP TTL : </i>'+decoded.data[0].ip_ttl+'</p>'
					            						    +  '<p><i>\nIP Protocol : </i>'+decoded.data[0].ip_proto+'</p>'
					            						    +  '<p><i>\nIP Checksum : </i>'+decoded.data[0].ip_csum+'</p>',
					            						    collapsed: true,
					            						    autoScroll: true
					                                },
					                                {
					                                	 title: '<b>Transmission Control Protocol</b>',
					                                        html: '<p><i>\nTCP Source Port : </i>'+decoded.data[0].tcp_sport+'</p>'
							    						    +  '<p><i>\nTCP Destination Port : </i>'+decoded.data[0].tcp_dport+'</p>'
							    						    +  '<p><i>\nTCP Seq : </i>'+decoded.data[0].tcp_seq+'</p>'
							    						    +  '<p><i>\nTCP Ack : </i>'+decoded.data[0].tcp_ack+'</p>'
							    						    +  '<p><i>\nTCP Offset : </i>'+decoded.data[0].tcp_off+'</p>'
							    						    +  '<p><i>\nTCP Res : </i>'+decoded.data[0].tcp_res+'</p>'
							    						    +  '<p><i>\nTCP ID : </i>'+decoded.data[0].tcp_flags+'</p>'
							    						    +  '<p><i>\nTCP Window Size : </i>'+decoded.data[0].tcp_win+'</p>'
							    						    +  '<p><i>\nTCP Urp : </i>'+decoded.data[0].tcp_urp+'</p>'
							    						    +  '<p><i>\nTCP Checksum : </i>'+decoded.data[0].tcp_csum+'</p>',
							    						    collapsed: true,
							    						    autoScroll: true
					                                },
					                                {
					                                	
					                                	 title: '<b>Payload</b>',
					                                        html: '<div style="overflow-y: scroll;float:left;color: #666666; word-break: break-all;width:auto; font-family: Candara,Trebuchet MS,sans-serif; font-size: 12px; font-weight: bold; border-right: thin dotted #666666; line-height: 10px;"><p>'+decoded.data[0].data_payload+'</p></div>',
					                                        collapsed: true,
					                                        autoScroll: true
					                                }]
					                    }]
							}).show();
					 }
					 else if(decoded.data[0].udp_csum >0){
						 var mypanel= Ext.create('Ext.panel.Panel', {
							    title: 'Event Discription',
							    width: 450,
							    height: 400,
						        plain: true,
						        floating: true,
						        closable : true,
						        autoScroll: true,
						        layout: 'fit',
						        rtl: false,
						        items: [
					                    {
					                    	layout: {
					                    	      type: 'accordion',      
					                    	      animate: true,
					                    	      multi: true,
											        
					                    	   },
					                                items: [{
					                                    hidden:true,        
					                                },{
					                                        title: '<b>Event Information </b>',
					                                        html: '<p><i>\nIndex : </i>'+ decoded.data[0].sid+'('+decoded.data[0].cid+')</p>'
					                                        +  '<p><i>\nSignature : </i>'+decoded.data[0].signature+'</p>'
					            						    +  '<p><i>\nTimestamp : </i>'+decoded.data[0].timestamp+'</p>'
					            						    +  '<p><i>\nSource IP : </i>'+decoded.data[0].ip_src+'</p>'
					            						    +  '<p><i>\nDestination IP : </i>'+decoded.data[0].ip_dst+'</p>',
					            						    collapsed: true,
					            						    autoScroll: true
					                                },
					                                {
					                                	 title: '<b>Internet Protocol</b>',
					                                        html: '<p><i>\nIP Version : </i>'+decoded.data[0].ip_ver+'</p>'
					            						    +  '<p><i>\nIP Header Length : </i>'+decoded.data[0].ip_hlen+'</p>'
					            						    +  '<p><i>\nIP TOS : </i>'+decoded.data[0].ip_tos+'</p>'
					            						    +  '<p><i>\nIP Length : </i>'+decoded.data[0].ip_len+'</p>'
					            						    +  '<p><i>\nIP ID : </i>'+decoded.data[0].ip_id+'</p>'
					            						    +  '<p><i>\nIP Flags : </i>'+decoded.data[0].ip_flags+'</p>'
					            						    +  '<p><i>\nIP Offset : </i>'+decoded.data[0].ip_off+'</p>'
					            						    +  '<p><i>\nIP TTL : </i>'+decoded.data[0].ip_ttl+'</p>'
					            						    +  '<p><i>\nIP Protocol : </i>'+decoded.data[0].ip_proto+'</p>'
					            						    +  '<p><i>\nIP Checksum : </i>'+decoded.data[0].ip_csum+'</p>',
					            						    collapsed: true,
					            						    autoScroll: true
					                                },
					                                {
					                                	 title: '<b>User Datagram Protocol</b>',
					                                        html: '<p><i>\nUDP Source Port : </i>'+decoded.data[0].udp_sport+'</p>'
					        							    +  '<p><i>\nUDP Destination Port : </i>'+decoded.data[0].udp_dport+'</p>'
					        							    +  '<p><i>\nUDP Seq : </b>'+decoded.data[0].udp_seq+'</p>'
					        							    +  '<p><i>\nUDP Checksum : </b>'+decoded.data[0].udp_csum+'</p>',
					        							    collapsed: true,
					        							    autoScroll: true
					                                },
					                                {
					                                	 title: '<b>Payload</b>',
					                                	 html: '<div style="overflow-y: scroll;float:left;color: #666666; word-break: break-all;width:auto; font-family: Candara,Trebuchet MS,sans-serif; font-size: 12px; font-weight: bold; border-right: thin dotted #666666; line-height: 10px;"><p>'+decoded.data[0].data_payload+'</p></div>',
					                                       collapsed: true,
					                                        autoScroll: true
					                                }]
					                    }]
							}).show();
					 
						 
						 }else if(decoded.data[0].icmp_csum >0){

							 var mypanel= Ext.create('Ext.panel.Panel', {
								    title: 'Event Discription',
								    width: 450,
								    height: 400,
							        plain: true,
							        floating: true,
							        closable : true,
							        autoScroll: true,
							        layout: 'fit',
							        rtl: false,
							        items: [
						                    {
						                    	layout: {
						                    	      type: 'accordion',      
						                    	      animate: true,
						                    	      multi: true,
												        
						                    	   },
						                                items: [{
						                                    hidden:true,        
						                                },{
						                                        title: '<b>Event Information </b>',
						                                        html: '<p><i>\nIndex : </i>'+ decoded.data[0].sid+'('+decoded.data[0].cid+')</p>'
						                                        +  '<p><i>\nSignature : </i>'+decoded.data[0].signature+'</p>'
						            						    +  '<p><i>\nTimestamp : </i>'+decoded.data[0].timestamp+'</p>'
						            						    +  '<p><i>\nSource IP : </i>'+decoded.data[0].ip_src+'</p>'
						            						    +  '<p><i>\nDestination IP : </i>'+decoded.data[0].ip_dst+'</p>',
						            						    collapsed: true,
						            						    autoScroll: true
						                                },
						                                {
						                                	 title: '<b>Internet Protocol</b>',
						                                        html: '<p><i>\nIP Version : </i>'+decoded.data[0].ip_ver+'</p>'
						            						    +  '<p><i>\nIP Header Length : </i>'+decoded.data[0].ip_hlen+'</p>'
						            						    +  '<p><i>\nIP TOS : </i>'+decoded.data[0].ip_tos+'</p>'
						            						    +  '<p><i>\nIP Length : </i>'+decoded.data[0].ip_len+'</p>'
						            						    +  '<p><i>\nIP ID : </i>'+decoded.data[0].ip_id+'</p>'
						            						    +  '<p><i>\nIP Flags : </i>'+decoded.data[0].ip_flags+'</p>'
						            						    +  '<p><i>\nIP Offset : </i>'+decoded.data[0].ip_off+'</p>'
						            						    +  '<p><i>\nIP TTL : </i>'+decoded.data[0].ip_ttl+'</p>'
						            						    +  '<p><i>\nIP Protocol : </i>'+decoded.data[0].ip_proto+'</p>'
						            						    +  '<p><i>\nIP Checksum : </i>'+decoded.data[0].ip_csum+'</p>',
						            						    collapsed: true,
						            						    autoScroll: true
						                                },
						                                {
						                                	 title: '<b>Internet Control Message Protocol</b>',
						                                        html: '<p><i>\nICMP Type : </i>'+decoded.data[0].icmp_type+'</p>'
						        							    +  '<p><i>\nICMP Code : </i>'+decoded.data[0].icmp_code+'</p>'
						        							    +  '<p><i>\nICMP ID : </i>'+decoded.data[0].icmp_id+'</p>'
						        							    +  '<p><i>\nICMP Seq : </i>'+decoded.data[0].icmp_seq+'</p>'
						        							    +  '<p><i>\nICMP Checksum : </i>'+decoded.data[0].icmp_csum+'</p>',
						        							    collapsed: true,
						        							    autoScroll: true
						                                },
						                                {
						                                	title: '<b>Payload</b>',
						                                	 html: '<div style="overflow-y: scroll;float:left;color: #666666; word-break: break-all;width:auto; font-family: Candara,Trebuchet MS,sans-serif; font-size: 12px; font-weight: bold; border-right: thin dotted #666666; line-height: 10px;"><p>'+decoded.data[0].data_payload+'</p></div>',
						                                       collapsed: true,
						                                        autoScroll: true
						                                }]
						                    }]
								}).show();
						 }
				 }
				 if(error == '1'){
					var errorPanel= Ext.create('Ext.panel.Panel', {
					    title: 'Signature Discription',
					    width: 450,
					    height: 400,
				        plain: true,
				        floating: true,
				        closable : true,
				        autoScroll: true,
					 html: '<div dir=ltr align="justify"><p align="center"><b>Signature Number    '+ decoded.data[0].signature+' </b>'
					    +  '<p><b>Message : </b>'+decoded.data[0].DefaultMessage+'</p>'
					    +  '<p><b>ParsIPS Default Config : </b> <br>Status is '+decoded.data[0].DefaultStatus+'.<br>Action is '+decoded.data[0].DefaultAction+'.<br>Log is '+decoded.data[0].DefaultLog+'</p>',
					    renderTo: document.body
					}).show();
				 }
			}
		});
		
	},
    popUpWindow: function(signatureNum){
		me = this;
	    //chart = Ext.ComponentQuery.query('#morteza')[0];
		merge = {
				"reportName": "Signature",
				"reportParams": [signatureNum]
		}
		
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			
			//async: false,
			params: Ext.encode(merge),
			success: function(response) {
				decoded = Ext.decode(response.responseText);
				
				var error = '0';
				var signature = '';
				if(decoded.data.length == 0)
					 error = '1';
				 if(error == '0')
					 signature = decoded.data[0].signature;
				 if(signature == '')
					 error = '1';
				 if(error == '0'){
					var mypanel= Ext.create('Ext.panel.Panel', {
					    title: 'Signature Discription',
					    width: 450,
					    height: 400,
				        plain: true,
				        floating: true,
				        closable : true,
				        autoScroll: true,
				       
					  html: '<div dir=ltr align="justify"><p align="center"><b>Signature Number    '+ decoded.data[0].signature+' </b>'
					    +  '<p><b>Message : </b>'+decoded.data[0].DefaultMessage+'</p>'
					    +  '<p><b>ParsIPS Default Config : </b> <br>Status is '+decoded.data[0].DefaultStatus+'.<br>Action is '+decoded.data[0].DefaultAction+'.<br>Log is '+decoded.data[0].DefaultLog+'</p>'
					    +  '<p><b>Impact : </b>'+decoded.data[0].impact+'</p>'
					    +  '<p><b>\nDetail Information : </b>'+decoded.data[0].info+'</p>'
					    +  '<p><b>\nAffected Systems : </b>'+decoded.data[0].affected+'</p>'
					    +  '<p><b>\nAttack Scenario : </b>'+decoded.data[0].attack+'</p>'
					    +  '<p><b>\nEase of Attack : </b>'+decoded.data[0].easeofattack+'</p>'
					    +  '<p><b>\nFalse Positive : </b>'+decoded.data[0].falsepositive+'</p>'
					    +  '<p><b>\nFalse Negative : </b>'+decoded.data[0].falsenegative+'</p>'
					    +  '<p><b>\nCorrective Action : </b>'+decoded.data[0].correctiveaction+'</p>'
					    +  '<p><b>\nContributor : </b>'+decoded.data[0].contribute+'</p>'
					    +  '<p><b>\nReferense : </b>'+decoded.data[0].referense+'</p><div>',
					    renderTo: document.body
					}).show();
				 }
				 if(error == '1'){
					var errorPanel= Ext.create('Ext.panel.Panel', {
					    title: 'Signature Discription',
					    width: 450,
					    height: 400,
				        plain: true,
				        floating: true,
				        closable : true,
				        autoScroll: true,
					 html: '<div dir=ltr align="justify"><p align="center"><b>Signature Number    '+ decoded.data[0].signature+' </b>'
					    +  '<p><b>Message : </b>'+decoded.data[0].DefaultMessage+'</p>'
					    +  '<p><b>ParsIPS Default Config : </b> <br>Status is '+decoded.data[0].DefaultStatus+'.<br>Action is '+decoded.data[0].DefaultAction+'.<br>Log is '+decoded.data[0].DefaultLog+'</p>',
					    renderTo: document.body
					}).show();
				 }
			}
		});
		
	},
	
    initComponent: function() {
        var me = this;
        GridForm = this;
        GridForm.BarChart = '';
        GridForm.grid = '';
        GridForm.data = '';
        GridForm.dataPie = '';
        GridForm.popup = '';
        GridForm.pieSourcePanel = '';

        var CartablesStore = Ext.create('MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.store.ChartsStore');        
        GridForm.pieSourcePanel = Ext.create('Ext.panel.Panel', {
        	bodyPadding: 5, 
            layout: 'fit',
            region: 'center',
            autoScroll: true, 
            rtl: false,
            id: 'reza111',
            style: {
                direction: 'ltr !important'
            },
            columnLines: true,
            renderTo: Ext.getBody(),
          
           items: [{
                //id: 'Document-Documents-grid',
                xtype: 'customgrid',
                hidePaging: false,
                model: 'MyDesktop.Modules.Report.Client.subModule.RecentAlert.RecentAlert.model.ChartsModel',
                viewConfig: { 
                    stripeRows: false, 
                    getRowClass: function(record) { 
                       if(record.get('priority') == 'High') 
                        	return 'high-row';
                       else if(record.get('priority') == 'Medium') 
                           	return 'medium-row';
                       else if(record.get('priority') == 'Low') 
                             return 'low-row';
                       return 'unknown-row';
                    } 
                },
                forcefit: true,
                itemId: 'Document-Documents-grid',
                store:CartablesStore, 
                hidesearchbar: true,
                hideNewBtn: true,
                hideRefreshBtn: false,
                editable: false,
                showRowonNew: false,
                listeners: {
                	itemclick: function(dv, record, item, index, e) {
                			e.stopEvent();
                		  var menu = new Ext.menu.Menu({
                			   margin: '0 0 10 0',
                			  items: [{
                				  icon: 'signature-detail-cell',
                				  cls: 'signature-detail-cell',
                	              text: 'Show Signature Details',
                	              handler: function() {
                	            	  me.popUpWindow(record.get('sid'));
                	              }
                	          }, {
                				  icon: 'packet-detail-cell',
                				  cls: 'packet-detail-cell',
                	              text: 'Show Packet Details',
                	              handler: function() {
                	            	 me.popUpWindowPacketDetail(record.get('timestamp'),record.get('srcip'),record.get('dstip'),record.get('sid'));
                	              }
                	          }]
                	      }).showAt(e.getXY());                                     
                	}},
                columns: [{
                    xtype: 'gridcolumn',
                    dataIndex: 'priority',
                    flex: 0,
                    width: 25,
                     renderer: function(value, meta) { 
                        if (value === 'High') { 
                            meta.tdCls = 'high-cell';
                        }
                        else if (value === 'Medium') { 
                            meta.tdCls = 'mid-cell';
                       }
                        else if (value === 'Low') { 
                            meta.tdCls = 'low-cell';
                        }
                        else{
                        	meta.tdCls = 'unknown-cell';
                        }
                    }
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'timestamp',
                    flex: 1.3,
                    text: 'Time',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'priority',
                    flex: 0,
                    text: 'Severity',
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'srcip',
                    flex: 1,
                    text: 'Source Address',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'srccountry',
                    flex: 2,
                    text: 'From',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'dstip',
                    flex: 1,
                    text: 'Destination Address',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'dstcountry',
                    flex: 2,
                    text: 'To',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'protocol',
                    flex: 0,
                    text: 'Protocol',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'sid',
                    flex: 0,
                    text: 'Signature ID',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'defaultMessage',
                    flex: 0,
                    text: 'Message',
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'defaultAction',
                    flex: 0,
                    text: 'Action',
                }]
            }]
        });
        Ext.applyIf(me, {
            items: [GridForm.pieSourcePanel]
        });
        me.callParent(arguments);
    }

});
