Ext.define('MyDesktop.Modules.Report.Client.Report', {
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'report-win',
    init : function(){
        this.launcher = {
            text: 'Reports',
            iconCls:'icon-reports',
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('report-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'report-win',									
                title:'Reports',								
                width:1500,										
                height:650,											
                iconCls: 'icon-report',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                rtl: false,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Report/Client/tree-data.json' , prefix: 'Report'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
