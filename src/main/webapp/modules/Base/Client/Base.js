Ext.define('MyDesktop.Modules.Base.Client.Base', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'base-win',
    init : function(){
        this.launcher = {
            text: 'اطلاعات پایه',								
            iconCls:'icon-base',								
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('base-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'base-win',									
                title:'اطلاعات پایه',								
                width:1000,										
                height:550,											
                iconCls: 'icon-base',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Base/Client/tree-data.json' , prefix: 'Base'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
