Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.view.TypeAddForm', {
	extend: 'Ext.window.Window',
	width: 830,
	title: 'اضافه کردن نوع مستند',
	itemId:'base-Types-TypesAdd-win',
	modal: true,
	initComponent: function(arguments) {
		var me = this;
		TypeAddForm = this;
		TypeAddForm.formData = this.selectedRecord;
		TypeAddForm.configType = this.configType;
		TypeAddForm.saveFlag = this.saveFlag;
		TypeAddForm.memberId = this.memberId;
		var PartsStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.store.PartsStore');
		Ext.applyIf(me, {
			items: [{
				xtype: 'customform',
				layout: 'auto',
				autoLoad: false,
				autoScroll: true,
				layout: 'auto',
				bodyPadding: 10,
				title: '',
				itemId: 'base-Types-TypesAdd-form',
				submitUrl: 'cxf/rest/doctype/save',
				//saveCallback: TypeAddForm.callback,
				saveItemId: 'base-Types-TypeAdd-save-btn',
				hideResetBtn: true,
				defaultSave: false,
				defaultReset: true,
				items: [{
					xtype: 'textfield',
					itemId: 'base-Types-TypeAdd-typeName-txt',
					width: 280,
					fieldLabel: 'نام',
					name: 'name'
				},{
					xtype: 'textfield',
					itemId: 'base-Types-TypeAdd-docPrefix-txt',
					width: 280,
					fieldLabel: 'پیشوند',
					name: 'docPrefix'
				}, {
					xtype: 'fieldset',
					itemId: 'base-Types-TypesAdd-gridF',
					collapsible: false,
					title: 'ترتیب نامگذاری',
					width: 650,
					height: 200,
					layout: 'fit',
					items: [{
						xtype: 'customgrid',
						itemId: 'base-Types-TypesAdd-grid',
						forceFit: true,
						hidesearchbar: true,
						hideNewBtn:true,
						editable: false,
						hidePaging: true,
						hideRefreshBtn: true,
						showRowonNew: false,
						viewConfig: {
							itemId: 'base-Types-TypesAdd-dragdrop',
							plugins: {
								ptype: 'gridviewdragdrop',
								ddGroup: 'mygrid-dd'
							}
						},
						store: PartsStore,
						model: 'MyDesktop.Modules.Base.Client.subModule.Types.model.PartsModel',
						enableDragDrop: true,
						ddGroup: 'mygrid-dd',
						ddText: 'Place this row.',
						columns: [{
								xtype: 'gridcolumn',
								dataIndex: 'name',
								flex: 1,
						}]
					}]
				}]
			}]
		});

		me.callParent(arguments);
	}

});
