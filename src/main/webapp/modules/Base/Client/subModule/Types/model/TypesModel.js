Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.model.TypesModel', {
	extend: 'Ext.data.Model',
	requires: [
		'Ext.data.Field'
	],		
	fields: [{
			name: 'id'
		}, {
			name: 'name'
		}, {
			name: 'docPrefix'
		}

	]
});
