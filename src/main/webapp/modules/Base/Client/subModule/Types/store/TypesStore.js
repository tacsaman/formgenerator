Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.store.TypesStore', {
	extend: 'Ext.data.Store',
	requires: [
		'MyDesktop.Modules.Base.Client.subModule.Types.model.TypesModel',
		'Ext.data.proxy.Ajax',
		'Ext.data.reader.Json'
	],
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			autoLoad: true,
			model: 'MyDesktop.Modules.Base.Client.subModule.Types.model.TypesModel',
			storeId: 'RegionStore',
			proxy: {
				type: 'ajax',
				method: 'GET',
				url: 'cxf/rest/doctype/getAll',
				reader: {
					type: 'json',
					root: 'data'
				}
			}
		}, cfg)]);
	}
});
