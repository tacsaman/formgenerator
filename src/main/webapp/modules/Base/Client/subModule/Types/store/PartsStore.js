Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.store.PartsStore', {
	extend: 'Ext.data.Store',
	requires: [
		'MyDesktop.Modules.Base.Client.subModule.Types.model.PartsModel',
		'Ext.data.proxy.Ajax',
		'Ext.data.reader.Json'
	],
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			autoLoad: false,
			model: 'MyDesktop.Modules.Base.Client.subModule.Types.model.PartsModel',
			storeId: 'RegionStore',
			proxy: {
				type: 'ajax',
				method: 'GET',
				url: 'cxf/rest/part/getAll',
				reader: {
					type: 'json',
					root: 'data'
				}
			}
		}, cfg)]);
	}
});
