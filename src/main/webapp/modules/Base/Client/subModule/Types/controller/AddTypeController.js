Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.controller.AddTypeController', {
	extend: 'Ext.app.Controller',
	onTypesSaveBtnClick: function() {},
    callback: function() {
        var Grid = Ext.ComponentQuery.query('#base-types-grid')[0];
        var gridStore = Grid.getStore();
        gridStore.reload();
		TypeAddForm.close();
    },
	onTypesNewBtnClick: function() {
		var TypeAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.view.TypeAddForm', {
			configType: '0',
			selectedRecord: '',
			saveFlag: '0'
		});
		TypeAddForm.show();
	},
	onSave: function() {
		var form = Ext.ComponentQuery.query('#base-Types-TypesAdd-form')[0];
		var url="cxf/rest/doctype/update";
		var parts = [];
		var grid = Ext.ComponentQuery.query('#base-Types-TypesAdd-grid')[0];
		var sm = grid.getSelectionModel().getStore().data.items;
				for (i = 0; i < sm.length; i++)
				parts.push(sm[i].data)		
		var merge = Ext.apply(form.getValues());

		if (TypeAddForm.configType == "0") {
			url="cxf/rest/doctype/save";
			delete merge['id'];
		}
		else merge.id=TypeAddForm.formData.data.id;
		var data = Ext.apply(merge,{"parts":parts});
		var util = Ext.create('MyDesktop.Util.util', {
			callBack: this.callback,
			url: url,
			data: data
		});
		util.sendRequest();
	},
	loadForm: function() {
		var form = Ext.ComponentQuery.query('#base-Types-TypesAdd-form')[0];
		var grid = Ext.ComponentQuery.query('#base-Types-TypesAdd-grid')[0].getStore();
		thisthis = this;
		if (TypeAddForm.configType == "1") {
			form.load({
				params: {
					id: TypeAddForm.formData.data.id
				},
				url: 'cxf/rest/doctype/getById',
				method: "GET"
			})
			grid.load({url:'cxf/rest/doctype/getDocTypeCodeParts',params: {id: TypeAddForm.formData.data.id}});
		}
		else{
			grid.load();			
		}
		form.on({
			actioncomplete: function(form, action) {
				if (action.type == 'load') {
					var contact = action.result.data;
					Ext.ComponentQuery.query('#base-Types-TypesAdd-form')[0].submitUrl = 'cxf/rest/doctype/update';
					Ext.ComponentQuery.query('#base-Types-TypeAdd-typeName-txt')[0].setReadOnly(true);
				}
			}
		})
	},
	init: function(application) {

		this.control({
			"#base-Types-TypeAdd-save-btn": {
				click: this.onTypesNewBtnClick
			},
			"#base-Types-TypesAdd-form": {
				afterrender: this.loadForm
			},
			"#base-Types-TypeAdd-save-btn-submit":{
				click: this.onSave
			}

		});
	}


});
