Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.controller.MainController', {
    extend: 'Ext.app.Controller',
    onTypesNewBtnClick: function() {
        var TypeAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.view.TypeAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        TypeAddForm.show();
    },
    onTypesCustomSelectionAction: function(view, rowIndex, colIndex, item, e, record, row) {
		var formToShow = Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.view.TypeAddForm', {
			configType: '1',
			selectedRecord: record,
			saveFlag: '1',
			callback: this.callback,
		});
        formToShow.show();
    },
    init: function(application) {
        this.control({
            "#base-types-grid-addBtn": {
                click: this.onTypesNewBtnClick
            },
			"#base-types-grid-customactioncolumn": {
				customSelectionAction: this.onTypesCustomSelectionAction
            }

        });
    }
});
