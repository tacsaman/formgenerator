Ext.define('MyDesktop.Modules.Base.Client.subModule.Types.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller=Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.controller.MainController');
        controller.init();
	var AddTypeController=Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.controller.AddTypeController');
        AddTypeController.init();

    }

});
