Ext.define('MyDesktop.Modules.Base.Client.subModule.Province.store.ProvinceStore', {
	extend: 'Ext.data.Store',
	requires: [
		'MyDesktop.Modules.Base.Client.subModule.Province.model.ProvinceModel',
		'Ext.data.proxy.Ajax',
		'Ext.data.reader.Json'
	],
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			autoLoad: true,
			model: 'MyDesktop.Modules.Base.Client.subModule.Province.model.ProvinceModel',
			storeId: 'ProvinceStore',
			proxy: {
				type: 'ajax',
				method: 'GET',
				url: 'cxf/rest/state/getAll',
				reader: {
					type: 'json',
					root: 'data'
				}
			}
		}, cfg)]);
	}
});
