Ext.define('MyDesktop.Modules.Base.Client.subModule.Province.controller.MainController', {
    extend: 'Ext.app.Controller',

    callback: function(){
        Ext.ComponentQuery.query('#base-province-grid')[0].getStore().reload();
    },
    onEditProvinceGrid: function() {
        var provinceGrid = Ext.ComponentQuery.query('#base-province-grid')[0];
        var util = Ext.create('MyDesktop.Util.util');
        var ProvinceName = Ext.ComponentQuery.query('#base-province-NameProvince-txt')[0].getValue();
        var id = util.returnId(provinceGrid.getStore(), ProvinceName);
        var merge = {};
        var url = 'cxf/rest/state/update'
        if (id) {
            merge = Ext.apply(merge, {
                name: ProvinceName,
                id: id
            });
        } else {
            url = 'cxf/rest/state/save';
            merge = Ext.apply(merge, {
                name: ProvinceName
            });
        }

        var util = Ext.create('MyDesktop.Util.util', {
            callBack: this.callback,
            url: url,
            data: merge
        });
        util.sendRequest();

    },
    init: function(application) {
        this.control({
            "#base-province-grid": {
                edit: this.onEditProvinceGrid
            }
        });
    }
});