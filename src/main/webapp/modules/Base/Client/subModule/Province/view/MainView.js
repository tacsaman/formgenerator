Ext.define('MyDesktop.Modules.Base.Client.subModule.Province.view.MainView', {
	extend: 'Ext.container.Container',
	requires: [
		'Ext.ux.customActionCol'
	],
	title: 'استان',
	itemId: 'base-province-panel',
	layout: 'fit',
	initComponent: function() {
		var me = this;
		ProvinceForm = this;
		ProvinceForm.connection = this.connection;
		var ProvinceStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Province.store.ProvinceStore');
		Ext.applyIf(me, {
			items: [{
				xtype: 'customgrid',
				dock: 'top',
				title: '',
				defaultEdit: false,
				forceFit: true,
				cancelObject: 'name',
				itemId: 'base-province-grid',
				newBtnItemId: 'base-province-grid-addBtn',
				newBtnText: 'اضافه کردن استان',
				newBtnTip: 'اضافه کردن استان',
				hidesearchbar: true,
				hidePaging: true,
				hideRefreshBtn: false,
				editable: true,
				showRowonNew: true,
				store: ProvinceStore,
				model: 'MyDesktop.Modules.Base.Client.subModule.Province.model.ProvinceModel',
				editObject:{},
				editkeyArray: [
					['name', 'name']
				],
				//addURL: 'cxf/rest/state/save',
				//editURL: 'cxf/rest/state/update',
				columns: [{
					xtype: 'gridcolumn',
					dataIndex: 'name',
					text: 'نام',
					flex: 1,
					editor: {
						xtype: 'textfield',
						name: 'name',
						allowBlank: false,
						itemId: 'base-province-NameProvince-txt'
					}
				}, {
					xtype: 'customactioncol',
					flex: 1,
					itemId: 'base-province-grid-customactioncolumn',
					deleteurl: 'cxf/rest/state/delete',
					deletetip: 'حذف',
					alwaysShowDelete: true,
					text: 'عملیات',
					deleteobject: {},
					deletekeyArray: [
						['id', 'id'],
						['name', 'name']
					],
					deletable: true,
					editable: true,
					defaultCustomEdit: true,
				}]
			}]
		});
		me.callParent(arguments);
	}

});