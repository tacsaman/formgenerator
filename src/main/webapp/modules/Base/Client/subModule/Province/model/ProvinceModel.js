Ext.define('MyDesktop.Modules.Base.Client.subModule.Province.model.ProvinceModel', {
	extend: 'Ext.data.Model',
	requires: [
		'Ext.data.Field'
	],
	fields: [{
		name: 'id'
	}, {
		name: 'name'
	}]
});