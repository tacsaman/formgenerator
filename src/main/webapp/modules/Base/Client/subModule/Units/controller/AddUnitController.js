Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.controller.AddUnitController', {
	extend: 'Ext.app.Controller',
	onUnitsSaveBtnClick: function() {},
    callback: function() {
        var Grid = Ext.ComponentQuery.query('#base-units-grid')[0];
        var gridStore = Grid.getStore();
        gridStore.reload();
		UnitAddForm.close();
    },
	onUnitsNewBtnClick: function() {
		var UnitAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.Units.view.UnitAddForm', {
			configType: '0',
			selectedRecord: '',
			saveFlag: '0'
		});
		UnitAddForm.show();
	},
	onSave: function() {
		var form = Ext.ComponentQuery.query('#base-Units-UnitAdd-form')[0];
		var url="cxf/rest/doctype/update";
		var merge = Ext.apply(form.getValues());

		if (UnitAddForm.configType == "0") {
			url="cxf/rest/doctype/save";
			delete merge['id'];
		}
		else merge.id=UnitAddForm.formData.data.id;
		var data = Ext.apply(merge);
		var util = Ext.create('MyDesktop.Util.util', {
			callBack: this.callback,
			url: url,
			data: data
		});
		util.sendRequest();
	},
	loadForm: function() {
		var form = Ext.ComponentQuery.query('#base-Units-UnitAdd-form')[0];
		thisthis = this;
		if (UnitAddForm.configType == "1") {
			form.load({
				params: {
					id: UnitAddForm.formData.data.id
				},
				url: 'cxf/rest/doctype/getById',
				method: "GET"
			})
		}
		form.on({
			actioncomplete: function(form, action) {
				if (action.type == 'load') {
					var contact = action.result.data;
					Ext.ComponentQuery.query('#base-Units-UnitAdd-form')[0].submitUrl = 'cxf/rest/doctype/update';
					Ext.ComponentQuery.query('#base-Units-UnitAdd-typeName-txt')[0].setReadOnly(true);
				}
			}
		})
	},
	init: function(application) {

		this.control({
			"#base-Units-UnitAdd-save-btn": {
				click: this.onUnitsNewBtnClick
			},
			"#base-Units-UnitAdd-form": {
				afterrender: this.loadForm
			},
			"#base-Units-UnitAdd-save-btn-submit":{
				click: this.onSave
			}

		});
	}


});
