Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.model.UnitsModel', {
	extend: 'Ext.data.Model',
	requires: [
		'Ext.data.Field'
	],		
	fields: [{
			name: 'id'
		}, {
			name: 'name'
		}, {
			name: 'docPrefix'
		}

	]
});
