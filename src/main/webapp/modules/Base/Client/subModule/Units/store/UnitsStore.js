Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.store.UnitsStore', {
	extend: 'Ext.data.Store',
	requires: [
		'MyDesktop.Modules.Base.Client.subModule.Units.model.UnitsModel',
		'Ext.data.proxy.Ajax',
		'Ext.data.reader.Json'
	],
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			autoLoad: true,
			model: 'MyDesktop.Modules.Base.Client.subModule.Units.model.UnitsModel',
			storeId: 'RegionStore',
			proxy: {
				type: 'ajax',
				method: 'GET',
				url: 'cxf/rest/doctype/getAll',
				reader: {
					type: 'json',
					root: 'data'
				}
			}
		}, cfg)]);
	}
});
