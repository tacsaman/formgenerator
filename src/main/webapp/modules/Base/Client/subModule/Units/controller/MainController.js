Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.controller.MainController', {
    extend: 'Ext.app.Controller',
    onUnitsNewBtnClick: function() {
        var UnitAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.Units.view.UnitAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        UnitAddForm.show();
    },
    onUnitsCustomSelectionAction: function(view, rowIndex, colIndex, item, e, record, row) {
		var formToShow = Ext.create('MyDesktop.Modules.Base.Client.subModule.Units.view.UnitAddForm', {
			configType: '1',
			selectedRecord: record,
			saveFlag: '1',
			callback: this.callback,
		});
        formToShow.show();
    },
    init: function(application) {
        this.control({
            "#base-units-grid-addBtn": {
                click: this.onUnitsNewBtnClick
            },
			"#base-units-grid-customactioncolumn": {
				customSelectionAction: this.onUnitsCustomSelectionAction
            }

        });
    }
});
