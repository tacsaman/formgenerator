Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'بخش',
	 itemId: 'base-units-panel',
    layout: 'fit',
	initComponent: function() {
 	var me = this;
	zonesForm = this;
	zonesForm.connection = this.connection;
	var UnitsStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Units.store.UnitsStore');
	Ext.applyIf(me, {
            items: [{
                xtype: 'customgrid',
                dock: 'top',
                title: '',
                forcefit: true,
		cancelObject:'name',
                itemId: 'base-units-grid',
                newBtnItemId: 'base-units-grid-addBtn',
                newBtnText: 'اضافه کردن بخش',
                newBtnTip: 'اضافه کردن بخش',
		hidesearchbar: true,
		hidePaging: true,
                hideRefreshBtn: false,
				editable: false,
				showRowonNew: false,
				disableEditRow: false,
                store: UnitsStore,
                model: 'MyDesktop.Modules.Base.Client.subModule.Units.model.UnitsModel',
		addURL: 'cxf/rest/doctype/save',
		editURL: 'cxf/rest/doctype/update',	
                columns: [{
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'کد',
                    flex: 1,
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'name',
                    text: 'نام',
                    flex: 1,
				},{
                    xtype: 'gridcolumn',
                    dataIndex: 'docPrefix',
                    text: 'پیشوند',
                    flex: 1,
				}, {
					xtype: 'customactioncol',
					flex: 1,
					itemId: 'base-units-grid-customactioncolumn',
					deleteurl: 'cxf/rest/doctype/delete',
					deletetip: 'حذف',
					alwaysShowDelete: true,
					text: 'عملیات',
					deleteobject: {},
					deletekeyArray: [
						[
							'id', 'id'
						]
					],
					deletable: true,
					editable: true,
					defaultCustomEdit: false,
					items: [{
						handler: function(view, rowIndex, colIndex, item, e, record, row) {
							this.fireEvent('customSelectionAction', view, rowIndex, colIndex, item, e, record, row);
						},
						icon: 'resources/images/default/component/user_edit.gif',
						tooltip: 'ویرایش'
					}]
                }]
            }]
        });
        me.callParent(arguments);
    }

});
