Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.view.UnitAddForm', {
	extend: 'Ext.window.Window',
	width: 830,
	title: 'اضافه کردن بخش',
	itemId:'base-Units-UnitAdd-win',
	modal: true,
	initComponent: function(arguments) {
		var me = this;
		UnitAddForm = this;
		UnitAddForm.formData = this.selectedRecord;
		UnitAddForm.configType = this.configType;
		UnitAddForm.saveFlag = this.saveFlag;
		UnitAddForm.memberId = this.memberId;
		Ext.applyIf(me, {
			items: [{
				xtype: 'customform',
				layout: 'auto',
				autoLoad: false,
				autoScroll: true,
				layout: 'auto',
				bodyPadding: 10,
				title: '',
				itemId: 'base-Units-UnitAdd-form',
				submitUrl: 'cxf/rest/doctype/save',
				//saveCallback: UnitAddForm.callback,
				saveItemId: 'base-Units-UnitAdd-save-btn',
				hideResetBtn: true,
				defaultSave: false,
				defaultReset: true,
				items: [{
					xtype: 'textfield',
					itemId: 'base-Units-UnitAdd-typeName-txt',
					width: 280,
					fieldLabel: 'نام',
					name: 'name'
				},{
					xtype: 'textfield',
					itemId: 'base-Units-UnitAdd-docPrefix-txt',
					width: 280,
					fieldLabel: 'پیشوند',
					name: 'docPrefix'
				}]
			}]
		});

		me.callParent(arguments);
	}

});
