Ext.define('MyDesktop.Modules.Base.Client.subModule.Units.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller=Ext.create('MyDesktop.Modules.Base.Client.subModule.Units.controller.MainController');
        controller.init();
	var UnitAddController=Ext.create('MyDesktop.Modules.Base.Client.subModule.Units.controller.AddUnitController');
        UnitAddController.init();

    }

});
