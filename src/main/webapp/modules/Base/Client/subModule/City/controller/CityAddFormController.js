Ext.define('MyDesktop.Modules.Base.Client.subModule.City.controller.CityAddFormController', {
    extend: 'Ext.app.Controller',
    callback: function(){
        Ext.ComponentQuery.query('#base-city-grid')[0].getStore().reload();
    },
    onSaveCity: function() {
        var util = Ext.create('MyDesktop.Util.util');
        var province = Ext.ComponentQuery.query('#base-city-CityAdd-ProvinceName-cmb')[0].getValue();
        var city = Ext.ComponentQuery.query('#base-city-CityAdd-CityName-txt')[0].getValue();
        var id =  util.returnId(Ext.ComponentQuery.query('#base-city-CityAdd-ProvinceName-cmb')[0].getStore(), province);
        var CityAddForm = Ext.ComponentQuery.query('#base-city-CityAdd-form')[0];

        if(CityAddForm.isValid()) {
            var merge = {};
            var region = Ext.apply({
                id: id,
                name: province
            });
            regions = {
                "state": region
            }
            var merge = Ext.apply(regions, {
                "name": city
            });
            var url = 'cxf/rest/City/save'
            var util = Ext.create('MyDesktop.Util.util', {
                callBack: this.callback,
                url: url,
                data: merge
            });
            util.sendRequest();
        }
    },
    init: function(application) {
        this.control({
            "#base-city-CityAdd-save-btn-submit": {
                click: this.onSaveCity
            }
        });
    }
});
