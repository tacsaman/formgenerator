Ext.define('MyDesktop.Modules.Base.Client.subModule.City.controller.MainController', {
    extend: 'Ext.app.Controller',
    onAddCity: function() {
        var CityAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.City.view.CityAddForm', {
            /*configType: '0',
            selectedRecord: '',
            saveFlag: '0'*/
        });
        CityAddForm.show();
    },
    init: function(application) {
        this.control({
            "#base-city-grid-addBtn": {
                click: this.onAddCity
            }

        });
    }
});
