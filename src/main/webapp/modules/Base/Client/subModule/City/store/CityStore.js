Ext.define('MyDesktop.Modules.Base.Client.subModule.City.store.CityStore', {
	extend: 'Ext.data.Store',
	requires: [
		'MyDesktop.Modules.Base.Client.subModule.City.model.CityModel',
		'Ext.data.proxy.Ajax',
		'Ext.data.reader.Json'
	],
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			autoLoad: true,
			model: 'MyDesktop.Modules.Base.Client.subModule.City.model.CityModel',
			storeId: 'CityStore',
			proxy: {
				type: 'ajax',
				method: 'GET',
				url: 'cxf/rest/City/getAll',
				reader: {
					type: 'json',
					root: 'data'
				}
			}
		}, cfg)]);
	}
});
