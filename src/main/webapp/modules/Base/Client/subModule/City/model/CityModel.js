Ext.define('MyDesktop.Modules.Base.Client.subModule.City.model.CityModel', {
	extend: 'Ext.data.Model',
	requires: [
		'Ext.data.Field'
	],
	fields: [{
		name: 'id'
	}, {
		name: 'ProvinceName', convert: function(v, rec) {if(rec.raw.state) return rec.raw.state.name}
	}, {
		name: 'name'
	}]
});