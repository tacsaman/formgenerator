Ext.define('MyDesktop.Modules.Base.Client.subModule.City.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller=Ext.create('MyDesktop.Modules.Base.Client.subModule.City.controller.MainController');
        controller.init();

        var CityAddFormController=Ext.create('MyDesktop.Modules.Base.Client.subModule.City.controller.CityAddFormController');
        CityAddFormController.init();
    }

});
