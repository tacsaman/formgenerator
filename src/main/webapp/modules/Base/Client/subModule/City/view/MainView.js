Ext.define('MyDesktop.Modules.Base.Client.subModule.City.view.MainView', {
	extend: 'Ext.container.Container',
	requires: [
		'Ext.ux.customActionCol'
	],
	title: 'شهرستان',
	itemId: 'base-city-panel',
	layout: 'fit',
	initComponent: function() {
		var me = this;
		CityForm = this;
		CityForm.connection = this.connection;
		var CityStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.City.store.CityStore');
		Ext.applyIf(me, {
			items: [{
				xtype: 'customgrid',
				//dock: 'top',
				title: '',
				forcefit: true,
				cancelObject: 'CityName',
				itemId: 'base-city-grid',
				newBtnItemId: 'base-city-grid-addBtn',
				newBtnText: 'اضافه کردن شهرستان',
				newBtnTip: 'اضافه کردن شهرستان',
				hidesearchbar: true,
				hidePaging: true,
				hideRefreshBtn: false,
				editable: true,
				showRowonNew: false,
				store: CityStore,
				model: 'MyDesktop.Modules.Base.Client.subModule.City.model.CityModel',
				editObject:{},
				editkeyArray: [
					['id' , 'id'],
					['ProvinceName', 'ProvinceName'],
					['name', 'name']
				],
				editURL: 'cxf/rest/City/update',
				columns: [{
					xtype: 'gridcolumn',
					dataIndex: 'ProvinceName',
					text: 'نام استان',
					flex: 1
				}, {
					xtype: 'gridcolumn',
					dataIndex: 'name',
					text: 'نام شهرستان',
					flex: 1,
					editor: {
						xtype: 'textfield',
						name: 'name',
						allowBlank: false,
						itemId: 'base-city-NameCity-txt'
					}
				},{
					xtype: 'customactioncol',
					flex: 1,
					itemId: 'base-city-grid-customactioncolumn',
					deleteurl: 'cxf/rest/City/delete',
					deletetip: 'حذف',
					alwaysShowDelete: true,
					text: 'عملیات',
					deleteobject: {},
					deletekeyArray: [
						['id' , 'id'],
						['ProvinceName', 'ProvinceName'],
						['name', 'name']
					],
					deletable: true,
					editable: true,
					defaultCustomEdit: false,
				}]
			}]
		});
		me.callParent(arguments);
	}

});