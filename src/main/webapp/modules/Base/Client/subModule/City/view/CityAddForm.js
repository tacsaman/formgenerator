Ext.define('MyDesktop.Modules.Base.Client.subModule.City.view.CityAddForm', {
	extend: 'Ext.window.Window',
	width: 830,
	title: 'اضافه کردن شهرستان',
	itemId:'base-city-CityAdd-win',
	modal: true,
	layout: 'fit',

	initComponent: function(arguments) {
		var me = this;
		CityAddForm = this;
		//var ProvinceStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.City.store.ProvinceStore');
		var ProvinceStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Province.store.ProvinceStore');

		Ext.applyIf(me, {
			items: [{
				xtype: 'customform',
				layout: 'auto',
				autoLoad: false,
				autoScroll: true,
				bodyPadding: 10,
				title: '',
				itemId: 'base-city-CityAdd-form',
				//submitUrl: 'cxf/rest/City/save',
				//saveCallback: this.callback,
				saveItemId: 'base-city-CityAdd-save-btn',
				hideResetBtn: true,
				defaultSave: false,
				defaultReset: true,
				items: [{
					xtype: 'combobox',
					itemId: 'base-city-CityAdd-ProvinceName-cmb',
					fieldLabel: 'نام استان',
					name: 'ProvinceName',
					displayField: 'name',
					valueField: 'name',
					allowBlank: false,
					emptyText: 'select ...',
					queryMode: 'local',
					store:ProvinceStore,
					typeAhead: true
				},{
					xtype: 'textfield',
					itemId: 'base-city-CityAdd-CityName-txt',
					width: 280,
					fieldLabel: 'نام شهرستان',
					name: 'CityName',
					allowBlank: false
				}]
			}]
		});

		me.callParent(arguments);
	}

});
