Ext.define('MyDesktop.Modules.Base.Client.subModule.Templates.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller=Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.controller.MainController');
        controller.init();
	var AddTemplateController=Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.controller.AddTemplateController');
        AddTemplateController.init();

    }

});
