Ext.define('MyDesktop.Modules.Base.Client.subModule.Templates.view.TemplateAddForm', {
	extend: 'Ext.window.Window',
	width: 830,
	title: 'اضافه کردن الگو',
	modal: true,
	initComponent: function(arguments) {
		var me = this;
		TemplateAddForm = this;
		TemplateAddForm.formData = this.selectedRecord;
		TemplateAddForm.configType = this.configType;
		TemplateAddForm.saveFlag = this.saveFlag;
		TemplateAddForm.memberId = this.memberId;
		Ext.applyIf(me, {
			items: [{
				xtype: 'customform',
				layout: 'auto',
				autoLoad: false,
				autoScroll: true,
				layout: 'auto',
				bodyPadding: 10,
				title: '',
				itemId: 'base-Templates-TemplatesAdd-form',
				submitUrl: 'cxf/rest/template/save',
				//saveCallback: TemplateAddForm.callback,
				saveItemId: 'base-Templates-TemplateAdd-save-btn',
				hideResetBtn: true,
				defaultSave: false,
				defaultReset: true,
				items: [{
					xtype: 'textfield',
					itemId: 'Base-Templates-TemplateAdd-templateName-txt',
					width: 280,
					fieldLabel: 'نام',
					name: 'templateName'
				}, {
					xtype: 'htmleditor',
					height: 300,
					width: 626,
					fontFamilies: ['Arial', 'Courier New', 'Tahoma', 'Times New Roman', 'Verdana', 'Lotus', 'B Nazanin'],
					itemId: 'Base-Templates-TemplateAdd-templateBody-htmleditor',
					id: 'Base-Templates-TemplateAdd-templateBody-htmleditor',
					fieldLabel: 'متن',
					name: 'templateBody'

				}]
			}]
		});

		me.callParent(arguments);
	}

});
