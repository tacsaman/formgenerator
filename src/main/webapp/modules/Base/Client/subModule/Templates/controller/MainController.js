Ext.define('MyDesktop.Modules.Base.Client.subModule.Templates.controller.MainController', {
    extend: 'Ext.app.Controller',
    onTemplatesNewBtnClick: function() {
        var TemplateAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.view.TemplateAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        TemplateAddForm.show();
    },
    onTemplatesCustomSelectionAction: function(view, rowIndex, colIndex, item, e, record, row) {
	var formToShow = Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.view.TemplateAddForm', {
	    configType: '1',
	    selectedRecord: record,
	    saveFlag: '1',
	    callback: this.callback,
	});
        formToShow.show();
    },
    init: function(application) {
        this.control({
            "#base-templates-grid-addBtn": {
                click: this.onTemplatesNewBtnClick
            },
			"#base-templates-grid-customactioncolumn": {
				customSelectionAction: this.onTemplatesCustomSelectionAction
            }

        });
    }
});
