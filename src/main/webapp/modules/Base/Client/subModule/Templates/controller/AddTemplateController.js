Ext.define('MyDesktop.Modules.Base.Client.subModule.Templates.controller.AddTemplateController', {
	extend: 'Ext.app.Controller',
	onTemplatesSaveBtnClick: function() {},
    callback: function() {
        var Grid = Ext.ComponentQuery.query('#base-templates-grid')[0];
        var gridStore = Grid.getStore();
        gridStore.reload();
		TemplateAddForm.close();
    },
	onTemplatesNewBtnClick: function() {
		var TemplateAddForm = Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.view.TemplateAddForm', {
			configType: '0',
			selectedRecord: '',
			saveFlag: '0'
		});
		TemplateAddForm.show();
	},
	onSave: function() {
		var form = Ext.ComponentQuery.query('#base-Templates-TemplatesAdd-form')[0];
		var url="cxf/rest/template/update";
		
		var merge = Ext.apply(form.getValues());
		if (TemplateAddForm.configType == "0") {
			url="cxf/rest/template/save";
			delete merge['id'];
		}
		else merge.id=TemplateAddForm.formData.data.id
		var util = Ext.create('MyDesktop.Util.util', {
			callBack: this.callback,
			url: url,
			data: merge
		});
		util.sendRequest();
	},
	loadForm: function() {
		var form = Ext.ComponentQuery.query('#base-Templates-TemplatesAdd-form')[0];
		thisthis = this;
		if (TemplateAddForm.configType == "1") {
			form.load({
				params: {
					id: TemplateAddForm.formData.data.id
				},
				url: 'cxf/rest/template/getById',
				method: "GET"
			})

		}
		form.on({
			actioncomplete: function(form, action) {
				if (action.type == 'load') {
					var contact = action.result.data;
					Ext.ComponentQuery.query('#base-Templates-TemplatesAdd-form')[0].submitUrl = 'cxf/rest/template/update';
					Ext.ComponentQuery.query('#Base-Templates-TemplateAdd-templateName-txt')[0].setReadOnly(true);
				}
			}
		})
	},
	init: function(application) {

		this.control({
			"#base-Templates-TemplateAdd-save-btn": {
				click: this.onTemplatesNewBtnClick
			},
			"#base-Templates-TemplatesAdd-form": {
				afterrender: this.loadForm
			},
			"#base-Templates-TemplateAdd-save-btn-submit":{
				click: this.onSave
			}
		});
	}


});
