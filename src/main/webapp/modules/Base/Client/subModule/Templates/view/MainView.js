Ext.define('MyDesktop.Modules.Base.Client.subModule.Templates.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'الگو',
	 itemId: 'base-templates-panel',
    layout: 'fit',
	initComponent: function() {
 	var me = this;
	zonesForm = this;
	zonesForm.connection = this.connection;
	var TemplatesStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.store.TemplatesStore');
	Ext.applyIf(me, {
            items: [{
                xtype: 'customgrid',
                dock: 'top',
                title: '',
                forcefit: true,
				cancelObject:'name',
                itemId: 'base-templates-grid',
                newBtnItemId: 'base-templates-grid-addBtn',
                newBtnText: 'اضافه کردن الگو',
                newBtnTip: 'اضافه کردن الگو',
				hidesearchbar: true,
				hidePaging: true,
                hideRefreshBtn: false,
				editable: false,
				showRowonNew: false,
				disableEditRow: false,
                store: TemplatesStore,
                model: 'MyDesktop.Modules.Base.Client.subModule.Templates.model.TemplatesModel',
				addURL: 'cxf/rest/template/save',
				editURL: 'cxf/rest/template/update',	
                columns: [{
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'کد',
                    flex: 1,
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'templateName',
                    text: 'نام الگو',
                    flex: 1,
				}, {
					xtype: 'customactioncol',
					flex: 1,
					itemId: 'base-templates-grid-customactioncolumn',
					deleteurl: 'cxf/rest/template/delete',
					deletetip: 'حذف',
					alwaysShowDelete: true,
					text: 'عملیات',
					deleteobject: {},
					deletekeyArray: [
						[
							'id', 'id'
						]
					],
					deletable: true,
					editable: true,
					defaultCustomEdit: false,
					items: [{
						handler: function(view, rowIndex, colIndex, item, e, record, row) {
							this.fireEvent('customSelectionAction', view, rowIndex, colIndex, item, e, record, row);
						},
						icon: 'resources/images/default/component/user_edit.gif',
						tooltip: 'ویرایش'
					}]
                }]
            }]
        });
        me.callParent(arguments);
    }

});
