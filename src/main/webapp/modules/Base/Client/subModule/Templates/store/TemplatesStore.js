Ext.define('MyDesktop.Modules.Base.Client.subModule.Templates.store.TemplatesStore', {
	extend: 'Ext.data.Store',
	requires: [
		'MyDesktop.Modules.Base.Client.subModule.Templates.model.TemplatesModel',
		'Ext.data.proxy.Ajax',
		'Ext.data.reader.Json'
	],
	constructor: function(cfg) {
		var me = this;
		cfg = cfg || {};
		me.callParent([Ext.apply({
			autoLoad: true,
			model: 'MyDesktop.Modules.Base.Client.subModule.Templates.model.TemplatesModel',
			storeId: 'RegionStore',
			proxy: {
				type: 'ajax',
				method: 'GET',
				url: 'cxf/rest/template/getAll',
				reader: {
					type: 'json',
					root: 'data'
				}
			}
		}, cfg)]);
	}
});
