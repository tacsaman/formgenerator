Ext.define('MyDesktop.Modules.Process.Client.Process', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'process-win',
    init : function(){
        this.launcher = {
            text: 'فرآیندها',								
            iconCls:'icon-process',								
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('process-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'process-win',									
                title:'فرآیندها',								
                width:1000,										
                height:550,											
                iconCls: 'icon-process',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Process/Client/tree-data.json' , prefix: 'Process'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
