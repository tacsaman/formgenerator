Ext.define('MyDesktop.Modules.Process.Client.subModule.Documents.model.DocumentsModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'id'
        },
        {
            name: 'code'
        },
	{
            name: 'documentType'
        }
	,{
            name: 'documentName'
        }
	,{
            name: 'changeCause'
        }
	,{
            name: 'documentBody'
        }

    ]
});
