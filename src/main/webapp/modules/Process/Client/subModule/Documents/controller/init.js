Ext.define('MyDesktop.Modules.Process.Client.subModule.Documents.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller=Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.controller.MainController');
        controller.init();
		if (! Ext.ClassManager.isCreated('MyDesktop.Modules.Process.Client.subModule.Documents.controller.AddDocumentController')){
			var AddDocumentController=Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.controller.AddDocumentController');
        	AddDocumentController.init();
		}
    }

});
