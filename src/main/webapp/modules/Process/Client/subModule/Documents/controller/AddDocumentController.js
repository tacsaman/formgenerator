Ext.define('MyDesktop.Modules.Process.Client.subModule.Documents.controller.AddDocumentController', {
	extend: 'Ext.app.Controller',
	onDocumentsSaveBtnClick: function() {
	},
	callback2: function() {
		if (DocumentAddForm.Class == "cartable")
			var Grid = Ext.ComponentQuery.query('#cartable-cartables-grid')[0];
		else
			var Grid = Ext.ComponentQuery.query('#cartable-cartablesGroups-grid')[0];
		var gridStore = Grid.getStore();
		gridStore.reload();
		DocumentAddForm.close();
	},

	callback: function() {
		var Grid = Ext.ComponentQuery.query('#process-documents-grid')[0];
		var gridStore = Grid.getStore();
		gridStore.reload();
		DocumentAddForm.close();
	},
	onDocumentsNewBtnClick: function() {
		var DocumentAddForm = Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.view.DocumentAddForm', {
			configType: '0',
			selectedRecord: '',
			saveFlag: '0'
		});
		DocumentAddForm.show();
	},
	onDocumentsTayidBtnClick: function() {
		var util = Ext.create('MyDesktop.Util.util');
		var box = Ext.MessageBox.wait('لطفا صبر نمایید ...', 'ارسال به سمت سرور');
		var me = this;
		if (DocumentAddForm.eventName == "formTasvib")
 			var variables = {tasvib:"true"}; 
 		else
 			var variables = {tayid:"true",nextStep:'tasvib'}; 
			variables = {taskId:DocumentAddForm.taskId,variables:variables}
			var ajax = Ext.Ajax.request({
				headers: { 'Content-Type': 'application/json' },
				url: 'cxf/rest/cartable/completeTask',
				method: 'POST',
				async: false,
				params:  Ext.encode(variables),
				success: function (response) {
					var decoded = Ext.decode(response.responseText);
					if(decoded.errorMessage){
						box.hide();
				//		util.showNotification(decoded.errorMessage)
					}else{
						box.hide();
						DocumentAddForm.close();
						me.callback2()
					}
				}
			});
	},
	onDocumentsRadBtnClick: function() {
		var util = Ext.create('MyDesktop.Util.util');
		var box = Ext.MessageBox.wait('لطفا صبر نمایید ...', 'ارسال به سمت سرور');
		var me = this;
		var variables = {nextStep:"cancel"}; 
		variables = {taskId:DocumentAddForm.taskId,variables:variables}
		var ajax = Ext.Ajax.request({
			headers: { 'Content-Type': 'application/json' },
			url: 'cxf/rest/cartable/completeTask',
			method: 'POST',
			async: false,
			params:  Ext.encode(variables),
			success: function (response) {
				var decoded = Ext.decode(response.responseText);
				if(decoded.errorMessage){
					box.hide();
				//	util.showNotification(decoded.errorMessage)
						me.callback2()
				}else{
					box.hide();
					DocumentAddForm.close();
				}
			}
		});
	},
	onDocumentsReviewBtnClick: function() {
		var util = Ext.create('MyDesktop.Util.util');
		var box = Ext.MessageBox.wait('لطفا صبر نمایید ...', 'ارسال به سمت سرور');
		var me = this;
		var variables = {nextStep:"baznegari"}; 
		variables = {taskId:DocumentAddForm.taskId,variables:variables}
		var ajax = Ext.Ajax.request({
		headers: { 'Content-Type': 'application/json' },
			url: 'cxf/rest/cartable/completeTask',
			method: 'POST',
			async: false,
			params:  Ext.encode(variables),
			success: function (response) {
				var decoded = Ext.decode(response.responseText);
				if(decoded.errorMessage){
					box.hide();
					//util.showNotification(decoded.errorMessage)
						me.callback2()
				}else{
					box.hide();
					DocumentAddForm.close();
				}
			}
		});
	},
	onDocumentChange: function() {
		var docType = Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getValue();
		if (docType!="" && !(isNaN(docType))){
			var ajax = Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json'
				},
				url: 'cxf/rest/template/getById',
				method: 'GET',
				async: false,
				params: {
					"id": docType
				},
				success: function(response) {
					var decoded = Ext.decode(response.responseText);
					Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentBody-htmleditor')[0].setValue(decoded.data.templateBody)
				}
			});
		}
	},
	loadForm: function() {
		Ext.ComponentQuery.query('#process-Documents-DocumentAdd-tayid-btn')[0].hide()
		Ext.ComponentQuery.query('#process-Documents-DocumentAdd-rad-btn')[0].hide()
		Ext.ComponentQuery.query('#process-Documents-DocumentAdd-review-btn')[0].hide()
		var form = Ext.ComponentQuery.query('#process-Documents-DocumentsAdd-form')[0];
		thisthis = this;
		if (DocumentAddForm.configType == "1") {
			if (DocumentAddForm.formData.data)
				var id = DocumentAddForm.formData.data.id;
			else 
				var id = DocumentAddForm.memberId
			form.load({
				params: {
					id: id
				},
				url: 'cxf/rest/document/getById',
				method: "GET"
			})

		}		
		else if (DocumentAddForm.configType == "2") {
			if (DocumentAddForm.formData.data)
				var id = DocumentAddForm.formData.data.id;
			else 
				var id = DocumentAddForm.memberId
			form.load({
				params: {
					id: id
				},
				url: 'cxf/rest/finalDoc/getById',
				method: "GET"
			})

		}
		form.on({
			actioncomplete: function(form, action) {
				if (action.type == 'load') {
					var contact = action.result.data;
					Ext.ComponentQuery.query('#process-Documents-DocumentsAdd-form')[0].submitUrl= 'cxf/rest/document/update';
					if (!isNaN(DocumentAddForm.memberId)) {
						Ext.ComponentQuery.query('#process-Documents-DocumentAdd-tayid-btn')[0].show()
						Ext.ComponentQuery.query('#process-Documents-DocumentAdd-rad-btn')[0].show()
						Ext.ComponentQuery.query('#process-Documents-DocumentAdd-review-btn')[0].show()
					}
					if (DocumentAddForm.eventName == "createUpdateDocument"){
						Ext.ComponentQuery.query('#process-Documents-DocumentAdd-review-btn')[0].hide()
						Ext.ComponentQuery.query('#process-Documents-DocumentAdd-rad-btn')[0].hide()
					}
				}
			}
		})
	},
	returnId:  function (listStore,value){
		for (var i = 0; i < listStore.getCount(); i++) {
			record = listStore.getAt(i);
			if (value = record.get("name"));
				return record.get("id");
		}
		
	},
	onSave: function() {
		var form = Ext.ComponentQuery.query('#process-Documents-DocumentsAdd-form')[0];
		var url="cxf/rest/document/update";
		var merge = Ext.apply(form.getValues());
		if ((DocumentAddForm.configType == "0") || (DocumentAddForm.configType == "2")) {
			url="cxf/rest/document/saveStartProcess";
			delete merge['id'];
		}
		else {
			if (isNaN(DocumentAddForm.memberId))
				merge.id=DocumentAddForm.formData.data.id
			else
				merge.id= DocumentAddForm.memberId;
			if (isNaN(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentType-cmb')[0].getValue()))
				merge.documentType=this.returnId(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentType-cmb')[0].getStore(),Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentType-cmb')[0].getValue());
			if (isNaN(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getValue()))
				merge.documentTemplate=this.returnId(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getStore(),Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getValue());
		}
		if (DocumentAddForm.configType == "2"){
			merge.referencedDocument=DocumentAddForm.formData.data.id;
			if (isNaN(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentType-cmb')[0].getValue()))
				merge.documentType=this.returnId(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentType-cmb')[0].getStore(),Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentType-cmb')[0].getValue());
			if (isNaN(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getValue()))
				merge.documentTemplate=this.returnId(Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getStore(),Ext.ComponentQuery.query('#Process-Documents-DocumentAdd-documentTemplate-cmb')[0].getValue());

		}
		var util = Ext.create('MyDesktop.Util.util', {
			callBack: this.callback,
			url: url,
			data: merge
		});
		var ajax = Ext.Ajax.request({
		headers: { 'Content-Type': 'application/json' },
			url: url,
			method: 'POST',
			async: false,
			params:  Ext.encode(merge),
			success: function (response) {
				var decoded = Ext.decode(response.responseText);
				if(decoded.errorMessage){
					util.showNotification(decoded.errorMessage)
				}else{
					if (isNaN(DocumentAddForm.memberId)){
						if (!isNaN(decoded.data))
						{
							variables = {taskId:decoded.data}
							var ajax = Ext.Ajax.request({
							headers: { 'Content-Type': 'application/json' },
								url: 'cxf/rest/cartable/goToTaskForm',
								method: 'POST',
								async: false,
								params:  Ext.encode(variables),
								success: function (response) {
									var decoded = Ext.decode(response.responseText);
									if(decoded.errorMessage){
										util.showNotification(decoded.errorMessage)
									}
									else
										DocumentAddForm.close();
								}
							});
						}
					}
					else{
						DocumentAddForm.close();
					}
				}
			}
		});
/*
		var util = Ext.create('MyDesktop.Util.util', {
			callBack: this.callback,
			url: url,
			data: merge
		});
		util.sendRequest();
*/
	},
	init: function(application) {

		this.control({	
			"#process-Documents-DocumentAdd-tayid-btn": {
				click: this.onDocumentsTayidBtnClick
			},
			"#process-Documents-DocumentAdd-rad-btn": {
				click: this.onDocumentsRadBtnClick
			},
			"#process-Documents-DocumentAdd-review-btn": {
				click: this.onDocumentsReviewBtnClick
			},
			"#process-Documents-DocumentAdd-save-btn": {
				click: this.onDocumentsNewBtnClick
			},
			"#process-Documents-DocumentsAdd-form": {
				afterrender: this.loadForm
			},
			"#Process-Documents-DocumentAdd-documentTemplate-cmb": {
				change: this.onDocumentChange
			},
			"#process-Documents-DocumentAdd-save-btn-submit":{
				click: this.onSave
			}
		});
	}


});
