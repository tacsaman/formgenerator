Ext.define('MyDesktop.Modules.Process.Client.subModule.Documents.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.customActionCol'
    ],
    title: 'مستندات',
	 itemId: 'process-documents-panel',
    layout: 'fit',
	initComponent: function() {
 	var me = this;
	zonesForm = this;
	zonesForm.connection = this.connection;
	var DocumentsStore = Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.store.DocumentsStore');
	Ext.applyIf(me, {
            items: [{
                xtype: 'customgrid',
                dock: 'top',
                title: '',
                forcefit: true,
		cancelObject:'name',
                itemId: 'process-documents-grid',
                newBtnItemId: 'process-documents-grid-addBtn',
                newBtnText: 'اضافه کردن مستند',
                newBtnTip: 'اضافه کردن مستند',
		hidesearchbar: true,
		hidePaging: true,
                hideRefreshBtn: false,
                editable: false,
                showRowonNew: false,
		disableEditRow: true,
                store: DocumentsStore,
                model: 'MyDesktop.Modules.Process.Client.subModule.Documents.model.DocumentsModel',
		addURL: 'cxf/rest/switchType/save',
		editURL: 'cxf/rest/switchType/update',	
                columns: [{
                    xtype: 'gridcolumn',
                    dataIndex: 'id',
                    text: 'کد',
                    flex: 1,
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'code',
                    text: 'شمارنده',
                    flex: 1,
                }, {
                    xtype: 'gridcolumn',
                    dataIndex: 'documentName',
                    text: 'نام',
                    flex: 1,
				}, {
					xtype: 'customactioncol',
					flex: 1,
					itemId: 'process-documents-grid-customactioncolumn',
					text: 'عملیات',
					deletable: false,
					editable: true,
					defaultCustomEdit: false,
					items: [{
						handler: function(view, rowIndex, colIndex, item, e, record, row) {
							this.fireEvent('customSelectionAction', view, rowIndex, colIndex, item, e, record, row);
						},
						icon: 'resources/images/default/component/user_edit.gif',
						tooltip: 'ویرایش'
					}]
                }]
            }]
        });
        me.callParent(arguments);
    }

});
