Ext.define('MyDesktop.Modules.Process.Client.subModule.Documents.view.DocumentAddForm', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.ux.twinCombo'
    ],
    width: 830,
    title: 'اضافه کردن مستند',
id:'11',
itemId:'11',
	modal: true,
    initComponent: function(arguments) {
        var me = this;
        DocumentAddForm = this;
        DocumentAddForm.formData = this.selectedRecord;
        DocumentAddForm.configType = this.configType;
        DocumentAddForm.saveFlag = this.saveFlag;
        DocumentAddForm.memberId = this.memberId;
        DocumentAddForm.taskId = this.taskId;
        DocumentAddForm.eventName = this.eventName;
		var TypesStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Types.store.TypesStore');
		var TemplatesStore = Ext.create('MyDesktop.Modules.Base.Client.subModule.Templates.store.TemplatesStore');
        Ext.applyIf(me, {
            items: [{
                xtype: 'customform',
                layout: 'auto',
		autoLoad:false,
                autoScroll: true,
                layout: 'auto',
                bodyPadding: 10,
                title: '',
				itemId:'process-Documents-DocumentsAdd-form',
                submitUrl: 'cxf/rest/document/saveStartProcess',
		//saveCallback: DocumentAddForm.callback,
                saveItemId: 'process-Documents-DocumentAdd-save-btn',
                hideResetBtn: true,
				defaultSave: false,
                defaultReset: true,
		toolbarArray: [{
		    text: 'تایید',
		    itemId: 'process-Documents-DocumentAdd-tayid-btn',
		    iconCls: 'icon-save',
		    hidden: true
		}, {
		    text: 'رد',
		    itemId: 'process-Documents-DocumentAdd-rad-btn',
		    iconCls: 'icon-cancel',
		    hidden: true
		}, {
		    text: 'بازنگری',
		    itemId: 'process-Documents-DocumentAdd-review-btn',
		    iconCls: 'icon-user-view',
		    hidden: true
		}],

                items: [
		{
                        xtype: 'textfield',
                        itemId: 'Process-Documents-DocumentAdd-documentName-txt',
                        width: 280,
                        fieldLabel: 'نام',
                        name: 'documentName'
		},
		{
                            xtype: 'combobox',
                            itemId: 'Process-Documents-DocumentAdd-documentType-cmb',
                            fieldLabel: 'نوع مستند',
                            name: 'documentType',
                            displayField: 'name',
                            valueField: 'id',
                            allowBlank: false,
                            emptyText: 'select ...',
                            queryMode: 'local',
                            store:TypesStore,
                            typeAhead: true
		},
		{
                            xtype: 'combobox',
                            itemId: 'Process-Documents-DocumentAdd-documentTemplate-cmb',
                            fieldLabel: 'مستند الکو',
                            name: 'documentTemplate',
                            displayField: 'templateName',
                            valueField: 'id',
                            allowBlank: false,
                            emptyText: 'select ...',
                            queryMode: 'local',
                            store:TemplatesStore,
                            typeAhead: true
		},
		{
                                xtype: 'textareafield',
                                height: 100,
                                width: 426,
                                itemId: 'Process-Documents-DocumentAdd-changeCause-txtarea',
                                id: 'Process-Documents-DocumentAdd-changeCause-txtarea',
                                fieldLabel: 'علت ایجاد',
                                name: 'changeCause'
		},{
										xtype: 'htmleditor',
                                height: 300,
                                width: 626,
                                fontFamilies:['Arial', 'Courier New', 'Tahoma', 'Times New Roman', 'Verdana','Lotus','B Nazanin'],
                                itemId: 'Process-Documents-DocumentAdd-documentBody-htmleditor',
                                id: 'Process-Documents-DocumentAdd-documentBody-htmleditor',
                                fieldLabel: 'متن',
                                name: 'documentBody'

		}
		]
            }]
        });

        me.callParent(arguments);
    }

});
