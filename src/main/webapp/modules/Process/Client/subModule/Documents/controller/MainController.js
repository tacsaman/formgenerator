Ext.define('MyDesktop.Modules.Process.Client.subModule.Documents.controller.MainController', {
    extend: 'Ext.app.Controller',
    onDocumentsNewBtnClick: function() {
var win = Ext.getCmp("11");
if (!win){
        var DocumentAddForm = Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.view.DocumentAddForm', {
            configType: '0',
            selectedRecord: '',
            saveFlag: '0'
        });
        DocumentAddForm.show();
}
    },

    onDocumentCustomSelectionAction: function(view, rowIndex, colIndex, item, e, record, row) {
	var formToShow = Ext.create('MyDesktop.Modules.Process.Client.subModule.Documents.view.DocumentAddForm', {
	    configType: '2',
	    selectedRecord: record,
	    saveFlag: '1',
	    callback: this.callback,
	});
        formToShow.show();
    },
    init: function(application) {
        this.control({
            "#process-documents-grid-addBtn": {
                click: this.onDocumentsNewBtnClick
            },
			"#process-documents-grid-customactioncolumn": {
				customSelectionAction: this.onDocumentCustomSelectionAction
            }

        });
    }
});
