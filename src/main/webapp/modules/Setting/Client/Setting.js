Ext.define('MyDesktop.Modules.Setting.Client.Setting', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'setting-win',
    init : function(){
        this.launcher = {
            text: 'تنظیمات',								
            iconCls:'icon-settings',								
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('setting-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'setting-win',									
                title:'تنظیمات',								
                width:1000,										
                height:550,											
                iconCls: 'icon-settings',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/Setting/Client/tree-data.json' , prefix: 'Setting'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
