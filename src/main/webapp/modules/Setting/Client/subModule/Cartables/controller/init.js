Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.init', {
    extend: 'Ext.app.Controller',
    init: function(application) {
        var controller=Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Cartables.controller.MainController');
        controller.init();
	var AddDocumentController=Ext.create('MyDesktop.Modules.Cartable.Client.subModule.Documents.controller.AddDocumentController');
        AddDocumentController.init();

    }

});
