Ext.define('MyDesktop.Modules.SystemStatus.Client.SystemStatus', {			
    extend: 'Ext.ux.desktop.Module',
    requires: [
	'Ext.layout.container.Fit',
    ],
    id:'SystemStatus-win',
    init : function(){
        this.launcher = {
            text: 'داشبورد',								
            iconCls:'icon-settings',								
            handler : this.createWindow,
            scope: this
        };
    },
    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('SystemStatus-win');
        if(!win){
            win = desktop.createWindow({							
                id: 'SystemStatus-win',									
                title:'داشبورد',								
                width:1000,										
                height:550,											
                iconCls: 'icon-settings',							
                animCollapse:false,
                constrainHeader:true,
                bodyBorder: true,
                layout: 'fit',
                border: false,
                items: [
                    {xtype:'module' , url:'modules/SystemStatus/Client/tree-data.json' , prefix: 'SystemStatus'}
                ]
            });
        }
        win.show();
        return win;
    }
});

	
