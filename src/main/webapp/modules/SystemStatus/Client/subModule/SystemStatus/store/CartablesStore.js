Ext.define('MyDesktop.Modules.Cartable.Client.subModule.Cartables.store.CartablesStore', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.Modules.Cartable.Client.subModule.Cartables.model.CartablesModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
				model: 'MyDesktop.Modules.Cartable.Client.subModule.Cartables.model.CartablesModel', 
            storeId: 'RegionStore',
            proxy: {
                type: 'ajax',
                method:'GET',
                url:'cxf/rest/cartable/getPersonalTaskList',
                reader: {
                    type: 'json',root: 'data'
                }
            }
        }, cfg)]);
    }
});
