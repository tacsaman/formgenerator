/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */

Ext.define('MyDesktop.DeskAccountXtypes.customerCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.customerCombo',

    initComponent: function () {
        var me = this;
        me.store =  Ext.create('Ext.data.Store', {

            model: 'customerListModel',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: 'ExtDesk.php',
                method: 'GET',
                extraParams: {
                    Module: 'Factor',
                    option: 'customerList',
                    action: 'read',
                    type: me.type

                },
                reader: {
                    type: 'json',
                    root: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                }
            }
        });

        me.displayField =  "CustomerName";
        me.valueField = "Customer_ID";
        me.typeAhead = true;
        me.minChars = 1;
        me.emptyText = 'انتخاب مقدار...';
        me.forceSelection = true;
        me.selectOnFocus = true;


        this.callParent();

    }


});
