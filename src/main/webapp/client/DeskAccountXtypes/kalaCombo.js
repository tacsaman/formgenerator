/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */

Ext.define('MyDesktop.DeskAccountXtypes.kalaCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.kalaCombo',

    initComponent: function () {
        var me = this;
        Ext.define('kalaListModel', {
            extend: 'Ext.data.Model',

            fields: [{
                name: 'stuffID'
            }, {
                name: 'StaffName'
            }]
        });
        me.store =  Ext.create('Ext.data.Store', {

            model: 'kalaListModel',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: 'ExtDesk.php',
                method: 'GET',
                extraParams: {
                    Module: 'Kala',
                    option: 'read',
                    action: 'kalaCombo'
                },
                reader: {
                    type: 'json',
                    root: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                }
            }
        });

        me.displayField =  "StaffName";
        me.valueField = "stuffID";
        me.typeAhead = true;
        me.minChars = 1;
        me.allowBlank = false;
        me.emptyText = 'انتخاب مقدار...';
        me.forceSelection = true;
        me.selectOnFocus = true;
        //me.selectedIndex = 0;// = me.store.getAt(0);


        this.callParent();

    }


});