/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('MyDesktop.DeskAccountXtypes.moeinPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.moeinPanel',

    initComponent: function () {
        var me = this;

        if (!me.kalaCombo)
        {
            me.kalaComboItem = {border:false};
        }
        else {
            me.kalaComboItem = {
                xtype: 'kalaCombo',
                anchor: '100%',
                fieldLabel: 'نام کالا'
            }  }
        if (!me.customerCombo)
        {
            me.customerComboItem = {border:false};
        }
        else {
            me.customerComboItem = {
                xtype: 'customerCombo',
                anchor: '100%',
                fieldLabel: 'نام مشتری'
            }  }



            me.config= {
            bodyPadding: 10,
            title: '',
            layout:'column',
            borderWidth:0,
            border: false,
            items: [
                {items: [
                    me.kalaComboItem,
                    me.customerComboItem
                ], columnWidth: 0.5,border:false},
                {items: [
                    {
                        id: this.gridPrefix + "FromDateId",
                        xtype: 'datefield',
                        plugins: ['jalalidate'],
                        fieldLabel: 'از تاریخ',
                        labelWidth: 50,
                        format: "Y/m/d"

                    },
                    {
                        id: this.gridPrefix + "ToDateId",
                        xtype: 'datefield',
                        plugins: ['jalalidate'],
                        fieldLabel: 'تا تاریخ',
                        labelWidth: 50
                    }
                ], columnWidth: 0.5,border:false}

                ,

                {
                    columnWidth: 0.5
                    ,border:false ,

                    items:[{width:80,
                        xtype: 'button',
                        bodystyle:'margin-right:200px;',
                        text: 'جستجو'}]

                }
            ]};




        me.groupField = "factorID";


      

        this.callParent();

    }


});
