/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('MyDesktop.DeskAccountXtypes.sumGrid', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.sumGrid',

    initComponent: function () {
        var me = this;

        me.listeners = {
            edit: {
                fn: function () {
                    me.saveGrid(me.id, 'ExtDesk.php', me.module, me.option, 'create');
                }
            },
            canceledit: {
                fn: function () {

                    me.store.reload();
                }
            }
        }

        if (!me.kalaCombo)
        {
            me.kalaComboItem = {border:false};
        }
        else {
            me.kalaComboItem = {
                xtype: 'kalaCombo',
                anchor: '100%',
                fieldLabel: 'نام کالا',
                id:me.gridPrefix+'kalacombo'
            }  }
        if (!me.customerCombo)
        {
            me.customerComboItem = {border:false};
        }
        else {
            me.customerComboItem = {
                xtype: 'customerCombo',
                anchor: '100%',
                fieldLabel: 'نام مشتری',
                id:me.gridPrefix+'customercombo'
            }  }

        Ext.define('anbarGridModel', {
            extend: 'Ext.data.Model',

            fields: [
                {
                    name: 'FactorDetail_ID'
                },
                {
                    name: 'FactorDate'
                },
                {
                    name: 'FactorNo',
                    type: 'int'
                },
                {
                    name: 'CustomerName'
                },
                {
                    name: 'KharidCount',
                    type: 'int'
                },
                {
                    name: 'ForooshCount' ,
                    type: 'int'
                },
                {
                    name: 'AvailableCount',
                    type: 'int'
                }
            ]
        });

        me.anbarGridStore = Ext.create('Ext.data.Store', {
            id: 'anbargridid',
            model: 'anbarGridModel',
            //autoLoad: true,
            groupField: 'Factor_IDx',
            proxy: {
                type: 'ajax',
                url: 'ExtDesk.php',
                method: 'GET',
                extraParams: {
                    Module: 'Anbar',
                    option: 'anbarKala',
                    action: 'read',
                    goodsId: '',
                    customerId: '',
                    fromDate:'',
                    toDate: '',
                    type: ''
                },
                reader: {
                    type: 'json',
                    root: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                }
            }
        });

        me.doGridSum = function(){
            var sumKharid = 0;
            var sumForoosh = 0;
            var sumAvailable = 0;
            me.anbarGridStore.each(function (rec) {
                sumKharid += rec.get('KharidCount');
            });
            Ext.getCmp(me.gridPrefix+'hesabdari-anbar-input').setValue(sumKharid);

            me.anbarGridStore.each(function (rec) {
                sumForoosh += rec.get('ForooshCount');
            });
            Ext.getCmp(me.gridPrefix+'hesabdari-anbar-output').setValue(sumForoosh);

            me.anbarGridStore.each(function (rec) {
                sumAvailable += rec.get('AvailableCount');
            });
            Ext.getCmp( me.gridPrefix+'hesabdari-anbar-remain').setValue(sumAvailable);


        }

        me.items = [
            {
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                layout:'column',
                region:'north',
                borderWidth:0,
                border: false,
                features: [{
                    ftype: 'summary'
                }],
                items: [
                    {items: [
                        me.kalaComboItem,
                        me.customerComboItem
                    ], columnWidth: 0.5,border:false},
                    {items: [
                        {
                            id: me.gridPrefix + "FromDateId",
                            xtype: 'datefield',
                            plugins: ['jalalidate'],
                            fieldLabel: 'از تاریخ',
                            labelWidth: 50,
                            format: "Y/m/d"

                        },
                        {
                            id: me.gridPrefix + "ToDateId",
                            xtype: 'datefield',
                            plugins: ['jalalidate'],
                            fieldLabel: 'تا تاریخ',
                            labelWidth: 50
                        }
                    ], columnWidth: 0.5,border:false}

                    ,

                  {
                        columnWidth: 0.5
                        ,border:false ,

                        items:[{width:80,
                            xtype: 'button',
                            bodystyle:'margin-right:200px;',
                            text: 'جستجو',
                            formBind: true,
                            handler:function(){
                                me.mythiscombo = Ext.getCmp(me.gridPrefix+'kalacombo');
                                me.mythiscombo = me.mythiscombo.getValue();


                                me.mycustomercombo = Ext.getCmp( me.gridPrefix+'customercombo');
                                var fromDate = Ext.getCmp(me.gridPrefix+'FromDateId').getRawValue();;
                                var toDate = Ext.getCmp(me.gridPrefix+'ToDateId').getRawValue();;
                                if( me.mycustomercombo){
                                me.mycustomercombo = me.mycustomercombo.getValue();

                                    me.anbarGridStore.load({
                                        params:{
                                            fromDate:fromDate,
                                            toDate: toDate,
                                            goodsId : me.mythiscombo ,
                                            customerId: me.mycustomercombo,
                                            type: 'customer'


                                        },
                                        callback: me.doGridSum
                                    });
                                }
                                else
                                {
                                    me.anbarGridStore.load({
                                        params:{
                                            fromDate:fromDate,
                                            toDate: toDate,
                                            goodsId : me.mythiscombo,
                                            type: 'anbar'


                                        } ,
                                        callback: me.doGridSum
                                    });
                                }





                            }


                        }

                        ]

                    }
                ]
            },
            {
                xtype: 'gridpanel',

                title: 'لیست گردش کالا در انبار',
                forceFit: true,
                region:'center',
                store:me.anbarGridStore,
                dockedItems: [
                    {xtype:'toolbar',
                        dock:'top',
                        items:[
                            {
                                xtype: 'button',
                                text: 'چاپ مورد',
                                icon: 'client/ux/grid/gridPrinterCss/printer.png',
                                handler : function(){
                                    var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(me.items.items[1].id);
                                    //if(me.id == 'PazireshGridId')
                                    //    var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(me.id);
                                    // else
                                    //var printContent = Ext.ux.PreparePrint.prepareContentOne(me.id);

                                    if(printContent != null)
                                        window.open('Reports/PrintOne.php?message='+ printContent);
                                }

                            },
                            {
                                xtype: 'button',
                                text: 'چاپ همه',
                                icon: 'client/ux/grid/gridPrinterCss/printer.png',
                                handler : function(){
                                    Ext.ux.grid.Printer.printAutomatically = false;
                                    Ext.ux.grid.Printer.print(me.items.items[1]);
                                }
                            }]



                    },
                    {
                        xtype: 'toolbar',
                        dock: 'bottom',
                        ui: 'footer',

                        layout: {
                            type: 'table',
                            columns: 3
                        },

                        defaults: {bodyStyle: 'text-align:"center"'},
                        items: [
                            {xtype: 'label', text: 'جمع ورود'},
                            {xtype: 'label', text: 'جمع خروج'},
                            {xtype: 'label', text: 'جمع باقیمانده'},

                            { xtype: 'textfield', id: me.gridPrefix+'hesabdari-anbar-input'},
                            { xtype: 'textfield', id: me.gridPrefix+'hesabdari-anbar-output'},
                            { xtype: 'textfield', id: me.gridPrefix+'hesabdari-anbar-remain'}

                        ]
                    }
                ],
                columns: [
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'FactorNo',
                        text: 'شماره فاکتور'
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'FactorDate',
                        text: 'تاریخ فاکتور'
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'CustomerName',
                        text: 'مشتری'
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'KharidCount',
                        text: 'تعداد-ورود'
                    } ,
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'ForooshCount',
                        text: 'تعداد-خروج'/*,
                        summaryType: 'count',
                        summaryRenderer: function(value, summaryData, dataIndex) {
                            return Ext.String.format('{0} student{1}', value, value !== 1 ? 's' : '');
                        }*/
                    },
                    {
                        xtype: 'gridcolumn',
                        dataIndex: 'AvailableCount',
                        text: 'باقیمانده'
                    }

                ],
                viewConfig: {

                }
            }
        ],

        me.saveGrid = function (gridName, url, module, option, action, extraParam) {
            var me = this;
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();
            var modified = store.getNewRecords();
            var updates = store.getUpdatedRecords();
            var params = [];
            Ext.each(modified, function (mod, i) {
                mod.id = 'nuevo',
                    params.push(mod.data);
            });
            Ext.each(updates, function (upd, i) {
                params.push(upd.data);
            });

            if (params.length > 0) {
                var json = Ext.encode(params);
                Ext.Ajax.request({
                    url: url,
                    method: 'GET',
                    params: {
                        module: me.module,
                        option: me.option,
                        action: action,
                        json: json
                    },
                    success: function (response) {
                        var text = response.responseText;
                        if (text != "") {

                            var resp = Ext.decode(text);
                            if (resp.success) {
                                if (extraParam != "") {
                                    grid.getStore().load(extraParam);
                                } else {
                                    grid.getStore().load();
                                }

                            } else {
                                Ext.Msg.alert(me.module, 'save error ' + '<b>' + resp.msg + '</b>')
                            }
                        } else {
                            Ext.MessageBox.hide();
                            Ext.Msg.alert(me.module, 'save error')
                        }

                    }
                }, this);

            } else {

            }

        }

        me.addGrid = function (gridName, record, editor) {

            var me = this;
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();
            var count = grid.getStore().getCount();
            store.insert(0, record);
            editor.startEdit(0, 0);
        }

        me.deleteGrid = function (gridName, url, module, option, action, keyColumn) {
            var me = this;
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];


            if (selection) {

                Ext.Ajax.request({
                    url: url,
                    method: 'GET',
                    params: {
                        id: selection.data[me.keyColumn],
                        module: me.module,
                        option: me.option,
                        action: action
                    },
                    success: function (response) {
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            grid.getStore().load();

                            grid.getStore().remove(selection)
                        } else {

                            Ext.Msg.alert(me.module, this.lang["server_error"] + '<b>' + resp.msg + '</b>')
                        }

                    }
                });
            } else {

            }

        }

        this.callParent();

    }


});
