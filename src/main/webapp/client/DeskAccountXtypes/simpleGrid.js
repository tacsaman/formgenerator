Ext.define('MyDesktop.DeskAccountXtypes.simpleGrid', {
    extend: 'Ext.ux.LiveSearchGridPanel',
    alias: 'widget.simpleGrid',



    initComponent: function () {
        var me = this;

        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.Printer',
            'Ext.ux.RowExpander'
        ]);
        this.initKeyMap();

        me.listeners =  {
            edit: {
                fn: function () {
                    me.saveGrid(me.id, 'ExtDesk.php', me.module, me.option, 'create');
                }
            },
            canceledit: {
                fn: function () {

                    me.store.reload();
                }
            }
        }

        me.dockedItems =  [
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'جدید [Insert]',
                        icon: 'resources/images/add.gif',
                        handler: function () {
                            //console.log(me.store.model);
                            var record = Ext.create(me.store.model, {
                                id: null
                            });
                            editor = me.getPlugin(me.editorId);
                            //console.log(record);
                            me.addGrid(me.id, record, editor, me.firstColToFocus);
                        }
                    },
                    {
                        xtype: 'button',
                        class: "x-btn-icon-el ",
                        text: 'حذف [Delete]',
                        icon: 'resources/images/delete.gif',
                        handler: function () {
                            Ext.Msg.confirm('حذف ورودی', 'آیا مطمئن هستید می خواهید این آیتم را حذف کنید؟ ', function(btn){
                                if (btn == 'yes'){
                                    me.deleteGrid(me.id, 'ExtDesk.php', me.module, me.option, 'destroy', me.keyColumn);
                                }
                            });

                        }
                    },
                    {

                        xtype: 'button',
                        icon: 'resources/images/refresh.png',
                        text: 'بروزرسانی [Alt+r]',
                        handler: function () {

                            me.store.reload();


                        }
                    },
                    {
                        xtype: 'button',
                        text: 'چاپ مورد [Alt+p]',
                        icon: 'client/ux/grid/gridPrinterCss/printer.png',
                        handler : function(){
                             var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(me.id);
                            //if(me.id == 'PazireshGridId')
                            //    var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(me.id);
                           // else
                                //var printContent = Ext.ux.PreparePrint.prepareContentOne(me.id);

                            if(printContent != null)
                                window.open('Reports/PrintOne.php?message='+ printContent);
                         }

                    },
                    {
                        xtype: 'button',
                        text: 'چاپ همه [Alt+Shift+p]',
                        icon: 'client/ux/grid/gridPrinterCss/printer.png',
                        handler : function(){
                            Ext.ux.grid.Printer.printAutomatically = false;
                            Ext.ux.grid.Printer.print(me);
                        }
                    }
                ]
            }
        ]

        me.saveGrid = function (gridName, url, module, option, action, extraParam) {
            //console.log(extraParam);
            var me = this;
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();
            var modified = store.getNewRecords();
            var updates = store.getUpdatedRecords();

            var params = [];
            Ext.each(modified, function (mod, i) {
                if(mod.data.IssueDate != null)
                    mod.data.IssueDate = Ext.Date.format(mod.data.IssueDate, 'Jalali');
                if(mod.data.MaturityDate != null)
                    mod.data.MaturityDate = Ext.Date.format(mod.data.MaturityDate, 'Jalali');

                if(mod.data.OrderDate != null)
                    mod.data.OrderDate = Ext.Date.format(mod.data.OrderDate, 'Jalali');
                if(mod.data.DeliverDate != null)
                    mod.data.DeliverDate = Ext.Date.format(mod.data.DeliverDate, 'Jalali');

                if(mod.data.purchasedate != null)
                    mod.data.purchasedate = Ext.Date.format(mod.data.purchasedate, 'Jalali');

                mod.id = 'nuevo',
                    params.push(mod.data);
            });
            Ext.each(updates, function (upd, i) {
                if(upd.data.IssueDate != null)
                    upd.data.IssueDate = Ext.Date.format(upd.data.IssueDate, 'Jalali');
                if(upd.data.MaturityDate != null)
                    upd.data.MaturityDate = Ext.Date.format(upd.data.MaturityDate, 'Jalali');

                if(upd.data.OrderDate != null)
                    upd.data.OrderDate = Ext.Date.format(upd.data.OrderDate, 'Jalali');
                if(upd.data.DeliverDate != null)
                    upd.data.DeliverDate = Ext.Date.format(upd.data.DeliverDate, 'Jalali');

                if(upd.data.purchasedate != null)
                    upd.data.purchasedate = Ext.Date.format(upd.data.purchasedate, 'Jalali');

                params.push(upd.data);
            });
            //params.push(Ext.getCmp(gridName).getSelectionModel().selected.items[0].data);
            //console.log(params);

            if (params.length > 0) {
                var json = Ext.encode(params);
                Ext.Ajax.request({
                    url: url,
                    method: 'GET',
                    params: {
                        Module: me.module,
                        option: me.option,
                        action: action,
                        json: json,
                        CheckType: me.CheckType,
                        Type: me.Type,
                        branch_id: company.branch_id
                    },
                    success: function (response) {
                        me.store.reload();
                        if(company.newPerson){
                            company.branchPersonStore.reload();
                            company.newPerson = false;
                        }
                        if(company.newProduct){
                            company.branchProductStore.reload();
                            company.newProduct = false;
                        }
                        //Ext.Msg.alert('Status', response.responseText);
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: response.responseText
                        }).show();
                        var text = response.responseText;
                        if (text != "") {

                            var resp = Ext.decode(text);
                            if (resp.success) {

                                if (extraParam != "") {
                                    grid.getStore().load(extraParam);
                                } else {
                                    grid.getStore().load();
                                }

                            } else {
                                Ext.Msg.alert(me.module, 'save error ' + '<b>' + resp.msg + '</b>')
                            }
                        } else {
                            Ext.MessageBox.hide();
                            Ext.Msg.alert(me.module, 'save error')
                        }

                    }
                }, this);

            } else {

            }

        }

        me.addGrid= function (gridName, record, editor, firstColToFocus) {
            var me = this;
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();

            //in this for, i set the current time in fields that its 'fillDate' config is true
            for(var i = 0; i < editor.cmp.columns.length; i++){
                if(editor.cmp.columns[i].initialConfig.editor != null){
                    //alert(i);
                    if(editor.cmp.columns[i].initialConfig.editor.fillDate != null)
                        record.data[editor.cmp.columns[i].dataIndex] = Ext.Date.format(new Date(), 'Jalali');
                }
            }
            //end for
            store.insert(0, record);
            editor.startEdit(0, 0);
            //alert(firstColToFocus);
            Ext.getCmp(firstColToFocus).focus();
            /*setTimeout(function(){
                Ext.getCmp(firstColToFocus).focus();
            },100);*/
        }

        me.deleteGrid =  function (gridName, url, module, option, action, keyColumn) {
            var me = this;
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];


            if (selection) {
                Ext.Ajax.request({
                    url: url,
                    method: 'GET',
                    params: {
                        id: selection.data[me.keyColumn],
                        Module: me.module,
                        option: me.option,
                        action: action,
                        OrderID: selection.data[me.keyColumn1],
                        RepairmanOrderID: selection.data[me.keyColumn2]
                    },
                    success: function (response) {
                        me.store.reload();
                        //Ext.Msg.alert('Status', response.responseText);
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 130,
                            iconCls: 'ux-notification-icon-information',
                            html: response.responseText
                        }).show();
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            grid.getStore().load();

                            grid.getStore().remove(selection)
                        } else {

                            Ext.Msg.alert(me.module, this.lang["server_error"] + '<b>' + resp.msg + '</b>')
                        }

                    }
                });
            } else {

            }

        }



        this.callParent();


        /*me.preparePrint = function(gridName){
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            //console.log(grid.getView().getGridColumns());
            //console.log(selection.data['StaffCode']);
            //alert(selection.data['StaffCode']);
            //console.log(grid.columns[1].dataIndex);
            var message ="";
                var table = '<div><table border="1" style="direction: rtl;">\n';
                var header = '<tr>';
                 for(var i = 0; i < grid.columns.length; i++){
                     if(!grid.columns[i].hidden){
                         header += '<td>';
                         header += grid.columns[i].text;
                         header += '</td>';
                     }
                 }
                header += '</tr>\n';
                var tbody = '<tr>';
                for(var i = 0; i < grid.columns.length; i++){
                    if(!grid.columns[i].hidden){
                        tbody += '<td>';
                        tbody += selection.data[grid.columns[i].dataIndex];
                        tbody += '</td>';
                    }
                }
            tbody += '</tr>\n';
            var footer = '</table></div>';
            message = table + header + tbody + footer;
            window.open('PrintOne.php?message='+ message);
        }*/

    },

    initKeyMap: function() {
        //listen for presses to the 'Insert' key, so call addGrid function
        this.keymap = new Ext.KeyMap(document, {
            key  : 45,
            id: 'simpleGrid-Insert',
            scope: this,
            fn   : function() {
                var record = Ext.create(this.store.model, {
                    id: null
                });
                editor = this.getPlugin(this.editorId);
                this.addGrid(this.id, record, editor, this.firstColToFocus);
            }
        });

        //listen for presses to the 'Delete' key, so call deleteGrid function
        this.keymap = new Ext.KeyMap(document, {
            key  : 46,
            id: 'simpleGrid-Delete',
            scope: this,
            fn   : function() {
                Ext.Msg.confirm('حذف ورودی', 'آیا مطمئن هستید می خواهید این آیتم را حذف کنید؟ ', function(btn){
                    if (btn == 'yes'){
                        this.deleteGrid(this.id, 'ExtDesk.php', this.module, this.option, 'destroy', this.keyColumn);
                    }
                });
            }
        });

        //listen for presses to the 'Alt+r' key, so call reload function
        this.keymap = new Ext.KeyMap(document, {
            key  : 82,
            id: 'simpleGrid-Reload',
            alt: true,
            scope: this,
            fn   : function() {
                this.store.reload();
            }
        });

        //listen for presses to the 'Alt + P' key, so call Print function
        this.keymap = new Ext.KeyMap(document, {
            key  : 80,
            id: 'simpleGrid-Print',
            alt:true,
            scope: this,
            fn   : function() {
                var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(this.id);
                //if(me.id == 'PazireshGridId')
                //    var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(me.id);
                // else
                //var printContent = Ext.ux.PreparePrint.prepareContentOne(me.id);

                if(printContent != null)
                    window.open('Reports/PrintOne.php?message='+ printContent);
            }
        });

        //listen for presses to the 'Alt + Shift + P' key, so call PrintAll function
        this.keymap = new Ext.KeyMap(document, {
            key  : 80,
            id: 'simpleGrid-PrintAll',
            alt: true,
            shift: true,
            scope: this,
            fn   : function() {
                Ext.ux.grid.Printer.printAutomatically = false;
                Ext.ux.grid.Printer.print(this);
            }
        });

        //listen for presses to the 'F2' key, so search in page
        this.keymap = new Ext.KeyMap(document, {
            key  : 113,
            id: 'simpleGrid-Search',
            scope: this,
            fn   : function() {
                Ext.getCmp('searchField').focus();
            }
        });


        //add a binding for each of our shortcut keys
        /*Ext.iterate(navKeys, function(key, tabId) {
            this.keymap.addBinding({
                key  : key,
                scope: this,
                fn   : function() {
                    if (this.navigateOnNextKey) {
                        this.tabs.setActiveTab(tabId);
                        this.navigateOnNextKey = false;
                    }
                }
            });
        }, this);*/
    }



});
