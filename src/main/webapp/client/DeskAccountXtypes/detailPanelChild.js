Ext.define('MyDesktop.DeskAccountXtypes.detailPanelChild', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.detailPanelChild',

    initComponent: function () {
        var me = this;
        Ext.Loader.setConfig({
            enabled: true,
            paths: {
                Ext: '.',
                'Ext.ux': 'client/ux'
            }
        });

        Ext.require([
            'Ext.grid.*',
            'Ext.data.*',
            'Ext.ux.grid.Printer',
            'Ext.ux.RowExpander'
        ]);

        me.dockedItems =  [
            {
                xtype: 'toolbar',
                dock: 'top',
                height: 30,
                items: [
                    {
                        xtype: 'label',
                        text: 'نام شرکت :'
                    },
                    {
                      xtype: 'label',
                      text: '----',
                      id: 'lblCompanyName',
                      padding: '0 20 0 0'
                    }/*,
                    {
                        xtype: 'label',
                        text: 'نام مشتری: '
                    },
                    {
                        xtype: 'label',
                        text: '----',
                        id: 'lblCustomer_' + me.id,
                        padding: '0 20 0 0'
                    },
                    {
                        xtype: 'label',
                        text: 'تاریخ: '
                    },
                    {
                        xtype: 'label',
                        text: '----',
                        id: 'lblFactorDate_' + me.id,
                        padding: '0 20 0 0'
                    }*/
                ]
            },
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'جدید',
                        icon: 'resources/images/add.gif',
                        id: 'btnNew_' + me.id,
                        disabled: true,
                        handler: function () {
                            //this.setDisabled(true);
                            if(company.editable == true){
                                var record = Ext.create('companyDetailsModel', {
                                    company_id: company.company_id
                                });
                                company_id: company.company_id;
                                me.addGrid(me.id, record, me.editorId);
                            }

                        }
                    },
                    {
                        xtype: 'button',
                        text: 'حذف',
                        icon: 'resources/images/delete.gif',
                        id: 'btnDelete_' + me.id,
                        disabled: true,
                        handler: function (){
                            if(company.editable == true){
                                Ext.Msg.confirm('حذف ورودی', 'آیا مطمئنید می خواهید این گزینه را حذف کنید ؟', function(btn){
                                    if (btn == 'yes'){
                                        me.deleteDetails(me.id, 'ExtDesk.php', me.module, 'details', 'destroy', me.parentId);
                                    }
                                });
                            }

                        }
                    },
                    {

                        xtype: 'button',
                        icon: 'resources/images/refresh.png',
                        text: 'به روز رسانی',
                        id: 'btnUpdate_detail',
                        disabled: true,
                        handler: function () {
                            company.loadDetails(me.parentId, me.id);

                        }
                    },
                    {
                        xtype: 'button',
                        text: 'چاپ',
                        id: 'btnPrint_detail',
                        disabled: true,
                        icon: 'client/ux/grid/gridPrinterCss/printer.png',
                        handler : function(){
                            Ext.ux.grid.Printer.printAutomatically = false;
                            Ext.ux.grid.Printer.print(me);
                        }
                    },
                    '-',
                    {
                        xtype: 'button',
                        text: 'لیست محصولات',
                        id: 'btnProducts_detail',
                        disabled: true,
                        icon: 'resources/images/products.png',
                        handler : function(){
                            company.createProductWindow(me.id);
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'لیست افراد',
                        id: 'btnPersons_detail',
                        disabled: true,
                        icon: 'resources/images/person.png',
                        handler : function(){
                            company.createPersonWindow(me.id);
                        }
                    }

                ]
            }

        ];

        me.listeners=  {

            edit: {
                fn: function () {
                    //console.log(me.parentId,me.id);

                    me.saveGrid(me.id, 'ExtDesk.php', me.module, me.option, 'create', me.type);
                }
            },
            canceledit: {
                fn: function () {
                    company.loadDetails(me.parentId, me.id);
                }
            }
        }

       me.deleteDetails =  function (gridName, url, module, option, action,sourceGrid) {
            var grid = Ext.getCmp(gridName);

            var selection = grid.getView().getSelectionModel().getSelection()[0];
           console.log(selection);
            loadObject = {
                id: selection.data.id,
                Module: module,
                option: option,
                action: action
            };

            if (selection) {

                Ext.Ajax.request({
                    url: url,
                    method: 'GET',

                    params: {
                        id: selection.data.id,
                        Module: module,
                        option: option,
                        action: action
                    },
                    success: function (response) {
                        company.loadDetails(sourceGrid, gridName);
                        //Ext.Msg.alert('Status', response.responseText);
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: response.responseText
                        }).show();
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            company.loadDetails(sourceGrid, gridName);

                            grid.getStore().remove(selection)
                        } else {

                            Ext.Msg.alert(module, this.lang["server_error"] + '<b>' + resp.msg + '</b>')
                        }

                    }
                });
            } else {

            }

        }

       me.saveGrid =function (gridName, url, module, option, action, extraParam) {
            var me = this;
            var grid = Ext.getCmp(gridName);
           //alert(Ext.getCmp(gridName).getSelectionModel().selected.items[0].data.FactorDetail_ID);
            var store = grid.getStore();
            /*var modified = store.getNewRecords();
                //alert("MM:" + modified);
            var updates = store.getUpdatedRecords();
                //alert("Up:" + updates);
            var params = [];
            Ext.each(modified, function (mod, i) {
                console.log(mod.data);
                mod.id = 'nuevo',
                    params.push(mod.data);
            });
            Ext.each(updates, function (upd, i) {
                params.push(upd.data);
            });*/
           var params = [];
           params.push(Ext.getCmp(gridName).getSelectionModel().selected.items[0].data);
           params[0].tbl_companyinfo_id = company.company_id;
            if (params.length > 0) {
                var json = Ext.encode(params);
                //alert(module + "++" + option + "++" + action);
                Ext.Ajax.request({
                    url: url,
                    method: 'GET',
                    params: {
                        Module: module,
                        option: option,
                        action: action,
                        json: json
                        //type: extraParam
                    },
                    success: function (response) {
                        company.loadDetails(me.parentId, me.id);

                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: response.responseText
                        }).show();
                        var text = response.responseText;
                        if (text != "") {

                            var resp = Ext.decode(text);
                            if (resp.success) {
                                if (extraParam != "") {
                                    company.loadDetails(me.parentId, me.id);
                                } else {
                                    grid.getStore().reload();
                                }

                            } else {
                                Ext.Msg.alert(module, 'save error ' + '<b>' + resp.msg + '</b>')
                            }
                        } else {
                            Ext.MessageBox.hide();
                            Ext.Msg.alert(module, 'save error');

                        }

                    }
                }, this);

            } else {

            }

        }

       me.addGrid = function (gridName, record, editor) {
            var editedGrid = Ext.getCmp(gridName);
            editor = editedGrid.getPlugin(editor);
            var me = this;
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();
            //var count = grid.getStore().getCount();
            store.insert(0, record);

           //editor.cancelEdit ();
           var r = store.getAt (0);

            editor.startEdit(r, 0);

        }

       me.deleteGrid= function (gridName, url, module, option, action, keyColumn) {
            var me = this;
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            console.log(selection.data.keyColumn);
            if (selection) {

                Ext.Ajax.request({
                    url: url,
                    method: 'GET',
                    params: {
                        id: selection.data[keyColumn],
                        Module: module,
                        option: option,
                        action: action
                    },
                    success: function (response) {
                        //me.store.reload();
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            grid.getStore().load();

                            grid.getStore().remove(selection)
                        } else {

                            Ext.Msg.alert(module, this.lang["server_error"] + '<b>' + resp.msg + '</b>')
                        }

                    }
                });
            } else {

            }

        }



       this.callParent();
    }







});
