/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */

Ext.define('MyDesktop.DeskAccountXtypes.bankCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.bankCombo',

    initComponent: function () {
        var me = this;
        me.store =  Ext.create('Ext.data.Store', {

            model: 'bankListModel',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: 'ExtDesk.php',
                method: 'GET',
                extraParams: {
                    Module: 'Hidden',
                    option: 'bankList',
                    action: 'read',
                    type: me.type

                },
                reader: {
                    type: 'json',
                    root: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                }
            }
        });

        me.displayField =  "BankInfo";
        me.valueField = "BankID";
        me.typeAhead = true;
        me.minChars = 1;
        me.forceSelection = true;
        me.selectOnFocus = true;


        this.callParent();

    }


});
