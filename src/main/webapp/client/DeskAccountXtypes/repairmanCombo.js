/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */

Ext.define('MyDesktop.DeskAccountXtypes.repairmanCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.repairmanCombo',

    initComponent: function () {
        var me = this;
        Ext.define('repairmanListModel', {
            extend: 'Ext.data.Model',

            fields: [{
                name: 'repairmanID'
            }, {
                name: 'repairmanName'
            }]
        });
        me.store =  Ext.create('Ext.data.Store', {

            model: 'repairmanListModel',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: 'ExtDesk.php',
                method: 'GET',
                extraParams: {
                    Module: 'Repairman',
                    option: 'repairmanCombo',
                    action: 'read'
                },
                reader: {
                    type: 'json',
                    root: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                }
            }
        });

        me.displayField =  "repairmanName";
        me.valueField = "repairmanID";
        me.typeAhead = true;
        me.minChars = 2;
        me.allowBlank = false;
        //me.selectedIndex = 0;// = me.store.getAt(0);


        this.callParent();

    }


});