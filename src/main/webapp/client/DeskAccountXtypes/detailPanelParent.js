Ext.define('MyDesktop.DeskAccountXtypes.detailPanelParent', {
    extend: 'Ext.grid.Panel',
    extend: 'Ext.ux.LiveSearchGridPanel',
    alias: 'widget.detailPanelParent',

    initComponent: function () {

        var me = this;
        //alert("parent " + me.id);
        var module=me.module;
        var option = me.option;
        var action = me.action;

        this.initKeyMap();

        me.deleteGrid =  function (gridName, url, module, option, action) {
            var me = this;

            var grid = Ext.getCmp(gridName);

            var selection = grid.getView().getSelectionModel().getSelection()[0];


            //alert(me.module + '--' + me.option + '--' + action);
            loadObject = {
                id: selection.data.id,
                Module: me.module,
                option: me.option,
                action: action
            };
            if (selection) {

                Ext.Ajax.request({
                    url: url,
                    method: 'GET',

                    params: {
                        id: selection.data.id,
                        Module: me.module,
                        option: me.option,
                        action: action
                    },
                    success: function (response) {
                        me.store.reload();
                        //Ext.Msg.alert('Status', response.responseText);
                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 120,
                            iconCls: 'ux-notification-icon-information',
                            html: response.responseText
                        }).show();
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            grid.getStore().load(loadObject);

                            grid.getStore().remove(selection)
                        } else {

                            Ext.Msg.alert(module, this.lang["server_error"] + '<b>' + resp.msg + '</b>')
                        }

                    }
                });
            } else {

            }

        }

        me.dockedItems =  [
            {
                xtype: 'pagingtoolbar',
                dock: 'bottom',
                displayInfo: true,
                store: this.store
            },

            {
                xtype: 'toolbar',
                dock: 'top',
                items: [

                    {
                        xtype: 'button',
                        text: 'جدید [Insert]',
                        icon: 'resources/images/add.gif',
                        handler: function () {
                            me.newhandler();
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'حذف',
                        icon: 'resources/images/delete.gif',
                        handler: function () {
                            Ext.Msg.confirm('حذف ورودی', 'آیا مطمئنید می خواهید این گزینه را حذف کنید ؟', function(btn){
                                if (btn == 'yes'){
                                    me.deleteGrid(me.id, 'ExtDesk.php', module, option, 'destroy');
                                }
                            });


                        }
                    },
                    {

                        xtype: 'button',
                        icon: 'resources/images/refresh.png',
                        text: 'بروزرسانی [Alt+r]',
                        handler: function () {
                            me.store.reload();
                        }
                    } ,
                    {
                        xtype: 'button',
                        text: 'نمایش همه',
                        handler: function () {

                            me.store.load();

                        }

                    }

                ]
            },
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        text: 'چاپ',
                        icon: 'client/ux/grid/gridPrinterCss/printer.png',
                        handler : function(){
                            var printContent = Ext.ux.PreparePrint.prepareContentFactor(me.id);
                            //if(me.id == 'PazireshGridId')
                            //    var printContent = Ext.ux.PreparePrint.prepareContentPaziresh(me.id);
                            // else
                            //var printContent = Ext.ux.PreparePrint.prepareContentOne(me.id);

                            if(printContent != null)
                                window.open('Reports/PrintOne.php?message='+ printContent);
                        }

                    }

                ]
            }

        ]

        me.saveGrid = function (gridName, url, module, option, action, extraParam, sourceGrid) {
            var me = this;
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();
            var modified = store.getNewRecords();
            var updates = store.getUpdatedRecords();
            var params = [];

            Ext.each(modified, function (mod, i) {
                //mod.data.FactorDate = Ext.Date.format(mod.data.FactorDate, 'Jalali');

                mod.id = 'nuevo',
                    params.push(mod.data);
            });
            Ext.each(updates, function (upd, i) {
                //upd.data.FactorDate = Ext.Date.format(upd.data.FactorDate, 'Jalali');

                params.push(upd.data);
            });
            console.log(params);
            /*var row = Ext.getCmp(gridName).getSelectionModel().selected.items[0].data;
            console.log(row);
            row.FactorDate = Ext.Date.format(row.FactorDate, 'Jalali');
            row.MaturityDate = Ext.Date.format(row.MaturityDate, 'Jalali');
            params.push(row);
            console.log(params);*/

            if (params.length > 0) {
                var json = Ext.encode(params);
                //console.log("3-" + json);
                Ext.Ajax.request({
                    url: url,
                    method: 'GET',

                    params: {
                        type: me.type,
                        Module: me.module,
                        option: me.option,
                        action: action,
                        json: json

                    },
                    success: function (response) {
                        me.store.reload({
                            callback: function(records, operation, success) {
                                var value = me.store.first();
                                var rec = me.store.findRecord("id", value.data.id);   // find the record with the unique value "name" after sync
                                grid.getSelectionModel().select(rec);
                                me.loadDetails(me.id, me.childId);
                            }
                        });

                        /*setTimeout(function(){
                            var index = me.id.lastIndexOf("Id");
                            var btnNewID = "btnNew_" + me.id.substring(0, index) + "DetailsId";
                            var btnNew = Ext.getCmp(btnNewID);
                            btnNew.fireHandler();
                            Ext.getCmp('factor-StaffName').focus();
                        },1000);*/

                        Ext.create('widget.uxNotification', {
                            title: 'وضعیت',
                            corner: 'tr',
                            stickOnClick: false,
                            manager: 'demo1',
                            width: 250,
                            height: 100,
                            iconCls: 'ux-notification-icon-information',
                            html: response.responseText
                        }).show();
                        var text = response.responseText;
                        if (text != "") {
                            var resp = Ext.decode(text);

                            if (resp.success) {

                                if (sourceGrid) {

                                    company.loadDetails(sourceGrid, gridName);
                                } else {
                                    grid.getStore().reload();
                                }

                            } else {
                                Ext.Msg.alert(module, 'save error ' + '<b>' + resp.msg + '</b>')
                            }
                        } else {
                            Ext.MessageBox.hide();
                            Ext.Msg.alert(module, 'save error')
                        }
                    }
                }, this);

            } else {

            }

        }

        me.listeners =   {
            click: {
                element: 'el',
                    fn: function () {
                    me.loadDetails(me.id, me.childId);


                }
            },
            edit: {
                fn: function () {
                    me.saveGrid(me.id, 'ExtDesk.php', me.module, option, 'create', me.type);

                }
            },
            canceledit: {
                fn: function () {
                    me.store.reload();
                }
            }
        }

        me.newhandler = function(){
            var record = Ext.create('companyModel');
            grid = Ext.getCmp(me.id);
            editor = grid.getPlugin(me.editorId);
            me.addGrid(me.id, record, editor, me.firstColToFocus);
        }

        me.addGrid =  function (gridName, record, editor, firstColToFocus) {
            var grid = Ext.getCmp(gridName);
            var store = grid.getStore();

            for(var i = 0; i < editor.cmp.columns.length; i++){
                if(editor.cmp.columns[i].initialConfig.editor != null){
                    //alert(i);
                    if(editor.cmp.columns[i].initialConfig.editor.fillDate != null)
                        record.data[editor.cmp.columns[i].dataIndex] = Ext.Date.format(new Date(), 'Jalali');
                }
            }

            var count = grid.getStore().getCount();
            store.insert(0, record);
            editor.startEdit(0, 0);
            Ext.getCmp(firstColToFocus).focus();
        }

        me.loadDetails = function (sourceGrid, targetGrid) {

            company_id = Ext.getCmp(sourceGrid).getSelectionModel().selected.items[0].data.id;
            //MaxId = Ext.getCmp(sourceGrid).getSelectionModel().selected.items[0].data.MaxID;
            var id = this.id;
            //alert(factorId);
            var index = id.lastIndexOf("Id");
            var btnNewID = "btnNew_" + id.substring(0, index) + "DetailsId";
            var btnDeleteID = "btnDelete_" + id.substring(0, index) + "DetailsId";

            /*var lblFactorNoID = "lblCompanyName";
            var lblFactorDateID = "lblFactorDate_" + id.substring(0, index) + "DetailsId";
            var lblCustomerID = "lblCustomer_" + id.substring(0, index) + "DetailsId";*/

            //alert(lblFactorNoID);

            Ext.getCmp('lblCompanyName').setText(Ext.getCmp(sourceGrid).getSelectionModel().selected.items[0].data.name);
            /*Ext.getCmp(lblFactorDateID).setText(Ext.getCmp(sourceGrid).getSelectionModel().selected.items[0].data.FactorDate);
            Ext.getCmp(lblCustomerID).setText(Ext.getCmp(sourceGrid).getSelectionModel().selected.items[0].data.CustomerName);*/



            var btnNew = Ext.getCmp(btnNewID);
            var btnDelete = Ext.getCmp(btnDeleteID);
            var btnUpdate = Ext.getCmp('btnUpdate_detail');
            var btnPrint = Ext.getCmp('btnPrint_detail');
            var btnProduct = Ext.getCmp('btnProducts_detail');
            var btnPerson = Ext.getCmp('btnPersons_detail');

            var editable;
            /*if(factorId == MaxId)
                editable =  true;
            else
                editable =  false;*/


            editable =  true;
            if(editable == true){
                //alert(btnNew.text);
                btnNew.enable(true);
                btnDelete.enable(true);
                btnUpdate.enable(true);
                btnPrint.enable(true);
                btnProduct.enable(true);
                btnPerson.enable(true);

            }
            else{
                //alert('d');
                btnNew.setDisabled(true);
                btnDelete.setDisabled(true);
                btnUpdate.setDisabled(true);
                btnPrint.setDisabled(true);
                btnProduct.setDisabled(true);
                btnPerson.setDisabled(true);
            }



            var grid = Ext.getCmp(targetGrid);
            company.company_id = company_id;
            //company.editable = editable;
            company.editable = true;

            var me = this;
            if (company_id) {
                Ext.Ajax.request({
                    url: 'ExtDesk.php',
                    method: 'GET',

                    params: {
                        company_id: company_id,
                        //editable: editable,
                        editable: true,
                        Module: 'Company',
                        option: 'details',
                        action: 'list'
                    },
                    success: function (response) {
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            grid.getStore().loadData(resp.data);
                        } else {

                        }

                    }
                });
            } else {

            }

        }

        deleteDetails = function (gridName, url, module, option, action, sourceGrid) {
            var me = this;

            var grid = Ext.getCmp(gridName);

            var selection = grid.getView().getSelectionModel().getSelection()[0];
            loadObject = {
                id: selection.data.FactorDetail_ID,
                Module: module,
                option: option,
                action: action
            };

            if (selection) {

                Ext.Ajax.request({
                    url: url,
                    method: 'GET',

                    params: {
                        id: selection.data.FactorDetail_ID,
                        Module: me.module,
                        option: me.option,
                        action: action
                    },
                    success: function (response) {
                        var text = response.responseText;
                        var resp = Ext.decode(text);
                        if (resp.success) {
                            company.loadDetails(sourceGrid, gridName);

                            grid.getStore().remove(selection)
                        } else {

                            Ext.Msg.alert(module, this.lang["server_error"] + '<b>' + resp.msg + '</b>')
                        }

                    }
                });
            } else {

            }

        }

        this.callParent();


    },

    initKeyMap: function() {
        //listen for presses to the 'Insert' key, so call addGrid function
        this.keymap = new Ext.KeyMap(document, {
            key  : 45,
            id: 'panelParent-Insert',
            scope: this,
            fn   : function() {
                this.newhandler();
            }
        });

        //listen for presses to the 'Delete' key, so call deleteGrid function
        /*this.keymap = new Ext.KeyMap(document, {
            key  : 46,
            id: 'panelParent-Delete',
            scope: this,
            fn   : function() {
                Ext.Msg.confirm('حذف ورودی', 'آیا مطمئنید می خواهید این گزینه را حذف کنید ؟', function(btn){
                    if (btn == 'yes'){
                        this.deleteGrid(this.id, 'ExtDesk.php', module, option, 'destroy');
                    }
                });
            }
        });*/

        //listen for presses to the 'Alt+r' key, so call reload function
        this.keymap = new Ext.KeyMap(document, {
            key  : 82,
            id: 'panelParent-Reload',
            alt: true,
            scope: this,
            fn   : function() {
                this.store.reload();
            }
        });

        //listen for presses to the 'Alt + p' key, so call Print function
        /*this.keymap = new Ext.KeyMap(document, {
            key  : 80,
            alt:true,
            scope: this,
            fn   : function() {
                var printContent = Ext.ux.PreparePrint.prepareContentFactor(this.id);
                if(printContent != null)
                    window.open('Reports/PrintOne.php?message='+ printContent);
            }
        });*/

        //listen for presses to the 'Alt+c' key, so search in page
        /*this.keymap = new Ext.KeyMap(document, {
            key  : 67,
            alt:true,
            scope: this,
            fn   : function() {
                alert(this.id);
                pardakhtM.loadModule(this.id);
            }
        });*/

        //listen for presses to the 'F2' key, so search in page
        this.keymap = new Ext.KeyMap(document, {
            key  : 113,
            id: 'panelParent-Search',
            scope: this,
            fn   : function() {
                Ext.getCmp('searchField').focus();
            }
        });


        //add a binding for each of our shortcut keys
        /*Ext.iterate(navKeys, function(key, tabId) {
         this.keymap.addBinding({
         key  : key,
         scope: this,
         fn   : function() {
         if (this.navigateOnNextKey) {
         this.tabs.setActiveTab(tabId);
         this.navigateOnNextKey = false;
         }
         }
         });
         }, this);*/
    }



});
