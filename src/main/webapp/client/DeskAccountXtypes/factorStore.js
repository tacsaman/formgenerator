/**
 * Created with JetBrains WebStorm.
 * User: root
 * Date: 7/1/13
 * Time: 12:20 PM
 * To change this template use File | Settings | File Templates.
 */


Ext.define('MyDesktop.DeskAccountXtypes.factorStore', {
    extend: 'Ext.data.Store',
    alias: 'widget.factorStore',

    initComponent: function () {
        var me = this;
        Ext.define('factorModel', {
            extend: 'Ext.data.Model',

            fields: [{
                name: 'Factor_ID'
            }, {
                name: 'Factor_Number'
            }, {
                name: 'Factor_Date'
            }, {
                name: 'CustomerName'
            }, {
                name: 'Customer_IDx'
            }, {
                name: 'Type'
            }, {
                name: 'Perfect_Date'
            }]
        });
        me.model = 'factorModel';
        me.autoLoad = true;


        me.proxy = {
            type: 'ajax',
            url: 'ExtDesk.php',
            method: 'GET',

            extraParams: {
                Module: 'Factor',
                option: 'get',
                action: 'kharid'

            },
            reader: {
                type: 'json',
                root: 'data',
                successProperty: 'success'
            }
        }
        me.displayField = "CustomerName";
        me.valueField = "Customer_ID";
        me.typeAhead = true;


        this.callParent();

    }


});
