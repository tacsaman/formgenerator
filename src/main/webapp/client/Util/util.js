Ext.define('MyDesktop.Util.util',{

	callBack: null,
	url: '',
	data: null,
	cfg: null,
	form: null,

	constructor: function(config) {
		var me=this;
		cfg = Ext.apply({}, Ext.clone(config));
		cfg.items = cfg.items || [];
	},
	returnTreeObject:  function (val,ptn){
		v = val;p=ptn;
		console.log(val)
		console.log(ptn)
		var ii = ptn.indexOf(".");
		console.log(ii)
		if (ii <0)
			return val[ptn];
		var parentPTN = ptn.substring(0,ii);
		var childPTN = ptn.substring(ii+1,ptn.length);
		var parent = val[parentPTN];
		return this.returnTreeObject(parent,childPTN);		
	},
	returnJalaliDate:  function (date){
		if (date) {
			date = new Date(date);
			date=  Ext.Date.JalaliConverter.gregorianToJalali([date.getFullYear(), date.getMonth() + 1, date.getDate()])
			return (date[0] + "/" + date[1] + "/"+date[2]);
		}else return "";
	},
	returnId:  function (listStore,value){
		if (value && isNaN(value)){
			for (var i = 0; i < listStore.getCount(); i++) {
				record = listStore.getAt(i);
				if ((value == record.get("id")) || (value == record.get("name")))
					return record.get("id");
			}
		}
		else 
			return value;		
	},
	showNotification:function(msg)
	{
		Ext.create('widget.uxNotification', {
			title: 'وضعیت',
			corner: 'tr',
			stickOnClick: false,
			width: 250,
			height: 100,
			iconCls: 'ux-notification-icon-information',
			html: msg
		}).show();
	},
	sendRequest: function(){
		var box = Ext.MessageBox.wait('Please wait...', 'ارسال به سمت سرور');
		var me = this;
		Ext.Ajax.request({
			headers: { 'Content-Type': 'application/json' },
			url:cfg.url,
			params:Ext.encode(cfg.data),
			success:function(form,action){
				var err=Ext.decode(form.responseText);

				if(err.errorMessage){
					box.hide();
					me.showNotification(err.errorMessage)
				}else{
					cfg.callBack.call();
					box.hide();
			}
			},
			failure:function(form,action){
				box.hide();
			}
		});
	},


	sendForm: function(){
		var box = Ext.MessageBox.wait('Please wait...', 'Saving');
		Ext.Ajax.request({
			url: cfg.url,
                headers: { 'Content-Type': 'application/json' },

			params:Ext.encode(cfg.form.getValues()),
			success:function(form,action){
				box.hide();
				var err=Ext.decode(form.responseText);
				if(err.success){
					cfg.callBack.call();
				}else if(err.errors !=undefined){
					Ext.Msg.alert("Error",err.errors);
				}
				else if(action.result.errors != undefined){
					Ext.Msg.alert("Error",action.result.errors);
				}else{
					Ext.Msg.alert("Error","some error occured");
				}
			},
			
			failure:function(form,action){
				var err=Ext.decode(form.responseText);
				Ext.Msg.alert("Error",action.result.errors);
				box.hide();
			}
		});
	},
	
	popUpWindow: function(signatureNum){
		me = this;
	    //chart = Ext.ComponentQuery.query('#morteza')[0];
		merge = {
				"reportName": "Signature",
				"reportParams": [signatureNum]
		}
		
		var ajax = Ext.Ajax.request({
			headers: {
                'Content-Type': 'application/json'
			},
			url:Ext.ux.preURL+'cxf/rest/report/fetchReportData',
			method: 'POST',
			
			//async: false,
			params: Ext.encode(merge),
			success: function(response) {
				decoded = Ext.decode(response.responseText);
				
				var error = '0';
				var signature = '';
				if(decoded.data.length == 0)
					 error = '1';
				 if(error == '0')
					 signature = decoded.data[0].signature;
				 if(signature == '')
					 error = '1';
				 if(error == '0'){
					 Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
					 var mypanel= Ext.create('Ext.window.Window', {
					    title: 'Signature Discription',
					    id : 'win',
					    width: 450,
					    height: 400,
				        plain: true,
				        floating: true,
				        closable : true,
				        autoScroll: true,
				       
				        html: '<div dir=ltr align="justify"><p align="center"><b>Signature Number    '+ decoded.data[0].signature+' </b>'
					    +  '<p><b>Message : </b>'+decoded.data[0].DefaultMessage+'</p>'
					    +  '<p><b>ParsIPS Default Config : </b> <br>Status is '+decoded.data[0].DefaultStatus+'.<br>Action is '+decoded.data[0].DefaultAction+'.<br>Log is '+decoded.data[0].DefaultLog+'</p>'
					    +  '<p><b>Impact : </b>'+decoded.data[0].impact+'</p>'
					    +  '<p><b>\nDetail Information : </b>'+decoded.data[0].info+'</p>'
					    +  '<p><b>\nAffected Systems : </b>'+decoded.data[0].affected+'</p>'
					    +  '<p><b>\nAttack Scenario : </b>'+decoded.data[0].attack+'</p>'
					    +  '<p><b>\nEase of Attack : </b>'+decoded.data[0].easeofattack+'</p>'
					    +  '<p><b>\nFalse Positive : </b>'+decoded.data[0].falsepositive+'</p>'
					    +  '<p><b>\nFalse Negative : </b>'+decoded.data[0].falsenegative+'</p>'
					    +  '<p><b>\nCorrective Action : </b>'+decoded.data[0].correctiveaction+'</p>'
					    +  '<p><b>\nContributor : </b>'+decoded.data[0].contribute+'</p>'
					    +  '<p><b>\nReferense : </b>'+decoded.data[0].referense+'</p><div>',
					    renderTo: document.body
					}).show();
				 }
				 if(error == '1'){
					 Ext.WindowManager.each(function(cmp) { if (cmp.getId () === 'win') cmp.destroy(); });
					 var errorPanel= Ext.create('Ext.window.Window', {
					    title: 'Signature Discription',
					    id : 'win',
					    width: 450,
					    height: 400,
				        plain: true,
				        floating: true,
				        closable : true,
				        autoScroll: true,
				        html: '<div dir=ltr align="justify"><p align="center"><b>Signature Number    '+ decoded.data[0].signature+' </b>'
					    +  '<p><b>Message : </b>'+decoded.data[0].DefaultMessage+'</p>'
					    +  '<p><b>ParsIPS Default Config : </b> <br>Status is '+decoded.data[0].DefaultStatus+'.<br>Action is '+decoded.data[0].DefaultAction+'.<br>Log is '+decoded.data[0].DefaultLog+'</p>',
					    renderTo: document.body
					}).show();
				 }
			}
		});
		
	},
	
	sendForm2: function(){
		var box = Ext.MessageBox.wait('Please wait...', 'Saving');
		cfg.form.submit({
		    submitEmptyText: true,
		    waitMsg: 'vasta',
		    params: cfg.data,
		    method: 'POST',
		    url: cfg.url,
		    success: function(form, action) {
			box.hide();
		    	cfg.callBack.call();
			if(action.result.errors != undefined){
				Ext.Msg.alert('Error', action.result.errors);
			}
		    },
		    failure: function(form, action) {
			box.hide();
		        switch (action.failureType) {
		            case Ext.form.action.Action.CLIENT_INVALID:
		                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
		                break;
		            case Ext.form.action.Action.CONNECT_FAILURE:
		                Ext.Msg.alert('Failure', 'Ajax communication failed');
		                break;
		            case Ext.form.action.Action.SERVER_INVALID:
				var message = action.result.msg == undefined ? action.result.errors : action.result.msg
		                Ext.Msg.alert('Failure', message);
				break;
		        }
		    }
		});
	}
});
