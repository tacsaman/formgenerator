Ext.define("Ext.ux.PreparePrint", {

    //requires: 'Ext.XTemplate',
    init: function () {
        var me = this;
        Ext.define('factorPrintModel', {
            extend: 'Ext.data.Model',

            fields: [
                {
                    name: 'FactorNo'
                },{
                    name: 'FactorDate'
                },{
                    name: 'MaturityDate'
                },{
                    name: 'CustomerName'
                },{
                    name: 'Tbl_StuffID'
                },{
                    name: 'Tbl_UnitID'
                },{
                    name: 'StuffQty'
                },{
                    name: 'SoldPrice'
                },{
                    name: 'Discount'
                },{
                    name: 'TaxAmount',
                    type: 'int'
                },{
                    name: 'StaffCode'
                },{
                    name: 'StaffName'
                },{
                    name: 'UnitName'
                }
            ]
        });

        store = Ext.create('Ext.data.Store', {
            id: 'factorPrintStore',
            model: 'factorPrintModel',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: 'ExtDesk.php',
                method: 'GET',
                extraParams: {
                    Module: 'Factor',
                    option: 'print',
                    action: 'list',
                    factorId: ''
                }
            }
        });
    },



    statics: {

        loadPrintStore: function(id){
            var store = Ext.create('Ext.data.Store', {
                id: 'factorPrintStore',
                model: 'factorPrintModel',
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: 'ExtDesk.php',
                    method: 'GET',
                    extraParams: {
                        Module: 'Factor',
                        option: 'print',
                        action: 'list',
                        factorId: id
                    }
                }
            });
            //alert(store);
            return store;
        },

        prepareContentOne : function(gridName){
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];

            //console.log(grid.getView().getGridColumns());
            //console.log(selection.data['StaffCode']);
            //alert(selection.data['StaffCode']);
            //console.log(grid.columns[1].dataIndex);
            var message ="";
            var table = '<div><table style="direction: rtl;width: 100%;font-family: tahoma;font-size: 13px;">\n';
            var header = '<tr style="background-color: d6d6d6;font-weight: bold;text-align: center;">';
            for(var i = 0; i < grid.columns.length; i++){
                if(!grid.columns[i].hidden){
                    header += '<td>';
                    header += grid.columns[i].text;
                    header += '</td>';
                }
            }
            header += '</tr>\n';
            var tbody = '<tr>';
            for(var i = 0; i < grid.columns.length; i++){
                if(!grid.columns[i].hidden){
                    tbody += '<td>';
                    tbody += selection.data[grid.columns[i].dataIndex];
                    tbody += '</td>';
                }
            }
            tbody += '</tr>\n';
            var footer = '</table></div>';
            message = table + header + tbody + footer;
            return message;
            //window.open('PrintOne.php?message='+ message);
        },

        prepareContentPaziresh : function(gridName){
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            if(selection != null){
            //console.log(grid.getView().getGridColumns());
            //console.log(selection.data['StaffCode']);
            //alert(selection.data['StaffCode']);
            //console.log(grid.columns[1].dataIndex);
            var message ="";
            var table = '<div><table style="direction: rtl;width: 500px;border: gray solid 1px; margin-left: auto; margin-right: auto; font-family: tahoma;font-size: 13px;">\n';
            var header = '<tr">';
            header += '<td style="height: 50px;">';
            header += '<h3>    ------    </h3>';
            header += '</td>';
            header += '<td>';
            header += '<img src="../client/ux/grid/gridPrinterCss/companylogo.png">';
            header += '</td>';
            /*for(var i = 0; i < grid.columns.length; i++){
                if(!grid.columns[i].hidden){
                    header += '<td>';
                    header += grid.columns[i].text;
                    header += '</td>';
                }
            }*/
            header += '</tr>\n';
            var tbody = '';
            for(var i = 0; i < grid.columns.length; i++){
                if(!grid.columns[i].hidden){
                    tbody += '<tr>';
                    tbody += '<td>';
                    tbody += grid.columns[i].text;
                    tbody += '</td>';
                    tbody += '<td>';
                    tbody += selection.data[grid.columns[i].dataIndex];
                    tbody += '</td>';
                    tbody += '</tr>\n';
                }
            }

                var comments = '';
                if(gridName == 'customerGridId'){
                    var ajax = Ext.Ajax.request({
                        url: 'ExtDesk.php',
                        method: 'GET',
                        async: false,
                        params: {
                            Module: 'BaseInfo',
                            option: 'printcomment',
                            action: 'readByType',
                            InType: '1'
                        },
                        success: function (response) {
                            //result = response.responseText;
                            //result = Ext.decode(result);
                        }
                    }, this);

                    //console.log(Ext.decode(ajax.responseText).data.length);
                    var comment = Ext.decode(ajax.responseText);
                    if(comment.data.length != 0){
                        comments += '<tr><td  colspan="2" style="height: 20px;"></td></tr>';
                        comments += '<tr><td  colspan="2">' + comment.data[0].Comment + '</td></tr>';
                    }
                }

                if(gridName == 'PazireshGridId'){
                    var ajax = Ext.Ajax.request({
                        url: 'ExtDesk.php',
                        method: 'GET',
                        async: false,
                        params: {
                            Module: 'BaseInfo',
                            option: 'printcomment',
                            action: 'readByType',
                            InType: '2'
                        },
                        success: function (response) {
                            //result = response.responseText;
                            //result = Ext.decode(result);
                        }
                    }, this);

                    var comment = Ext.decode(ajax.responseText);
                    if(comment.data.length != 0){
                        comments += '<tr><td  colspan="2" style="height: 20px;"></td></tr>';
                        comments += '<tr><td  colspan="2">' + comment.data[0].Comment + '</td></tr>';
                    }
                }

            var footer = '</table></div>';
            message = table + header + tbody + comments + footer;
            return message;
            //window.open('PrintOne.php?message='+ message);
            }
            else{
                //alert('لطفا یک مورد را انتخاب کنید');
                Ext.Msg.alert('اخطار', 'لطفا یکی از موارد را انتخاب کنید')
            }
        },

        prepareContentFactor : function(gridName){
            var grid = Ext.getCmp(gridName);
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            var ajax = Ext.Ajax.request({
                url: 'ExtDesk.php',
                method: 'GET',
                async: false,
                params: {
                    Module: 'Factor',
                    option: 'print',
                    action: 'list',
                    factorId: selection.data['id']
                },
                success: function (response) {
                    //result = response.responseText;
                    //result = Ext.decode(result);
                }
            }, this);

            var result = Ext.decode(ajax.responseText);

            var ajax1 = Ext.Ajax.request({
                url: 'ExtDesk.php',
                method: 'GET',
                async: false,
                params: {
                    Module: 'BaseInfo',
                    option: 'printcomment',
                    action: 'readByType',
                    InType: '3'
                },
                success: function (response) {
                    //result = response.responseText;
                    //result = Ext.decode(result);
                }
            }, this);

            var comment = Ext.decode(ajax1.responseText);


            return this.MakeHtmlOfFactor(result, comment);
            //console.log(store);
            //alert(store.data[0]);
            //console.log(selection.data['StaffCode']);
            //alert(selection.data['StaffCode']);
            //console.log(grid.columns[1].dataIndex);
            /*var message ="";
            var table = '<div><table style="direction: rtl;width: 100%;font-family: tahoma;font-size: 13px;">\n';
            var header = '<tr style="background-color: d6d6d6;font-weight: bold;text-align: center;">';
            for(var i = 0; i < grid.columns.length; i++){
                if(!grid.columns[i].hidden){
                    header += '<td>';
                    header += grid.columns[i].text;
                    header += '</td>';
                }
            }
            header += '</tr>\n';
            var tbody = '<tr>';
            for(var i = 0; i < grid.columns.length; i++){
                if(!grid.columns[i].hidden){
                    tbody += '<td>';
                    tbody += selection.data[grid.columns[i].dataIndex];
                    tbody += '</td>';
                }
            }
            tbody += '</tr>\n';
            var footer = '</table></div>';
            message = table + header + tbody + footer;
            return message;*/
            //window.open('PrintOne.php?message='+ message);
        },

        MakeHtmlOfFactor : function(result, comment){
            //alert(result.data[2].Discount);
            var message ="";
            var table = '<div><table style="direction: rtl; border: gray solid 1px; width: 90%;font-family: tahoma;font-size: 13px;margin-left: auto;margin-right: auto;">\n';
            var header = '<tr">';
            header += '<td  rowspan="2" style="width: 200px;"><img src="../client/ux/grid/gridPrinterCss/logo1.png"></td>';
            header += '<td colspan="2" rowspan="2" style="text-align: center;width: 300px;"><h2>     شرکت پارس دانه سوادکوه    </h2></td>';
            header += '<td style="width: 200px;">  تاریخ فاکتور:  ' + result.data[0].FactorDate + '</td>';
            header += '</tr>\n';

            header += '<tr">';
            header += '<td style="width: 200px;"> شماره فاکتور:  ' +result.data[0].FactorNo + '</td>';
            header += '</tr>\n';

            header += '<tr>\n';
            header += '<td colspan="2"> نام مشتری:  ' + result.data[0].CustomerName + '</td>';
            header += '<td colspan="2"></td>';
            header += '</tr>\n';


            var tbody = '<tr>';
            tbody += '<td colspan="4">';

            tbody += '<table style="direction: rtl; border: gray solid 1px; width: 95%;font-family: tahoma;font-size: 13px;margin-left: auto;margin-right: auto;">';
            tbody += '<tr style="background-color: d6d6d6;text-align: center;font-weight: bold;">';
            tbody += '<td style="width: 10%;"> ردیف </td>';
            tbody += '<td style="width: 40%;"> شرح </td>';
            tbody += '<td style="width: 10%;"> تعداد </td>';
            tbody += '<td style="width: 10%;"> قیمت واحد </td>';
            tbody += '<td style="width: 10%;"> جمع </td>';
            tbody += '</tr>';

            var total = 0;
            var totalAll = 0;
            for(var i = 0; i < result.data.length; i++){
                tbody += '<tr>';
                tbody += '<td>' + i +1 + '</td>';
                tbody += '<td>' + result.data[i].StaffName + '</td>';
                tbody += '<td>' + result.data[i].StuffQty + '</td>';
                tbody += '<td>' + result.data[i].SoldPrice + '</td>';
                total = parseFloat(result.data[i].SoldPrice) * parseFloat(result.data[i].StuffQty);
                totalAll += total;
                tbody += '<td>' + total + '</td>';
                tbody += '</tr>';
            }

            tbody += '<tr>';
            tbody += '<td></td>';
            tbody += '<td></td>';
            tbody += '<td></td>';
            tbody += '<td> تخفیف </td>';
            tbody += '<td>' + result.data[0].Discount + '</td>';
            tbody += '</tr>';

            tbody += '<tr>';
            tbody += '<td></td>';
            tbody += '<td></td>';
            tbody += '<td></td>';
            tbody += '<td> مالیات </td>';
            tbody += '<td>' + result.data[0].TaxAmount + '</td>';
            tbody += '</tr>';

            tbody += '<tr>';
            tbody += '<td></td>';
            tbody += '<td></td>';
            tbody += '<td></td>';
            tbody += '<td> جمع کل </td>';
            tbody += '<td>' + (totalAll + parseFloat(result.data[0].TaxAmount) - parseFloat(result.data[0].Discount)) + '</td>';
            tbody += '</tr>';

            tbody += '</table>';

            tbody += '</td>';
            tbody += '</tr>';

            if(comment.data.length != 0){
                var Comment = '<tr>';
                Comment += '<td></td>';
                Comment += '<td style="text-align: center; font-family: tahoma;">';
                Comment += '<h3>';
                Comment += comment.data[0].Comment;
                Comment += '</h3>';
                Comment += '</td>';
                Comment += '<td></td></tr>';
            }

            var footer = '</table></div>';
            message = table + header + tbody + Comment + footer;
            return message;
        }
    }
});