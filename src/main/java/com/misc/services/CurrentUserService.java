package com.misc.services;

import com.misc.CurrentUser;

/**
 * Created by saman on 8/25/2017.
 */
    public interface CurrentUserService {

        boolean canAccessUser(CurrentUser currentUser, Long userId);
    }

