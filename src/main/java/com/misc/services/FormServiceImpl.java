package com.misc.services;

import com.misc.DAO.ElementRepository;
import com.misc.DAO.FormRepository;
import com.misc.DTO.FormDto;
import com.misc.entitis.ElementEntity;
import com.misc.entitis.FormEntity;
import com.misc.entitis.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FormServiceImpl implements FormService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FormServiceImpl.class);

    @Autowired
    FormRepository formRepository;

    @Autowired
    ElementRepository elementRepository;

    @Override
    public FormEntity getFormById(long id) {
        return null;
    }

    @Override
    public Collection<FormEntity> getAllForms() {
        return (Collection<FormEntity>) formRepository.findAll();
    }

    @Override
    public Collection<FormEntity> getFormsByRole(Role role) {
        return null;
    }

    @Override
    public FormEntity create(FormDto formDto) {
        ElementEntity elementEntity = new ElementEntity();
        elementEntity.setElementName(formDto.getElementName());
        elementEntity.setType(formDto.getElementType());
        elementEntity.setValue("");
        elementRepository.save(elementEntity);

        FormEntity entity = formRepository.findOne(formDto.getFormId());
        entity.getElementEntities().add(elementEntity);
        return formRepository.save(entity);
    }
}
