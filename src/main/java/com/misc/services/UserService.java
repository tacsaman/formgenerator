package com.misc.services;

import com.misc.DTO.UserDto;
import com.misc.entitis.UserEntity;

import java.util.Collection;
import java.util.List;

/**
 * Created by saman on 8/25/2017.
 */
public interface UserService {
    UserEntity getUserById(long id);

    UserEntity getUserByEmail(String email);

    List<UserEntity> getAllUsers();

    UserEntity create(UserDto userBean);
}
