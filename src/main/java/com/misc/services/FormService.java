package com.misc.services;

import com.misc.DTO.FormDto;
import com.misc.DTO.UserDto;
import com.misc.entitis.ElementEntity;
import com.misc.entitis.FormEntity;
import com.misc.entitis.Role;
import com.misc.entitis.UserEntity;

import java.util.Collection;

public interface FormService {
    public FormEntity getFormById(long id);
    public Collection<FormEntity> getAllForms();
    public Collection<FormEntity> getFormsByRole(Role role);
    FormEntity create(FormDto fromDto);


}
