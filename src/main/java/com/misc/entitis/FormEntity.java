package com.misc.entitis;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by saman on 8/25/2017.
 */
@Entity
@Table(name = "tbl_form")
public class FormEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "form_id")
    Long formId;

    @Column(name= "name")
    String name;

    @OneToMany(mappedBy = "formEntity",fetch = FetchType.LAZY)
   Set<ElementEntity> elementEntities = new HashSet<>();

    public Set<ElementEntity> getElementEntities() {
        return elementEntities;
    }

    public void setElementEntities(Set<ElementEntity> elementEntities) {
        this.elementEntities = elementEntities;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
