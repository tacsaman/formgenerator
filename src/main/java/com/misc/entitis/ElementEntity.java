package com.misc.entitis;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by saman on 8/25/2017.
 */
@Entity
@Table(name = "tbl_element")
public class ElementEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "element_id", nullable = false, updatable = false)
    Long elmentId;

    @Column(name = "elementName", nullable = false)
    String elementName;

    @Column(name = "type", nullable = false, unique = true)
    @Enumerated(EnumType.STRING)
    ElementType type;


    @Column(name = "value", nullable = false)
    String value;


    public Long getElmentId() {
        return elmentId;
    }

    public void setElmentId(Long elmentId) {
        this.elmentId = elmentId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public ElementType getType() {
        return type;
    }

    public void setType(ElementType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
