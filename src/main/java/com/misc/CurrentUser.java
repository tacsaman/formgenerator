package com.misc;

import com.misc.entitis.Role;
import com.misc.entitis.UserEntity;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * Created by saman on 8/25/2017.
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private UserEntity user;

    public CurrentUser(UserEntity user) {
        super(user.getEmail(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole().toString()));
        this.user = user;
    }

    public UserEntity getUser() {
        return user;
    }

    public Long getId() {
        return user.getUserid();
    }

    public Role getRole() {
        return user.getRole();
    }

    @Override
    public String toString() {
        return "CurrentUser{" +
                "user=" + user +
                "} " + super.toString();
    }
}
