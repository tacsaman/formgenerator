package com.misc.validator;

import com.misc.DTO.UserDto;
import com.misc.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by saman on 8/25/2017.
 */
@Component
public class UserBeanValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserBeanValidator.class);

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LOGGER.debug("Validating {}", target);
        UserDto bean = (UserDto) target;
        validatePasswords(errors, bean);
        validateEmail(errors, bean);
    }
    private void validatePasswords(Errors errors, UserDto bean) {
        if (!bean.getPassword().equals(bean.getPasswordRepeated())) {
            errors.reject("password.no_match", "Passwords do not match");
        }
    }

    private void validateEmail(Errors errors, UserDto bean) {
        if (userService.getUserByEmail(bean.getEmail()) != null) {
            errors.reject("email.exists", "User with this email already exists");
        }
    }
}