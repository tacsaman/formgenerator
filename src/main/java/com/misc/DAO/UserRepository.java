package com.misc.DAO;

import com.misc.entitis.UserEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by saman on 8/25/2017.
 */
public interface UserRepository extends CrudRepository<UserEntity,Long> {
    UserEntity findOneByEmail(String email);
}
