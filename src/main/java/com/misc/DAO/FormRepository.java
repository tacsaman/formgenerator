package com.misc.DAO;

import com.misc.entitis.ElementEntity;
import com.misc.entitis.FormEntity;
import org.springframework.data.repository.CrudRepository;

public interface FormRepository extends CrudRepository<FormEntity,Long> {
}
