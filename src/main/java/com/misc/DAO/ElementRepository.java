package com.misc.DAO;

import com.misc.entitis.ElementEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by saman on 9/22/2017.
 */
public interface ElementRepository extends CrudRepository<ElementEntity,Long> {
}
