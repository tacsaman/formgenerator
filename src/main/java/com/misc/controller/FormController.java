package com.misc.controller;

import com.misc.DTO.ElementDto;
import com.misc.DTO.FormDto;
import com.misc.DTO.UserDto;
import com.misc.services.FormService;
import com.misc.services.UserService;
import com.misc.validator.UserBeanValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by s_salehi on 9/17/2017.
 */
@Controller
public class FormController {
    @Autowired
    FormService formService;
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/form/create", method = RequestMethod.GET)
    public ModelAndView getFormCreatePage() {
        LOGGER.debug("Getting form create page");
        return new ModelAndView("form_create", "form", new FormDto());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/form/create", method = RequestMethod.POST)
    public String handleUserCreateForm(@Valid @ModelAttribute("form") FormDto form, BindingResult bindingResult) {
        LOGGER.debug("Processing form create form={}, bindingResult={}", form, bindingResult);
        if (bindingResult.hasErrors()) {
            return "user_create";
        }
        try {
            formService.create(form);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the user, assuming duplicate email", e);
            bindingResult.reject("email.exists", "Email already exists");
            return "user_create";
        }
        return "redirect:/users";
    }
}
