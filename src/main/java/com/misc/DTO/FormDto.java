package com.misc.DTO;

import com.misc.entitis.ElementEntity;
import com.misc.entitis.ElementType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by saman on 8/25/2017.
 */
public class FormDto {
    @NotEmpty
    long formId;
    String elementName = "";
    ElementType elementType = ElementType.RADIOBUTTON;

    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public ElementType getElementType() {
        return elementType;
    }

    public void setElementType(ElementType elementType) {
        this.elementType = elementType;
    }
}
