package com.misc.DTO;

import com.misc.entitis.ElementType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Created by saman on 8/25/2017.
 */
public class ElementDto {
    @NotEmpty
    String elementName = "";
    @NotNull
    ElementType type = ElementType.RADIOBUTTON;
    @NotEmpty
    String value = "";

    @NotNull
    FormDto formDto;

    public FormDto getFormDto() {
        return formDto;
    }

    public void setFormDto(FormDto formDto) {
        this.formDto = formDto;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public ElementType getType() {
        return type;
    }

    public void setType(ElementType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
